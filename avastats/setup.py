from setuptools import setup, find_packages
 
setup(
    name='avastats',
    version='0.0.0',
    # url='https://gitlab.com/albina-euregio/pyAvaCore',
    license='GPL',
    author='LWD Tirol',
    packages=find_packages(exclude=['tests']),
)
