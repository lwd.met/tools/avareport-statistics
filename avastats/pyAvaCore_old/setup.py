from setuptools import setup, find_packages

setup(
    name="pyAvaCore_old",
    version="0.0.0",
    url="https://gitlab.com/albina-euregio/pyAvaCore",
    license="GPL",
    author="Friedrich Mütschele",
    packages=find_packages(exclude=["tests"]),
)
