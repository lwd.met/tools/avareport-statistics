from shapely.geometry import shape, Point
from shapely.ops import unary_union
import numpy as np
import os
import json

class EAWSRegions:

    regions: list
    neighborhood_matrix: np.array

    #Region IDs currently have different naming schemas. This yet needs custom adoption at the moment
    def ex_region_id(self, feature):
        region_id = ''
        try:
            if feature['properties']['country_id'] == 'FR':
                region_id = feature['properties']['id']
            if feature['properties']['country_id'] == 'NO':
                region_id = 'NO-' + feature['properties']['aws_ID']
            if feature['properties']['country_id'] == 'AD':
                region_id = 'FR-71'
            if feature['properties']['country_id'] == 'GB':
                region_id = 'UK-' + str(int(feature['properties']['aws_ID']))
        except:
            region_id = feature['properties']['id']
            if region_id[-1].isalpha() and region_id.startswith('IT'):
                region_id = region_id[:-1]

        if region_id.startswith('FR-0'):
            region_id = region_id.replace('FR-0', 'FR-')

        return region_id


    #Crawl micro-regions and build a matrix of all regions and a dict with meta information like ID, shape and validity for each shape
    def __init__(self):
        self.regions = []
        directory = os.getcwd()+'/eaws-regions/public/micro-regions/'
        for entry in os.scandir(directory):
            if entry.path.endswith(".json") and entry.is_file():
                with open(entry.path, 'r') as f:
                    file = json.load(f)

                for feat in file['features']:
                    region_id = self.ex_region_id(feat)
                    start_date = None
                    end_date = None
                    if 'start_date' in feat['properties']:
                        start_date = feat['properties']['start_date']
                    if 'end_date' in feat:
                        end_date = feat['properties']['end_date']
                    
                    geometry = shape(feat['geometry'])

                    self.regions.append([region_id, shape(feat['geometry']), start_date, end_date, str(entry.path)])

        self.neigborhood_matrix = np.zeros((len(self.regions),len(self.regions)), dtype=bool)

        for m, region_m in enumerate(self.regions):
            for n, region_n in enumerate(self.regions):
                try:
                    self.neigborhood_matrix[m][n] = region_m[1].intersects(region_n[1])
                except:
                    print('Problem comparing to region', region_m[0], region_n[0], type(region_m[1]), type(region_n[1]), region_n[4])

    def get_neighbors(self, region_id):
        neighbors = []
        for m, current_region in enumerate(self.regions):
            if region_id in current_region[0]:
                for n in range(len(self.regions)-1):
                    if self.neigborhood_matrix[m][n] and m != n:
                        neighbors.append(self.regions[n])
        return neighbors

            
    def get_neighbor_relations(self, region_id):
        neighbors = []
        for m, current_region in enumerate(self.regions):
            if region_id in current_region[0]:
                for n in range(len(self.regions)-1):
                    if self.neigborhood_matrix[m][n] and m != n:
                        neighbors.append(self.regions[n][0])
        return neighbors
    

    def display_neighborhood(self, region_id):
        import cartopy.crs as ccrs
        import cartopy.feature as cfeature
        import matplotlib.pyplot as plt

        for i, current_region in enumerate(self.regions):
            if region_id in current_region[0]:
                bounds = [0., 30., 40., 55.]
                ax = plt.axes(projection=ccrs.PlateCarree())
                ax.set_extent(bounds, crs=ccrs.PlateCarree())
                ax.add_feature(cfeature.COASTLINE.with_scale('110m'), linewidth=0.75)
                ax.set_title(self.regions[i][0])

                for j in range(len(self.regions)-1):
                    if self.neigborhood_matrix[i][j] and i != j:
                        self.add_geometrie(self.regions[j][1], ax, 'red')
                self.add_geometrie(self.regions[i][1], ax, 'yellow')
                plt.show()

    def add_geometrie(self, geometry, ax, edgecolor):
        import cartopy.crs as ccrs
        if "Multi" in geometry.type:
            ax.add_geometries(geometry, edgecolor=edgecolor, crs=ccrs.PlateCarree())
        else:
            ax.add_geometries([geometry], edgecolor=edgecolor, crs=ccrs.PlateCarree())