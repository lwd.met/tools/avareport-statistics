from bs4 import BeautifulSoup
from urllib.request import urlopen
import datetime
import requests

def download_from_index(url, local_dir, file_ext = None, start=None, end=None, details=""):
    page = requests.get(url).text
    soup = BeautifulSoup(page, 'html.parser')
    
    if start != None and end != None:
        start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
        end = datetime.datetime.strptime(end, "%Y-%m-%d").date()
    
    for node in soup.find_all('a'):
        keep = True
        _url = url + node.get('href')
        
        if any(x in node.get('href') for x in ["../", "latest", "archive"]):
            continue           
        
        if 'albina_euregio' in details:
            s_date = node.get('href').replace('/', '')
            _url = _url + s_date + "_de.xml"
            filename = "EUREGIO_" + s_date + "_de.xml"
            
            if 'restrict' in details:
                date = datetime.datetime.strptime(s_date, "%Y-%m-%d").date()
                if date < end and date > start:
                    keep = True
                else:
                    keep = False
           
        if 'tyrol_archive' in details:
            filename = "AT-07_" +node.get('href')
            if 'en' in filename:
                keep = False

        if keep:
            with urlopen(_url) as http, open(
                    f"{local_dir}/{filename}", mode="wb"
                ) as f:
                    f.write(http.read())