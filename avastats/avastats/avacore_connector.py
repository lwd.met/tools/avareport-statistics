import os
import json
import datetime
import numpy as np
import pandas as pd
import xml.etree.ElementTree as ET

from pyAvaCore_old.avacore import avabulletins
from pyAvaCore_old.avacore.avabulletins import Bulletins
from pyAvaCore_old.avacore.processor_caamlv5 import parse_xml
from pyAvaCore_old.avacore.processor_caamlv5 import parse_xml_vorarlberg


class AvacoreConnector:

    def __init__(self, dir="/data/bulletins"):
        self.dir = dir
        self.micro_regions = {}
        self.macro_regions = {}
        self.file_index = {}
        self.get_available_seasons()

    def get_available_seasons(self):
        
        for file in os.listdir(os.getcwd() + self.dir):
            if file.endswith(".json") and not file.endswith(".rating.json"):
                with open(os.getcwd() + self.dir + '/' + file) as fp:
                    data = json.load(fp)
                bulletins = avabulletins.Bulletins()
                tmp = file.split('/')[-1]
                file_detail_region = '-'.join(tmp.split('-')[3:]).replace(".json", "")
                file_detail_valid_day = datetime.datetime.strptime('-'.join(tmp.split('-')[0:3]), "%Y-%m-%d")
                try:
                    bulletins.from_json(data)
                except:
                    print("Can't read in JSON:", file)
                self.file_index[file_detail_region + ":" + str(file_detail_valid_day.date())] = [file, "json"]
            elif file.endswith(".xml"):  # Use only CAAMLv5 Std XML's!
                with open(os.getcwd() + self.dir + '/' + file) as fp:
                    root = ET.parse(fp)
                tmp = file.split('/')[-1]
                file_detail_region = tmp.split('_')[1]
                file_detail_valid_day = datetime.datetime.strptime(tmp.split('_')[0], "%Y-%m-%d")
                bulletins = avabulletins.Bulletins()
                bulletins.bulletins = parse_xml(root)
                
                parser = "caamlv5" if not 'TYROL' in file else "caamlv5_vbg"
                self.file_index[file_detail_region + ":" + str(file_detail_valid_day.date())] = [file, parser]

            # try:
            for bulletin in bulletins.bulletins:
                for region in bulletin.get_region_list():
                    if not file_detail_region in self.macro_regions:
                        self.macro_regions[file_detail_region] = [file_detail_valid_day, file_detail_valid_day]
                    else:
                        if file_detail_valid_day < self.macro_regions[file_detail_region][0]:
                            self.macro_regions[file_detail_region][0] = file_detail_valid_day
                        elif file_detail_valid_day > self.macro_regions[file_detail_region][1]:
                            self.macro_regions[file_detail_region][1] = file_detail_valid_day

                    if not region in self.micro_regions:
                        self.micro_regions[region] = [file_detail_valid_day, file_detail_valid_day, file_detail_region]
                    else:
                        if file_detail_valid_day < self.micro_regions[region][0]:
                            self.micro_regions[region][0] = file_detail_valid_day
                        elif file_detail_valid_day > self.micro_regions[region][1]:
                            self.micro_regions[region][1] = file_detail_valid_day
            # except:
            #     print("Faild to work with Bulletin in:", file)

    def get_dangerlevels_for_region(self, macro_id, day_range, macro_filename=None):

        micro_regions_axes = []

        macro_filename = macro_id if macro_filename is None else macro_filename

        for micro_region in self.micro_regions:
            if macro_id in micro_region:
                micro_regions_axes.append(micro_region)

        d_max = np.full((len(micro_regions_axes), len(day_range)), -1, dtype=np.int16)
        d_morning = np.full((len(micro_regions_axes), len(day_range)), -1, dtype=np.int16)
        d_afternoon = np.full((len(micro_regions_axes), len(day_range)), -1, dtype=np.int16)

            
        for n, day in enumerate(day_range):
            # try:
            if macro_filename + ":" + str(day.date()) in self.file_index:
                filename, parser = self.file_index[macro_filename + ":" + str(day.date())]
                if 'json' in parser:
                    with open(os.getcwd() + self.dir + '/' + filename) as fp:
                        data = json.load(fp)
                    bulletins = avabulletins.Bulletins()
                    bulletins.from_json(data)

                if 'caamlv5' in parser:
                    with open(os.getcwd() + self.dir + '/' + filename) as fp:
                        root = ET.parse(fp)
                    bulletins = avabulletins.Bulletins()
                    bulletins.bulletins = parse_xml(root)

                if 'caamlv5_vbg' in parser:
                    with open(os.getcwd() + self.dir + '/' + filename) as fp:
                        root = ET.parse(fp)
                    bulletins = avabulletins.Bulletins()
                    bulletins.bulletins = parse_xml_vorarlberg(root)
                
                ratings = bulletins.max_danger_ratings(validity_date=day.date())
                # except:
                    # print("can't open", self.dir + '/' + str(day) + '-' + macro_id)
                #     pass
                for m, micro_region in enumerate(micro_regions_axes):
                    try:
                        d_max[m][n] = np.int16(ratings[micro_region])
                    except:
                        # print('wrong reading for d_max', day.date(), micro_region)
                        pass
                    try:
                        d_morning[m][n] = np.int16(ratings[micro_region+':am'])
                    except:
                        # print('wrong reading for d_max', day.date(), micro_region)
                        pass

                    try:
                        d_afternoon[m][n] = np.int16(ratings[micro_region+':pm'])
                    except:
                        # print('wrong reading for d_max', day.date(), micro_region)
                        pass

        return d_max, d_morning, d_afternoon, micro_regions_axes


    def get_avalanche_problems(self,local_dir,start_season,end_season,regions_of_interest,avaProbs):
        """
        """
        day_range = pd.date_range(start=start_season,end=end_season).to_pydatetime().tolist()
        day_range = [d.date() for d in day_range]
        df_avaProbs = pd.DataFrame(0, index=np.arange(len(avaProbs)), columns=day_range)

        for file in os.listdir(local_dir):
            bulletins = Bulletins()
            with open(local_dir + '/' + file) as fp:
                # clear_output(wait=True)
                # print(file)
                if ".xml" in file:
                    root = ET.parse(fp)
                    bulletins.bulletins = parse_xml(root)
                if ".json" in file:
                    data = json.load(fp)
                    try:
                        bulletins.from_json(data)
                    except:
                        print("Can't work with JSON file:", file)
            date = bulletins.main_date()

            if date <= datetime.datetime.strptime(end_season, "%Y-%m-%d").date() and date >= datetime.datetime.strptime(start_season, "%Y-%m-%d").date():
                no_regions = 0.
                ### print(no_regions)
                i=0
                for bulletin in bulletins.bulletins:
                    ### print(bulletin.get_region_list())
                    bulletin_no_regions = sum(self.check_regions_of_interest(x,regions_of_interest) for x in bulletin.get_region_list())
                    no_regions += bulletin_no_regions
                    ### print(i,no_regions)
                    # if bulletin.avalancheProblems:
                    ns = []
                    for avaProb in bulletin.avalancheProblems:
                        if not avaProb.problemType in ['','favourable_situation']:
                            n = avaProbs.index(avaProb.problemType)
                            if n in ns:
                                pass # cover early, later, ... bulletin
                            else:
                                # print(i,avaProb.problemType,avaProb.validTimePeriod)
                                ns.append(n)
                                df_avaProbs.loc[n, date] += bulletin_no_regions
                    i+=1
                    
                for avaProb in avaProbs:
                    n = avaProbs.index(avaProb)
                    df_avaProbs.loc[n, date] = 100 * df_avaProbs.loc[n, date] / no_regions
                    # df_avaProbs.loc[n, date] = 100 * df_avaProbs.loc[n, date] / 58

        # - Cumulative dataframe required for bar plots (to include bottom argument) - #
        df_avaProbs_cum=df_avaProbs.cumsum()
        i=np.arange(1,len(avaProbs))
        df_avaProbs_cum.loc[i]=df_avaProbs_cum.loc[i-1].values
        df_avaProbs_cum.loc[0,:] = 0
        df_avaProbs_cum=df_avaProbs_cum.T
        df_avaProbs=df_avaProbs.T

        return df_avaProbs


    def get_upper_danger_level_and_avalanche_Problems_for_region(self,local_dir,start_season,end_season,regions_of_interest,avaProbs):
        """
        """
        day_range = pd.date_range(start=start_season,end=end_season).to_pydatetime().tolist()
        day_range = [d.date() for d in day_range]

        danger_levels = ['low','moderate','considerable','high','very high']
        df_danger_level = pd.DataFrame(0, index=day_range, columns=['danger_level'])

        df_avaProbs = pd.DataFrame(0, index=np.arange(len(avaProbs)), columns=day_range)

        for file in os.listdir(local_dir):
            bulletins = Bulletins()
            with open(local_dir + '/' + file) as fp:
                # clear_output(wait=True)
                # print(file)
                if ".xml" in file:
                    root = ET.parse(fp)
                    bulletins.bulletins = parse_xml(root)
                if ".json" in file:
                    data = json.load(fp)
                    try:
                        bulletins.from_json(data)
                    except:
                        print("Can't work with JSON file:", file)
            date = bulletins.main_date()
            if date <= datetime.datetime.strptime(end_season, "%Y-%m-%d").date() and date >= datetime.datetime.strptime(start_season, "%Y-%m-%d").date():
                for bulletin in bulletins.bulletins:
                    ### print(bulletin.get_region_list())
                    bulletin_no_regions = sum(self.check_regions_of_interest(x,regions_of_interest) for x in bulletin.get_region_list())

                    if bulletin_no_regions > 0:
                        # if bulletin.avalancheProblems:
                        ns = []
                        for avaProb in bulletin.avalancheProblems:
                            n = avaProbs.index(avaProb.problemType)
                            if n in ns:
                                pass # cover early, later, ... bulletin
                            else:
                                # print(i,avaProb.problemType,avaProb.validTimePeriod)
                                ns.append(n)
                                df_avaProbs.loc[n, date] += 100
                                # df_out.loc[n, date] = 1

                        # - Get danger level of highest elevation band or above 2100m / seems like only treeline is used - #
                        max_rating = 0
                        for rating in bulletin.dangerRatings:
                            if hasattr(rating.elevation, 'upperBound'):
                                print('Current rating upper bound: {}, Threshold: 2200m'.format(rating.elevation.upperBound), end='\r')
                                if rating.elevation.upperBound != 'treeline':
                                    if int(rating.elevation.upperBound) > 2200:
                                        if danger_levels.index(rating.mainValue) > max_rating:
                                            max_rating = danger_levels.index(rating.mainValue)
                            else:
                                if danger_levels.index(rating.mainValue) > max_rating:
                                    max_rating = danger_levels.index(rating.mainValue)
                        
                        df_danger_level.loc[date,'danger_level'] = max_rating + 1
                        # break

        df_avaProbs=df_avaProbs.T
        df_danger_level['danger_level'].replace(0,np.nan,inplace=True)
        return df_avaProbs, df_danger_level


    def get_danger_patterns(self,local_dir,start_season,end_season,regions_of_interest,patterns):
        """
        """
        day_range = pd.date_range(start=start_season,end=end_season).to_pydatetime().tolist()
        day_range = [d.date() for d in day_range]
        df_danger_patterns = pd.DataFrame(0, index=np.arange(len(patterns)), columns=day_range)

        for file in os.listdir(local_dir):
            bulletins = Bulletins()
            with open(local_dir + '/' + file) as fp:
                # clear_output(wait=True)
                # print(file)
                if ".xml" in file:
                    root = ET.parse(fp)
                    bulletins.bulletins = parse_xml(root)
                if ".json" in file:
                    data = json.load(fp)
                    try:
                        bulletins.from_json(data)
                    except:
                        print("Can't work with JSON file:", file)
            date = bulletins.main_date()
            if date <= datetime.datetime.strptime(end_season, "%Y-%m-%d").date() and date >= datetime.datetime.strptime(start_season, "%Y-%m-%d").date():
                no_regions = 0.
                # local_patterns = np.zeros(len(patterns))
                for bulletin in bulletins.bulletins:
                    bulletin_no_regions = sum(self.check_regions_of_interest(x,regions_of_interest) for x in bulletin.get_region_list())
                    no_regions += bulletin_no_regions
                    if bulletin.customData:
                        if "LWD_Tyrol" in bulletin.customData:
                            if "dangerPatterns" in bulletin.customData["LWD_Tyrol"]:
                                for pattern in bulletin.customData["LWD_Tyrol"]['dangerPatterns']:
                                    n = patterns.index(pattern)
                                    df_danger_patterns.loc[n, date] += bulletin_no_regions

                for pattern in patterns:
                    n = patterns.index(pattern)
                    df_danger_patterns.loc[n, date] = 100 * df_danger_patterns.loc[n, date] / no_regions

        # - Cumulative dataframe required for bar plots (to include bottom argument) - #
        df_danger_patterns_cum=df_danger_patterns.cumsum()
        i=np.arange(1,len(patterns))
        df_danger_patterns_cum.loc[i]=df_danger_patterns_cum.loc[i-1].values
        df_danger_patterns_cum.loc[0,:] = 0
        df_danger_patterns_cum=df_danger_patterns_cum.T
        df_danger_patterns=df_danger_patterns.T
        
        return df_danger_patterns



    def get_bulletin_distribution(self,local_dir,start_season,end_season,regions_of_interest):
        """
        Collects the distribution of bulletins with respect to their regions. Bulletins are sorted based
        on the regions they affect.
        """
        day_range = pd.date_range(start=start_season,end=end_season).to_pydatetime().tolist()
        day_range = [d.date() for d in day_range]
        tasks = ['reports','blogs_ty', 'blogs_bz', 'blogs_tn', 'virtual_training', 'field_training']
        df_tasks = pd.DataFrame(0, index=day_range, columns=tasks)

        dict_reports = {'AT07'          : 0,
                        'IT32TN'        : 0,
                        'IT32BZ'        : 0,
                        'IT32TN_BZ'     : 0,
                        'AT07_IT32BZ'   : 0,
                        'AT07_IT32TN'   : 0,
                        'AT07_IT32TN_BZ': 0}

        EUREGIO_REGIONS = ['AT-07', 'IT-32-BZ', 'IT-32-TN']

        for file in os.listdir(local_dir):
            if "de" in file:
                bulletins = Bulletins()
                with open(local_dir + '/' + file) as fp:
                    # clear_output(wait=True)
                    # print(file)
                    if ".xml" in file:
                        root = ET.parse(fp)
                        bulletins.bulletins = parse_xml(root)
                    if ".json" in file:
                        data = json.load(fp)
                        try:
                            bulletins.from_json(data)
                        except:
                            print("Can't work with JSON file:", file)
                #date = bulletins.main_date()
                tmp = file[:10]
                date = datetime.datetime.strptime(tmp.split('_')[0], "%Y-%m-%d").date()
                if date <= datetime.datetime.strptime(end_season, "%Y-%m-%d").date() and date >= datetime.datetime.strptime(start_season, "%Y-%m-%d").date():
                    df_tasks.loc[date,'reports'] = len(bulletins.bulletins)
                    for bulletin in bulletins.bulletins:
                        regions_included = np.zeros(len(EUREGIO_REGIONS))
                        for region in bulletin.get_region_list():
                            for i,entry in enumerate(EUREGIO_REGIONS):
                                if region.startswith(entry):
                                    regions_included[i] = 1
                        
                        if sum(regions_included) == 3:
                            dict_reports['AT07_IT32TN_BZ'] += 1 
                        
                        elif sum(regions_included) == 2:
                            if   regions_included[1] == 1 and  regions_included[2] == 1:
                                dict_reports['IT32TN_BZ']   += 1
                            elif regions_included[0] == 1 and  regions_included[1] == 1:
                                dict_reports['AT07_IT32BZ'] += 1
                            elif regions_included[0] == 1 and  regions_included[2] == 1:
                                dict_reports['AT07_IT32TN'] += 1 
                            
                        elif sum(regions_included) == 1:
                            if   regions_included[0] == 1:
                                dict_reports['AT07']   += 1
                            elif regions_included[1] == 1:
                                dict_reports['IT32BZ'] += 1
                            elif regions_included[2] == 1:
                                dict_reports['IT32TN'] += 1


        df_reports = pd.DataFrame(dict_reports,index=['count','freq'])
        if df_tasks.loc[:,'reports'].sum() == df_reports.loc['count',:].sum():
            print('Number of reports matches count: {} reports collected'.format(df_tasks.loc[:,'reports'].sum()))
        else:
            print('Number of reports does not match - CHECK!!')
            
        reports_total = df_tasks.loc[:,'reports'].sum()
        df_reports.loc['freq',:] = 100 * df_reports.loc['count',:] / reports_total
        # df_tasks.loc[:,'reports'] = df_tasks.loc[:,'reports'].replace({0:np.nan})

        return df_reports, df_tasks


    def check_regions_of_interest(self, x, regions_of_interest):
        # for region in x:
        for region in regions_of_interest:
            if x.startswith(region):
                return True
        return False

    def print_inventory(self):
        print(self.macro_regions)
        print(self.micro_regions)

    def get_macro_regions(self):
        return list(self.macro_regions.items())