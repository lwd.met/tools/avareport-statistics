from avastats.eaws_regions import EAWSRegions
from avastats.avacore_connector import AvacoreConnector

import datetime
import numpy as np
import pandas as pd

if __name__ == "__main__":
    print('--- LWD Triol Stats Module - Welcome ---\n')
    print('➤ Step 1. Calculate neighobrhood relations from EAWS regions files')
    eaws_regions = EAWSRegions()
    print('➤ Step 2. Check files provided in /data/bulletins folder for CAAML .json files.')
    connector =  AvacoreConnector()
    macro_regions = connector.get_macro_regions()
    print('Available macro regions in provided files are:')
    for i, region in enumerate(macro_regions):
        macro_id, validity = region
        print(f" - [{i:<2}]: {macro_id:<12} from {validity[0].strftime('%Y-%m-%d')} to {validity[1].strftime('%Y-%m-%d')}")
    selection = input('Select regions to take, number seperatet by space: >')
    selected_regions = selection.split(" ")

    min_day = datetime.date.today()
    max_day = datetime.date(1, 1, 1)

    # select min and max of available data of current selected regions

    for region_index in selected_regions:
        region_index = int(region_index)
        _, validity = macro_regions[region_index]
        if validity[0].date() < min_day:
            min_day = validity[0].date()
        if validity[1].date() > max_day:
            max_day = validity[1].date()
    
    delta_days = max_day - min_day

    print('Selected max period:' , min_day, max_day, delta_days)

    day_range = pd.date_range(start=min_day,end=max_day).to_pydatetime().tolist()
    
    # d_max = np.array(no_micro_regions, len(day_range))
    # d_max = np.full((no_micro_regions, len(day_range)), -1, dtype=np.int16)
    # d_morning = np.full((no_micro_regions, len(day_range)), -1, dtype=np.int16)
    d_max = []
    d_morning = []
    micro_regions_axes = []

    # print(d_max.shape, d_morning.shape)

    for region_index in selected_regions:
        macro_id, _ = macro_regions[int(region_index)]
        d_max_n, d_morning_n, micro_regions_axes_n = connector.get_dangerlevels_for_region(macro_id, day_range)
        d_max.append(d_max_n)
        d_morning.append(d_morning_n)
        micro_regions_axes.append(micro_regions_axes_n)


    print('Read out danger ratings. Calculate Matrix.')

    
    for region_index in selected_regions:
        # macro_id, _ = macro_regions[int(region_index)]
        pass
        # for micro_region in 
    
    
    print(len(d_max))
    for idx, d_max_elem in enumerate(d_max):
        # pass
        print(len(d_max_elem), d_max_elem, micro_regions_axes[idx])
    print('end')
    

"""
How is this going to work?
- Create neighborhood relations (x)
- fetch jsons from input folder and check regions and seasons where data is available
(currently only jsons, later also caaml-files? Or option for caaml pre processing?) (x)
- select regions and season TBD
- Calculate Dmax and Dmorning: (x)
-- If Value available> Take rating, if no Rating or no snow, take 0, if no bulletin take -1. (x)
- 
"""

"""
Not yet considered from Paper:
- Size of Region
- Max Height of Region by DEM 90 x 90
"""