from bs4 import BeautifulSoup
from urllib.request import urlopen
import datetime
import requests
from urllib.error import HTTPError

def download_from_index(url, local_dir, file_ext = None, start = None, end = None, details=""):
    page = requests.get(url).text
    soup = BeautifulSoup(page, 'html.parser')
    
    if start != None and end != None:
        start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
        end = datetime.datetime.strptime(end, "%Y-%m-%d").date()

    
    for node in soup.find_all('a'):
        keep = True
        _url = url + node.get('href')[2:]
        
        if any(x in node.get('href') for x in ["./2"]):
                  
            if 'albina_euregio' in details:
                s_date = node.get('href').replace('/', '').replace('.', '')
                date_n = datetime.datetime.strptime(s_date, "%Y-%m-%d").date()
                
                # naming changed from season 2022/23 on
              
                # if date_n < datetime.date(2020, 12, 4):
                #     filename_extension = "_de_CAAMLv6.xml"
                # elif date_n < datetime.date(2022, 12, 7):
                #     filename_extension = "_de.xml"
                if date_n < datetime.date(2022, 12, 7):
                     filename_extension = "_de.xml"
                else:
                    filename_extension = "_EUREGIO_de.xml"

                ## if 'restrict' in details:
                if date_n < end and date_n > start:
                    keep = True
                else:
                    keep = False
           
            if 'tyrol_archive' in details:
                filename = "AT-07_" +node.get('href')
                if 'en' in filename:
                    keep = False
    
            if keep:
                try:
                    print("Try to download "+_url+s_date+filename_extension)
                    with urlopen(_url+s_date+filename_extension) as http, open(
                            f"{local_dir}/{s_date}{filename_extension}", mode="wb"
                        ) as f:
                            f.write(http.read())
                except HTTPError:
                    print("ERROR: Unable to download "+_url+s_date+filename_extension)
                    # filename_extension = "_de.xml" 
                    # print("Try again to download "+_url+s_date+filename_extension)
                    # with urlopen(_url+s_date+filename_extension) as http, open(
                    #         f"{local_dir}/{s_date}{filename_extension}", mode="wb"
                    #     ) as f:
                    #         f.write(http.read())
                    
                    
                    
                    