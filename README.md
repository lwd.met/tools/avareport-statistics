# Avareport_statistics

<p float="center">
  <img src="./avastats/figures/season-summary.png" width="95%" />
</p>

This project contains multiple subfolders for the statistical analysis of the avalanche report data-set.

## Avastats
Avastats is organized within the notebook in the subfolder. The data download is included in the notebook, so no data preparation is needed. The basic concept is, to generate a local folder with the requested reports (original files as .xml or .json). Those files are processed using the avastats module utilizing pyAvaCore (submodule) which can process CAAML bulletins of the EUREGIO and most bulletins of other warning services in Europe. Avastats generates the plot above (danger level distribution, avalanche problems, danger patterns, number of reports). 

- warnregionen_auswertung
	- Statistische Auswertungen der Warnregionen: warning regions per day, sum of warning days/regions, daily median, boxplot, bar charts 

## Public
- danger-levels-for-season-report
	- For Jahresbericht:  create table of maximum DangerRating and AvalancheProblems per Day for each micro region
- map_animation
	- Animated video of danger rating map

## Relevant for forecasters
- eaws_matrix_usage (EAWS Matrix)
	- How often a field was selected during the complete season in the ALBINA region
- neighbor_regions
	- Erweiterung von euregio_neighbours for complete Austria, Bavaria and Euregio
- satzkatalog_auswertung
	- Frequencies of usage of Satzbausteinen.

For a more detailed description of each analysis you can find a readme in the related subfolder.


