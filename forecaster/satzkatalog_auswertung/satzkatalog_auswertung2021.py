"""
Created on Mon Nov 11 14:53:07 2019

@author: Alexander Kehl

"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import pdb

# Local imports
from satzkatalog_auswertung import all_langues, names_it

#%% User input
# Column name fore the current season
col_nam2021 = 'freq2021'

input_dir = 'input'
sentence_path = 'Satzkatalog_Rohdaten.xlsx'
ava_report_path = 'avalanche_report_statistics_2021_processed.csv'

#%%
# Data processing of analysis season 19-20
names_it.set_index('name', inplace=True)
names_it.drop(labels=['sentence_id', 'language'], axis=1, inplace=True)
# Drop the unsed sentence "joker"
names_it = names_it[:-5]

all_langues.set_index('name', inplace=True)
# Keep only frequency of season 1920
all_langues.drop(labels=['name_2', 'header'], axis=1, inplace=True)

#%%
# Auswertung Satzkatalog; Daten einladen
data_sentence = pd.read_excel(os.path.join(input_dir, sentence_path),
                              sheet_name='Sentence IDs', engine='openpyxl')

# Season 2021
df = pd.read_csv(os.path.join(input_dir, ava_report_path),
                 sep=';', header=0, low_memory=False)

# TODO: Optional.Ask Christoph if only updates should be considered
# If an update was released, keep latest release
# df_season.drop_duplicates(subset=['Date', 'Daytime', 'Region', 'Subregion'], keep='last', inplace=True)

# Only look at aggregated subregions
df = df.drop_duplicates(subset='BulletinId')

#%%

# The input dataset df has no sorted columns, search for strings in all columns
str_cols = [col for col in df if 'str' in dir(df[col])]
df_filter = df[str_cols]

# Furhter search for sentences in columns
mask = np.column_stack(
    [df[col].str.contains(r'\[\{\"curlyName\"\:', na=False)
     for col in df_filter])

# Get Dataframe with columns with sentences only
df_filt_sent = df_filter.mask(~mask).dropna(axis=1, how='all')

# List of cells with sentences
sentence_cells = [df_filt_sent[col].dropna().values for col in df_filt_sent]
# Make a nicer format
sentence_cells = np.concatenate(sentence_cells).flatten()

# List of sentences
lst = []

for string in sentence_cells:

    # Sentence keywords start with one of the two phrases
    split1 = string.split('[{"curlyName":"')[1:]
    split2 = string.split(',{"curlyName":"')[1:]

    # Find and save the sentence keyword
    for it, splitstr in enumerate(split1):
        lst.append(split1[it][:split1[it].find('"')])
    for it, splitstr in enumerate(split2):
        lst.append(split2[it][:split2[it].find('"')])

# Raw result of sentence counts 2021
counts2021 = pd.Series(lst).value_counts()
counts2021 = counts2021.rename(col_nam2021)
# Merge results 2021 with results from 1920 and italian names
counts = pd.concat([counts2021, names_it, all_langues], axis=1)

# Translate sentences introduced in season 20/21
counts.loc['Neuschnee03', 'header'] = 'Neve fresca 03'
counts.loc['Neuschnee04', 'header'] = 'Neve fresca 04'
counts.loc['Schneedecke03', 'header'] = 'Manto nevoso 03'
counts.loc['Schneedecke04', 'header'] = 'Manto nevoso 04'
counts.loc['Schneedecke05', 'header'] = 'Manto nevoso 05'
counts.loc['Schneedecke06', 'header'] = 'Manto nevoso 06'
counts.loc['Schneedecke07', 'header'] = 'Manto nevoso 07'
counts.loc['Schneedecke08', 'header'] = 'Manto nevoso 08'
counts.loc['Schneedecke09', 'header'] = 'Manto nevoso 09'
counts.loc['Änderung10', 'header'] = 'Cambiamento 10'

# Selection of sentence counts
counts_geq2 = counts[counts.loc[:, col_nam2021] >= 2]
counts_geq25quant = counts[
    counts.loc[:, col_nam2021] >= counts.loc[:, col_nam2021].quantile(0.25)]
counts_seq_mean = counts[
    counts.loc[:, col_nam2021] <= counts.loc[:, col_nam2021].mean()]
counts_geq_mean = counts[
    counts.loc[:, col_nam2021] >= counts.loc[:, col_nam2021].mean()]
counts_rel = counts.loc[:, ['freq2021', 'freq']] / counts.loc[:, ['freq2021', 'freq']].sum()
counts_rel = pd.concat([counts_rel, counts.header], axis=1)


#%%

# Plots
# activate latex text rendering

def bar_plot(fig_size, series, fname, cols2plot=['freq2021', 'freq'],
             legend=['20/21', '19/20'], relative=False):


    # TODO: documentation

    fig, ax = plt.subplots(figsize=fig_size)

    # x locations for the groups
    ind = np.arange(len(series))
    # Width of the bars
    width = 0.35

    rects1 = ax.bar(ind, series.loc[:,cols2plot[0]], width=width,
           label=cols2plot[0])

    if relative:
        ax.set_ylabel('Yearly relative frequency')
    else:
        ax.set_ylabel('Counts')
    ax.set_xlabel('Sentence')
    ax.tick_params(axis='both', which='major', labelsize=6)

    # Create axis for italian names
    ax2 = ax.twiny()

    for axs in [ax, ax2]:
        axs.set_xticks(ind + width / 2)

    ax.set_xticklabels(series.index,rotation=90)

    ax2.set_xlim(ax.get_xlim())
    ax2.set_xticklabels(series.header,rotation=90,size=2)
    ax2.tick_params(axis='both', which='major', labelsize=6)

    # FIXME solve compatibility for more than 2 seasons
    #Idea:
    # Create empty list for rects/ax which will be appended in loop
        # for col, leg in zip(cols2plot, legend):
        # Create axis
        # Challenge: get width of bars and position of bars right for n>=1 seasons
    if (len(series.columns) > 2):
        rects2 = ax.bar(ind + width, series.loc[:,cols2plot[1]], width=width,
           label=cols2plot[1]) # Plots
        ax.legend((rects1[0], rects2[0]), legend, loc='upper right')
    else:
        ax.legend(rects1, [legend], loc='upper right')

    output = 'output'
    fname_pdf = (fname + '.pdf')
    fname_png = (fname + '.png')
    path_pdf = os.path.join(output, fname_pdf)
    path_png = os.path.join(output, fname_png)
    # If you want to save the figures, uncomment the lines below
    # fig.savefig(path_pdf, format='pdf', dpi=1000, bbox_inches='tight')
    # fig.savefig(path_png, format='png', dpi=1000, bbox_inches='tight')

    return(None)

#%%

if __name__ == '__main__':
    # Do the plots
    # Set the figure size in order to read the x-tick labels nicely
    bar_plot((17,2), counts, 'Satz_auswertung_2021_all')
    bar_plot((16,2), counts_geq2, 'Satz_auswertung_2021_geq2')
    bar_plot((12,2), counts_geq25quant,'Satz_auswertung_2021_geq25quant')
    bar_plot((12,2), counts_seq_mean, 'Satz_auswertung_2021_seq_mean')
    bar_plot((10,2), counts_geq_mean, 'Satz_auswertung_2021_geq_mean')

    #Season2021 only
    bar_plot((17,2), counts.loc[:,['freq2021', 'header']],
              'Satz_auswertung_2021_all_1season',
              cols2plot=['freq2021'],legend=['20/21'])

    # Make a relative frequency plot for the complete dataset only
    bar_plot((17,2), counts_rel, 'Satz_auswertung_2021_all_relative',
             relative=True)

    #######
    # Horizontal Bar plot
    # In June 2021 Christoph Mitterer asked for a horizontal bar plot.
    # But finally I had no time to finish it properly.

    # fig, ax = plt.subplots(figsize=(2,12))
    # ax.barh(counts.index, pd.Series(lst).value_counts().values) # Plots
