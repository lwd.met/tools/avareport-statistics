# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 09:33:31 2021

@author: Alexander Kehl
"""

import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

# Import data

# import data as dataframe
path_input = 'input'
path_output = 'output'
path_folder = os.path.join('data', '2011-2015')
path = 'avalanche_report_AT-07_BY_sort_by_date_2011-2015.csv'
path_2 = 'neighboursBY-TY.csv'


df_tech = pd.read_csv(os.path.join('data', 'techel_paper', 'data_dmax.csv'), sep=";", index_col=0, parse_dates=True)

df_tech.replace({'warningRegion': {'bay_allgau': 'BYALL', 'bay_ammer': 'BYAMM',
                                  'bay_voralp': 'BYBVA', 'bay_werden': 'BYWFK',
                                  'bay_chiemg': 'BYCHG'}}, inplace=True)

df_tech_by = df_tech.groupby('forecastCenter').get_group('BAY')
df_tech_by = df_tech_by[df_tech_by.warningRegion != 'bay_bercht']


###### VERSION ALEXANDER (MaxDanger per DAY)
df2_stats = pd.read_csv(os.path.join(path_folder, 'BY-TY_maxDR_2011-2015.csv'),
                  header=0, index_col=0, parse_dates=True)
df2_stats.rename(columns={'MaxPerDay':'max_danger'}, inplace=True)
#######

# TYROL example
# df2_ty = df2_stats.groupby('SubregionCode').get_group('AT-07-02')
# df_tech_ty_ = df_tech.groupby('warningRegion').get_group('R1_arlberg')

# fig, ax = plt.subplots()
# ax.plot(df2_ty.max_danger, '.-', label='TY Alex')
# ax.plot(df_tech_ty_.dangerLevelMax, '.-', label='TY techel')
# plt.legend()

# Bavaria
for subreg in df_tech_by.warningRegion.value_counts().index:

    df2_by = df2_stats.groupby('SubregionCode').get_group(subreg)
    df_tech_by_ = df_tech_by.groupby('warningRegion').get_group(subreg)

    fig, ax = plt.subplots()
    ax.plot(df2_by.max_danger, '.-', label='BY Alex')
    ax.plot(df_tech_by_.dangerLevelMax, '.-', label='BY techel')
    plt.legend()
    plt.title(f'{subreg}')
