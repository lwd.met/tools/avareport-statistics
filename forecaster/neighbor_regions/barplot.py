# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 17:58:33 2021

Hasty barplot to compare DangerRatings in Bavaria (BY) and Tyrol (TY) for
different avalanche problems.

@author: Alexander Kehl
"""

import pandas as pd
import numpy as np
import os
import pdb
import matplotlib.pyplot as plt
import matplotlib as mpl

files = ['barplot_for_ava_probs_DangerRatingBelow',
         'barplot_for_ava_probs_DangerRatingAbove',
         'barplot_for_ava_probs_MaxPerDay']

for file in files:


    df = pd.read_csv(os.path.join('output', file + '.csv'))


    ##### Make corrections of table from .R script

    if file.endswith('Below'):

        df.columns = np.arange(0,5)

        df.loc[1, 3:4] = np.nan
        df.loc[2, 2:] = np.nan
        df.loc[3:,4] = np.nan

        df.columns = ['TY greater 2 BY', 'TY greater 1 BY',
                'equal', 'BY greater 1 TY', 'BY greater 2 TY']



    if file.endswith('Above'):

        df.columns = np.arange(0,4)

        df.loc[[1,2,4], 3] = np.nan
        df.loc[2, 2] = np.nan

        df.columns = ['TY greater 2 BY', 'TY greater 1 BY', 'equal', 'BY greater 1 TY']


    if file.endswith('Day'):

        df.columns = np.arange(0,4)

        df.loc[[1,2,4], 3] = np.nan
        df.loc[2, 2] = np.nan

        df.columns = ['TY greater 2 BY', 'TY greater 1 BY', 'equal', 'BY greater 1 TY']


    df.index = ["wind_drifted_snow", "new_snow", "persistent_weak_layers",
              "wet_snow", "gliding_snow"]




    df = df.T

    if not file.endswith('Below'):
        df.loc['BY greater 2 TY'] = np.nan


    ###### Plot

    ax = df.plot.bar(rot=45, title=file)
    ax.legend(ncol=2, columnspacing=0.5)
    ax.set_ylabel('Counts')
    ax.set_ylim(0, 600)
    plt.tight_layout()
    # plt.savefig(file + '.jpeg')