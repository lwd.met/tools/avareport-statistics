# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 15:08:44 2021

Parses the JSON files from the albina homepage for neighbour regions.

@author: Alexander Kehl
"""

import os
import json
import requests
import pandas as pd
import numpy as np
import pdb
import time
from bs4 import BeautifulSoup
import warnings
import itertools



def listFD(url, ext=''):
    """
    Example:
    url = 'https://lawinen.report/albina_neighbors/'
    ext = 'AT-05.json' # Salzbourg
    urls = listFD(url, ext)"""
    page = requests.get(url).text
    soup = BeautifulSoup(page, 'html.parser')
    return [url + '/' + node.get('href') for node in soup.find_all('a') if node.get('href').endswith(ext)]


def custom_formatwarning(msg, *args, **kwargs):
    # ignore everything except the message
    return str(msg) + '\n'


def json_source(regionID, **kwargs):
    """
    Reads multiple json files from file or downloads from URL.


    Parameters
    ----------
    regionID : str
        The region ID of the region of interest, e.g. AT-07.
    **kwargs :
        mypath : str
            Relative folder path of json files.
        url : str
            URL adress where json files are stored.

    Returns
    -------
    all_lists: list of lists.
    cols: names for each list in all_lists.

    """
    ext = regionID + '.json'


    for key, value in kwargs.items():
        if key == 'mypath':
            # Json files
            data_source = [os.path.join(value, f) for f in os.listdir(value)
                         if os.path.isfile(os.path.join(value, f))
                         if f.endswith(ext)]
            is_files = True
        elif key == 'url':
            # List of all URLS
            data_source = listFD(value, ext)
            is_files = False
        else:
            raise NameError(f'{key} is not a valid keyword argument.')

    return(data_source, is_files)

def json2lst(regionID, **kwargs):
    """
    Parses parameters from multiple json files and writes them to a list.


    Parameters
    ----------
    regionID : str
        The region ID of the region of interest, e.g. AT-07.
    **kwargs :
        mypath : str
            Relative folder path of json files.
        url : str
            URL adress where json files are stored.

    Returns
    -------
    all_lists: list of lists.
    cols: names for each list in all_lists.

    """

    # Columns for DataFrame
    cols = ['Date', 'Daytime', 'Region', 'Subregion','DangerRatingBelow',
            'DangerRatingAbove', 'DangerRatingElevation', 'AvalancheProblem1',
            'AvalancheProblem1AspectN', 'AvalancheProblem1AspectNE',
            'AvalancheProblem1AspectE', 'AvalancheProblem1AspectSE',
            'AvalancheProblem1AspectS', 'AvalancheProblem1AspectSW',
            'AvalancheProblem1AspectW', 'AvalancheProblem1AspectNW']

    # Create empty lists for writing json values to them
    date_lst = []
    daytime_lst = []
    reg = []
    subreg = []
    dr_below = []
    dr_abov = []
    dr_elev = []
    # First avalanche problem
    ava_prob = []
    ava_prob_elev_below = []
    ava_prob_elev_abov = []
    asp_n = []
    asp_ne = []
    asp_e = []
    asp_se = []
    asp_s = []
    asp_sw = []
    asp_w = []
    asp_nw = []
    # Second avalanche problem
    ava_prob2 = []
    ava_prob_elev_below2 = []
    ava_prob_elev_abov2 = []
    asp_n2 = []
    asp_ne2 = []
    asp_e2 = []
    asp_se2 = []
    asp_s2 = []
    asp_sw2 = []
    asp_w2 = []
    asp_nw2 = []

    data_source, is_files = json_source(regionID, **kwargs)
    # Loop over all json's
    for source in data_source:
        # OPTIMIZE get json keys automatically (ca):
        #     keys = []
        #     1. keys.append(data.keys()), ev. list
        #     2. for key in data.keys():
        #         values = data[0][list(data[0].keys())[key]][0]
        #         if type(values) == dict:
        #             keys.append(values.keys())
        # Standard keys for json
        time_key = 'validity_begin'
        valid_reg_key = 'valid_regions'
        prob_array_key = 'problem_list'
        danger_key = 'danger_main'
        main_val_key = 'main_value'
        elev_key = 'valid_elevation'
        prob_key = 'problem_type'

        # Open json files
        if is_files == True:
            # Read json from file
            with open(source, 'r') as myfile:
                json_data=myfile.read()
            # Parse file
            data = json.loads(json_data)
        else:
            # OPTIMIZE download all jsons for BY and VBG because reading json from file is faster than from URL
            # Read json from URL
            data = requests.get(source).json()
            # Special json keys due to change from 2021-01-25 to 2021-01-26
            if os.path.split(source)[1][:10] == '2021-01-25':
                time_key = 'timeBegin'
                valid_reg_key = 'validRegions'
                prob_array_key = 'problemList'
                danger_key = 'dangerMain'
                main_val_key = 'mainValue'
                elev_key = 'validElev'
                prob_key = 'type'
        # pdb.set_trace()

        # Loop through main levels of json (= (merged) subregions)
        for i in range(0, len(data)):
            try:
                full_date = data[i][time_key]
            except KeyError:
                time_key = 'timeBegin'
                valid_reg_key = 'validRegions'
                prob_array_key = 'problemList'
                danger_key = 'dangerMain'
                main_val_key = 'mainValue'
                elev_key = 'validElev'
                prob_key = 'type'


                full_date = data[i][time_key]
            # One of the two option should work
            # date = full_date[:10]               # extract date from json key
            date = source.split('\\')[-1][:10]  # extract date from file name

            # Szbg: several subregions are merged to a bigger region -> use array
            # Note: BY and Vbg season 20/21: subregions are always seperate ->
            #   thus length of array = 1
            subr_arr = data[i][valid_reg_key]
            # In BY only one problem -> list index always 0
            prob_lst = data[i][prob_array_key][0]


            subreg.append(subr_arr)
            for _ in range(len(subr_arr)):
                date_lst.append(date)

            # Danger rating dict
            danger_dict = data[i][danger_key]


            # Loop over subregions
            for subr in subr_arr:

                # Reports start at different times for each state
                hour = full_date[11:13]
                if len(danger_dict) <= 2:
                    if hour in ('00', '07', '22', '23'):
                        daytime = 'AM'
                    elif hour in ('10', '11', '12'):
                        daytime = 'PM'
                    elif hour in ('17', '08'):
                        daytime = 'AM'
                        warnings.warn(f'Converted hour={hour} to daytime "AM" in "{source}" for {subr} with inferred Date:{date}.')
                    else:
                        warnings.warn(f'Can not convert hour={hour} to daytime "AM/PM" in "{source}" for {subr} with inferred Date:{date}.')
                        daytime = np.nan

                # Loop over danger ratings keys
                for it, dic in enumerate(danger_dict):

                    # BY special case
                    # This report has an error for the danger rating,
                    # do it by hand.
                    if (date == '2021-01-28') & (region == 'BY'):
                        if it == 1:
                            if daytime == 'AM':
                                dr_below.append(2)
                            else:
                                dr_below.append(3)
                        else:
                            if subreg != 'BYALL':
                                dr_abov.append(3)
                            else:
                                if daytime == 'AM':
                                    dr_abov.append(3)
                                else:
                                    dr_abov.append(4)
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_elev.append(1500)
                            ava_prob.append('wind_drifted_snow')
                            # asp_n.append(1)
                            # asp_ne.append(1)
                            # asp_e.append(1)
                            # asp_se.append(1)
                            # asp_s.append(1)
                            # asp_sw.append(0)
                            # asp_w.append(0)
                            # asp_nw.append(0)

                    # Vbg special case #1
                    # This report has an error for the danger rating,
                    # do it by hand.
                    elif (date == '2021-01-26') & (
                            (subr == 'AT-08-02') | (subr == 'AT-08-03')):
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_below.append(2)
                            dr_abov.append(3)
                            dr_elev.append(1800)
                            ava_prob.append('new_snow')
                            # ava_prob2.append('wind_drifted_snow')
                            # # Problem 1 aspects
                            # for lst in [asp_n, asp_ne, asp_e, asp_se, asp_s,
                            #             asp_sw, asp_w, asp_nw]:
                            #     lst.append(1)
                            # # Problem 2 aspects
                            # asp_n2.append(1)
                            # asp_ne2.append(1)
                            # asp_e2.append(1)
                            # asp_se2.append(1)
                            # asp_s2.append(0)
                            # asp_sw2.append(0)
                            # asp_w2.append(0)
                            # asp_nw2.append(1)

                    # Vbg special case #2
                    # This report has an error for the danger rating,
                    # do it by hand.
                    elif (date == '2021-01-27') & (
                            (subr == 'AT-08-01') |
                            (subr == 'AT-08-02') | (subr == 'AT-08-03')):
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_below.append(2)
                            dr_abov.append(3)
                            dr_elev.append('treeline')
                            ava_prob.append('wind_drifted_snow')
                            # ava_prob2.append('persistent_weak_layers')
                            # # Problem 1 aspects
                            # asp_n.append(1)
                            # asp_ne.append(1)
                            # asp_e.append(1)
                            # asp_se.append(1)
                            # asp_s.append(0)
                            # asp_sw.append(0)
                            # asp_w.append(0)
                            # asp_nw.append(1)
                            # # Problem 2
                            # for lst in [asp_n, asp_ne, asp_nw]:
                            #     lst.append(1)
                            # for lst in [asp_e, asp_se, asp_s,
                            #             asp_sw, asp_w]:
                            #     lst.append(0)

                    # Vbg special case #3
                    # This report has an error for the danger rating,
                    # do it by hand.
                    elif (date in ('2021-02-27', '2021-02-28')) & (
                            region == 'AT-08') & (daytime == 'PM'):
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_below.append(2)
                            dr_abov.append(1)
                            if subr == 'AT-08-01':
                                dr_elev.append(2000)
                            else:
                                dr_elev.append(2400)

                    # Vbg special case #4
                    # This report has an error for the danger rating,
                    # do it by hand.
                    elif (date in ('2021-03-14', '2021-03-15')) & (
                            region in 'AT-08'):
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_below.append(2)
                            dr_abov.append(3)
                            if (date == '2021-03-14') & (
                                    subr in ('AT-08-04', 'AT-08-05', 'AT-08-06')):
                                dr_elev.append(180)
                            else:
                                dr_elev.append('treeline')

                    # Vbg special case #5
                    # This report has an error for the danger rating,
                    # do it by hand.
                    elif (date == '2021-03-16') & (subr in ('AT-08-02', 'AT-08-03',
                                                            'AT-08-04')):
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_below.append(3)
                            dr_abov.append(4)
                            dr_elev.append('treeline')
                            # ava_prob.append('new_snow')
                            # ava_prob2.append('wind_drifted_snow')

                    # Vbg special case #6
                    # This report has an error for the danger rating,
                    # do it by hand.
                    elif (date == '2021-03-17') & (subr in ('AT-08-02', 'AT-08-03')):
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_below.append(3)
                            dr_abov.append(4)
                            dr_elev.append('treeline')
                            # ava_prob.append('new_snow')
                            # ava_prob2.append('wind_drifted_snow')

                    # Vbg special case #7
                    # This report has an error for the danger rating,
                    # do it by hand.
                    elif (date == '2021-03-31') & (subr == 'AT-08-06') & (
                            daytime == 'PM'):
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_below.append(3)
                            dr_abov.append(2)
                            dr_elev.append(3000)
                            # ava_prob.append('persistent_weak_layers')
                            # ava_prob2.append('wet_snow')

                    # Vbg special case #8
                    # This report has an error for the danger rating,
                    # do it by hand.
                    elif (date == '2021-04-01') & (subr == 'AT-08-06') & (
                            daytime == 'PM'):
                        # Add other parameters once per day for this special day
                        if it == 0:
                            dr_below.append(3)
                            dr_abov.append(2)
                            dr_elev.append(2800)
                            # ava_prob.append('persistent_weak_layers')
                            # ava_prob2.append('wet_snow')


                    # Treat the cases when everything is correct
                    else:
                        dr = dic[main_val_key]
                        elev = dic[elev_key]
                        if elev is not None:
                            if elev.startswith('>'):
                                dr_abov.append(dr)
                            else:
                                dr_below.append(dr)
                            if it in (0, 2):
                                dr_elev.append(elev[1:])
                        else:
                            if it == 0:
                                dr_abov.append(dr)
                                dr_below.append(dr)
                                dr_elev.append(np.nan)
                            else:
                                dr_below.append(dr)
                        # Add other parameters once per day
                        if it == 0:
                            ava_prob.append(prob_lst[prob_key])

                # Append daytime to list
                daytime_lst.append(daytime)



    # TODO: write loop for ava problems
    ava_prob = []

    subreg_flat = list(itertools.chain(*subreg))
    # List of all variables (later put into a pd.df)
    all_lst = [date_lst, daytime_lst, reg, subreg_flat, dr_below, dr_abov,
                dr_elev, ava_prob, asp_n, asp_ne, asp_e, asp_se, asp_s, asp_sw,
                asp_w, asp_nw]

    return(cols, all_lst)


def lst2df(cols, all_lst):
    data_len = len(all_lst[0])
    df_ava = pd.DataFrame(columns=cols)

    for col, lst in zip (cols, all_lst):
        if len(lst) == 0:
            nans = np.empty(data_len)
            nans[:] = np.nan
            df_ava[col] = nans
        else:
            df_ava[col] = lst

    return(df_ava)


def repl_str(df):
    df.replace(
        to_replace =['drifting snow'], value ='wind_drifted_snow', inplace=True)
    df.replace(
        to_replace =['wet snow'], value ='wet_snow', inplace=True)
    df.replace(
        to_replace =['gliding snow'], value ='gliding_snow', inplace=True)
    df.replace(
        to_replace =['new snow'], value ='new_snow', inplace=True)
    df.replace(
        to_replace =['old snow'], value ='persistent_weak_layers', inplace=True)
    return(df)


if __name__ == '__main__':
    warnings.formatwarning = custom_formatwarning
    # ext = 'BY.json' # Bavaria
    # ext = 'AT-08.json' # Vorarlberg
    # ext = 'AT-05.json' # Salzbourg
    url_albina = 'https://lawinen.report/albina_neighbors/'

    # Loop over regions. Possibilities see README.md
    for region in ['AT-05']:
        # For regions with json files stored on web server
        if region in (''):
            df_ava = lst2df(*json2lst(region, url=url_albina))
        # For regions with json files stored locally
        else:
            df_ava = lst2df(*json2lst(region, mypath='data_json'))

    yr_start = df_ava.Date.iloc[0][:4]
    yr_end = df_ava.Date.iloc[-1][:4]
    # In case the report started in January and not in November/December as usual
    if df_ava.Date[0][5:7] == '01':
        yr_start = str(int(yr_start) - 1)


    df_ava = repl_str(df_ava)
    df_ava.to_csv(os.path.join(
        'data', ''.join((f'{yr_start}-{yr_end}_avalanche_report_',
                          region, '.csv'))))



