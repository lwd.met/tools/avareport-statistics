# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 12:03:54 2021

Merges and processes avalanche reports from Tyrol and Bavaria
(and Salzbourg, Vorarlberg for season 2020/2021).
For seasons 2019/2020 and before, I extracted the borders Tyrol-Bavaria here already.


@author: Alexander Kehl
"""

import os
import pandas as pd
import numpy as np
import pdb
import warnings

# Local imports
from functions import repl_str_with_val

# Columns for the published data set
# ??? Add  'Region', 'Subregion',
COLS = ['Daytime', 'SubregionCode', 'DangerRatingBelow',
       'DangerRatingAbove', 'DangerRatingElevation', 'AvalancheProblem1']
#%%

######## SEASON 2020/21 START TYROL, BAVARIA; SALZBOURG; VORARLBERG


# Columns of 2020/21 Dataframe
cols = ['Date', 'Daytime', 'Region', 'Subregion','DangerRatingBelow',
        'DangerRatingAbove', 'DangerRatingElevation', 'AvalancheProblem1']

# Read Data
ava_ty = pd.read_csv(
    os.path.join('data', '2020-2021_avalanche_report_TY.csv'),
    parse_dates=True, usecols=cols, index_col='Date')

ava_by_json = pd.read_csv(
    os.path.join('data', '2020-2021_avalanche_report_BY_since_2021-01-26.csv'),
    parse_dates=True, usecols=cols, index_col='Date')

ava_by_manual = pd.read_csv(
    os.path.join('data', '2020-2021_avalanche_report_BY_until_2021-01-26_manual.csv'),
    parse_dates=True, usecols=cols, index_col='Date')

ava_szbg = pd.read_csv(os.path.join('data', '2020-2021_avalanche_report_AT-05.csv'), parse_dates=True, usecols=cols, index_col='Date')
ava_kar = pd.read_csv(os.path.join('data', '2020-2021_avalanche_report_AT-02.csv'), parse_dates=True, usecols=cols, index_col='Date')

ava_vbg_json = pd.read_csv(os.path.join('data', '2020-2021_avalanche_report_AT-08_json.csv'), parse_dates=True, usecols=cols, index_col='Date')

ava_vbg_manual = pd.read_csv(os.path.join('data', '2020-2021_avalanche_report_AT-08_manual.csv'), parse_dates=True, usecols=cols, index_col='Date')



# Processing

ava_by_json = ava_by_json.set_index([ava_by_json.index, 'Subregion', 'Daytime']).sort_index()
# VBG has one NaN row - Alexander double checked it with the pdf's on the homepage.
# I dont know where it comes from, it can be dropped
ava_vbg_json.dropna(subset=['Subregion'], inplace=True)




# LWD Bavaria did not release daily report in last days of April, but was valid
# several days
missing_dates = pd.date_range('2021-04-24', '2021-04-29', freq='D')[[0,1,2,3,5]]
missing_dates2 = pd.date_range('2021-05-01', '2021-05-02', freq='D')
# Add them to the report. OPTIMIZE: this loop isnt pyhtonic
for date in missing_dates:
    for val, subreg, daytime in zip(
            ava_by_json.loc['2021-04-23'].values,
            [l for k, l, m in ava_by_json.loc['2021-04-23'].index],
            [m for k, l, m in ava_by_json.loc['2021-04-23'].index]):
        ava_by_json.loc[(date, subreg, daytime)] = val
        # Keep index lexsorted to guarantee slicing in loop is working
        ava_by_json = ava_by_json.sort_index()
for date in missing_dates2:
    for val, subreg, daytime in zip(
            ava_by_json.loc['2021-04-30'].values,
            [l for k, l, m in ava_by_json.loc['2021-04-30'].index],
            [m for k, l, m in ava_by_json.loc['2021-04-30'].index]):
        ava_by_json.loc[(date, subreg, daytime)] = val
        # Keep index lexsorted to guarantee slicing in loop is working
        ava_by_json = ava_by_json.sort_index()

# Convert back to index for concatenation
ava_by_json.reset_index(level=['Subregion', 'Daytime'], inplace=True)



# Merge avas
ava_by = pd.concat([ava_by_manual, ava_by_json]).sort_index()

# Replace DangerRating string with value
ava_ty = repl_str_with_val(ava_ty)




# Merge region and subregion of Tyrol for later handling in .R script
ava_ty['Subregion'] = ava_ty['Subregion'].astype(str)
ava_ty['Subregion'] = ava_ty['Subregion'].str.zfill(2)
ava_ty['SubregionCode'] = ava_ty[['Region', 'Subregion']].agg('-'.join, axis=1)
ava_vbg_manual['SubregionCode'] = ava_vbg_manual[['Region', 'Subregion']].astype('str').agg('-0'.join, axis=1)

ava_by['SubregionCode'] = ava_by['Subregion']
ava_szbg['SubregionCode'] = ava_szbg['Subregion']
ava_kar['SubregionCode'] = ava_kar['Subregion']
ava_vbg_json['SubregionCode'] = ava_vbg_json['Subregion']

# Merge reports from several countries
avas_merged = pd.concat([ava_ty, ava_by, ava_szbg, ava_kar, ava_vbg_json, ava_vbg_manual])

# Unify notation
avas_merged.replace({'Treeline': 'treeline'}, inplace=True)
# Unify columns for publication
avas_merged = avas_merged.loc[:,COLS]

# Save merged table
avas_merged.sort_index().to_csv(os.path.join('data', '2020-2021_avalanche_report_euregio_neighbours.csv'))

######## SEASON 2020/21 END


#%%

####### SEASONS 2019-2020 START

season = '2019-2020'

path_folder = 'data'
fname_ty = f'{season}_avalanche_report_TY.csv'
fname_by = f'{season}_avalanche_report_BY.csv'
cols = ['Date', 'Daytime', 'Region', 'Subregion','DangerRatingBelow',
        'DangerRatingAbove', 'DangerRatingElevation', 'AvalancheProblem1']

# Load data
df_ty = pd.read_csv(os.path.join(path_folder, fname_ty), sep=';', header=0,
                    index_col=0, usecols=cols, parse_dates=True, dayfirst=True)
df_by = pd.read_csv(os.path.join(path_folder, fname_by),
                    index_col=0, parse_dates=True, dayfirst=True)

# Merge region and subregion of Tyrol for later handling in .R script
df_ty['Subregion'] = df_ty['Subregion'].astype(str)
df_ty['Subregion'] = ['0' + k if len(k)==1 else k for k in df_ty['Subregion']]
df_ty['SubregionCode'] = df_ty[['Region', 'Subregion']].agg('-'.join, axis=1)

# Process data

# Replace DangerRating string with value
df_ty = repl_str_with_val(df_ty)
df_by.rename(columns = {'Subregion':'SubregionCode'}, inplace = True)


df_merge = pd.concat([df_by, df_ty]).sort_index()

# Unify notation
df_merge.replace({'Treeline': 'treeline'}, inplace=True)
# Drop PM if AM=PM
df_merge = df_merge.reset_index().sort_values(
    by=['Date', 'Daytime']).drop_duplicates(
        subset=['Date', 'SubregionCode', 'DangerRatingBelow',
                'DangerRatingAbove', 'DangerRatingElevation'],
        keep='first')
# Drop first versions after update was released
df_merge = df_merge.drop_duplicates(
    subset=['Date', 'Daytime', 'SubregionCode'],
    keep='last')
# Set index as Datetime
df_merge.set_index('Date', inplace=True)
# Unify columns for publication
df_merge = df_merge.loc[:,COLS]

# Save to csv
df_merge.to_csv(os.path.join('data', f'{season}_avalanche_report_euregio_neighbours.csv'))

###### SEASONS 2019-2020 END



#%%



###### SEASONS 2018-2019 START

season = '2018-2019'

path_folder = 'data'
fname_ty = 'avalanche_report_TY_2006-12_bis_2020_02_neue_Regionen.csv'
fname_by = f'{season}_avalanche_report_BY.csv'
cols = ['Date', 'Daytime', 'Code', 'DangerRatingBelow',
        'DangerRatingAbove', 'DangerRatingElevation']

# Load data
df_ty = pd.read_csv(os.path.join(path_folder, fname_ty), sep=';',
                    index_col=0, parse_dates=True, dayfirst=True,
                    usecols=cols, encoding='cp1252')
df_by = pd.read_csv(os.path.join(path_folder, fname_by),
                    index_col=0, parse_dates=True, dayfirst=True)

# Process data

# Replace DangerRating string with value
df_ty = repl_str_with_val(df_ty)

df_ty.rename(columns = {'Code':'SubregionCode'}, inplace = True)
df_by.rename(columns = {'Subregion':'SubregionCode'}, inplace = True)


# TY: has always an dr_elevation even if dr_above = dr_below
condition_no_elev = df_ty.DangerRatingAbove == df_ty.DangerRatingBelow
df_ty.DangerRatingElevation = np.where(condition_no_elev,
                                        np.nan, df_ty.DangerRatingElevation)

df_merge = pd.concat([df_by, df_ty]).sort_index()

# Unify notation
df_merge.replace({'Treeline': 'treeline'}, inplace=True)
# Drop PM if AM=PM
df_merge = df_merge.reset_index().sort_values(
    by=['Date', 'Daytime']).drop_duplicates(
        subset=['Date', 'SubregionCode', 'DangerRatingBelow',
                'DangerRatingAbove', 'DangerRatingElevation'],
        keep='first')
# Drop first versions after update was released
df_merge = df_merge.drop_duplicates(
    subset=['Date', 'Daytime', 'SubregionCode'],
    keep='last')
# Set index as Datetime
df_merge.set_index('Date', inplace=True)

# Unify columns for publication
col_missing = [k for k in COLS if k not in df_merge.columns]
for col in col_missing:
    df_merge[col] = np.nan
df_merge = df_merge.loc[:,COLS]


# Save to csv
df_merge.to_csv(os.path.join('data', f'{season}_avalanche_report_euregio_neighbours.csv'))

###### SEASONS 2018-2019 END




#%%
####### SEASONS 2011-2015
# Techel data for Non-Tyrol/Bavaria

# Load data
df_techel = pd.read_csv(os.path.join('data', 'techel_paper', 'data_dmax.csv'),
                        sep=";", index_col=0, parse_dates=True)


# Filter for Tyrol neighbors except BAY (which I parsed directly and more
# accurate than Techel from the Homepage)
ty_neighbor = ['SAL', 'VOR', 'KAE', 'BOL', 'SWI'] # ??? include Switzerland 'SWI'

df_techel = pd.concat([df_techel.groupby('forecastCenter').get_group(k)
                       for k in ty_neighbor])

# Drop unneccesary columns
df_techel.drop(columns = {'forecastCenter', 'country'}, inplace=True)
# And rows if no DangerRating was issued
df_techel.dropna(subset=['dangerLevelMax'], inplace=True)
idx = df_techel.dangerLevelMax != 0
df_techel = df_techel[idx]
#%%

# Rename warning regions to ISO-code
df_techel.replace(
    {'warningRegion':
      {'1_bregenz': 'AT-08-01', '2_allgaeu': 'AT-08-02', '3_raetik': 'AT-08-05',
       '4_silvret': 'AT-08-06', '5_verwall': 'AT-08-04', '6_lechqu': 'AT-08-03',
       'sb_hohTau': 'AT-05-05', 'sb_innGras': 'AT-05-13', 'sb_niedTau': 'AT-05-02',
       'sb_nockb': 'AT-05-01', 'sb_nordalp': 'AT-05-17', 'sb_osterh': 'AT-05-19',
        '1kaernt': 'AT-02-01', '2kaernt': 'AT-02-09', '3kaernt': 'AT-02-10',
        '4kaernt': 'AT-02-14', '5kaernt': 'AT-02-04', '6kaernt': 'AT-02-05',
        '7kaernt': 'AT-02-17', '8kaernt': 'AT-02-06',
       '1_wVinschg': 'IT-32-BZ-01', '2_wAHK': 'IT-32-BZ-02', '3_zAHK': 'IT-32-BZ-07',
       '4_sarntal':'IT-32-BZ-08', '5_oAHK': 'IT-32-BZ-05',
       '6_hochpust': 'IT-32-BZ-12', '7_oDolom': 'IT-32-BZ-19',
       '8_zDolom': 'IT-32-BZ-18', '9_wDolom': 'IT-32-BZ-17',
       '10_nonsber': 'IT-32-BZ-16', '11_ortler': 'IT-32-BZ-14',
       }},
        inplace=True)

df_techel.rename(
    columns={'dangerLevelMax':'MaxPerDay', 'warningRegion': 'SubregionCode'},
    inplace=True)
df_techel.rename_axis('Date', inplace=True)

# Old regs are keys, new regs in values
old_new_regs = {'AT-05-05': ['AT-05-06', 'AT-05-07', 'AT-05-09', 'AT-05-10', 'AT-05-11'],
                'AT-05-13': ['AT-05-14', 'AT-05-15'],
                'AT-05-02': ['AT-05-03', 'AT-05-04', 'AT-05-08', 'AT-05-12'],
                'AT-05-17': ['AT-05-16', 'AT-05-18', 'AT-05-20', 'AT-05-21'],
                'AT-02-01': ['AT-02-02', 'AT-02-03'],
                'AT-02-06': ['AT-02-07', 'AT-02-08'],
                'AT-02-10': ['AT-02-11', 'AT-02-12', 'AT-02-13'],
                'AT-02-14': ['AT-02-15', 'AT-02-16'],
                'AT-02-17': ['AT-02-18', 'AT-02-19'],
                'IT-32-BZ-14': ['IT-32-BZ-15'],
                'IT-32-BZ-02': ['IT-32-BZ-03', 'IT-32-BZ-06'],
                'IT-32-BZ-07': ['IT-32-BZ-04'],
                'IT-32-BZ-05': ['IT-32-BZ-09', 'IT-32-BZ-10', 'IT-32-BZ-11'],
                'IT-32-BZ-12': ['IT-32-BZ-13'],
                'IT-32-BZ-19': ['IT-32-BZ-20']
                }


# Create rows for new regions
for key, lst in zip(old_new_regs.keys(), old_new_regs.values()):
    df_techel = df_techel.append(pd.concat(
        [df_techel.groupby('SubregionCode').get_group(key).replace(key, k)
         for k in lst]))


# Sort by Date
df_techel = df_techel.sort_index()
# Periods used in Techel paper
start = ['2011-12-16', '2012-12-14', '2013-12-14', '2014-12-14']
stop = ['2012-04-16', '2013-04-16', '2014-04-16', '2015-04-16']


# Split the data into single years
df_techel1112 = df_techel.loc[start[0]:stop[0]]
df_techel1213 = df_techel.loc[start[1]:stop[1]]
df_techel1314 = df_techel.loc[start[2]:stop[2]]
df_techel1415 = df_techel.loc[start[3]:stop[3]]

# Save data
df_techel1112.to_csv(os.path.join('data', '2011-2012_avalanche_report_techel.csv'))
df_techel1213.to_csv(os.path.join('data', '2012-2013_avalanche_report_techel.csv'))
df_techel1314.to_csv(os.path.join('data', '2013-2014_avalanche_report_techel.csv'))
df_techel1415.to_csv(os.path.join('data', '2014-2015_avalanche_report_techel.csv'))

#%%
#######

path_folder = 'data'
fname_ty = 'avalanche_report_TY_2006-12_bis_2020_02_neue_Regionen.csv'
fname_by = '2011-2015_avalanche_report_BY.csv'
cols = ['Date', 'Daytime', 'Code', 'DangerRatingBelow',
        'DangerRatingAbove', 'DangerRatingElevation']

# Load data
df_ty = pd.read_csv(os.path.join(path_folder, fname_ty), sep=';', usecols=cols,
                    index_col=0, parse_dates=True, dayfirst=True, encoding='cp1252')
df_by = pd.read_csv(os.path.join(path_folder, fname_by),
                    index_col=0, parse_dates=True, dayfirst=True)




# Process data

# Extract periods when report was released
df_ty = df_ty.loc['2011-12-16': '2015-05-01']


# Replace DangerRating string with value
df_ty = repl_str_with_val(df_ty)

df_ty.rename(columns = {'Code':'SubregionCode'}, inplace = True)
df_by.rename(columns = {'Subregion':'SubregionCode'}, inplace = True)


# TY: has always an dr_elevation even if dr_above = dr_below
condition_no_elev = df_ty.DangerRatingAbove == df_ty.DangerRatingBelow
df_ty.DangerRatingElevation = np.where(condition_no_elev,
                                        np.nan, df_ty.DangerRatingElevation)
# Drop PM if AM=PM
df_ty = df_ty.reset_index().sort_values(
    by=['Date', 'Daytime']).drop_duplicates(
        subset=['Date', 'SubregionCode', 'DangerRatingBelow',
                'DangerRatingAbove', 'DangerRatingElevation'],
        keep='first')

# Unify index
if 'Date' in df_by.columns:
    df_by.set_index('Date', inplace=True)
df_ty.set_index('Date', inplace=True)

# Merge reports
df_merge = pd.concat([df_by, df_ty]).sort_index()

# Unify notation
df_merge.replace({'Treeline': 'treeline'}, inplace=True)
# Drop first versions after update was released
df_merge = df_merge.reset_index().drop_duplicates(
    subset=['Date', 'Daytime', 'SubregionCode'],
    keep='last')
# Set index as Datetime
df_merge.set_index('Date', inplace=True)
# Unify columns for publication
col_missing = [k for k in COLS if k not in df_merge.columns]
for col in col_missing:
    df_merge[col] = np.nan
df_merge = df_merge.loc[:,COLS]


# Save to csv
df_merge.to_csv(os.path.join('data', '2011-2015_avalanche_report_BY_TY.csv'))

# Periods when report was released in BAY/TIR
start = ['2011-12-15', '2012-12-07', '2013-12-02', '2014-12-19']
stop = ['2012-05-01', '2013-05-01', '2014-05-01', '2015-05-03']
# Sort by Date
df_merge = df_merge.sort_index()

# Split the data into single years
df_merge1112 = df_merge.loc[start[0]:stop[0]]
df_merge1213 = df_merge.loc[start[1]:stop[1]]
df_merge1314 = df_merge.loc[start[2]:stop[2]]
df_merge1415 = df_merge.loc[start[3]:stop[3]]

# Save data
df_merge1112.to_csv(os.path.join('data', '2011-2012_avalanche_report_euregio_neighbours.csv'))
df_merge1213.to_csv(os.path.join('data', '2012-2013_avalanche_report_euregio_neighbours.csv'))
df_merge1314.to_csv(os.path.join('data', '2013-2014_avalanche_report_euregio_neighbours.csv'))
df_merge1415.to_csv(os.path.join('data', '2014-2015_avalanche_report_euregio_neighbours.csv'))


######## SEASONS 2011-2015 (Techel) END