#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
One season and one avalanche report

Created on Mon Aug 24 16:39:40 2020

User input:
    regions
    path to df_stats (see Pagree_df_max_BY.py)
    use same periods as in Techel paper (see Pagree_df_max_BY.py)

@author: Julius Loos, modified by Alexander Kehl
"""

import pandas as pd
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import seaborn as sns
from geopandas import GeoDataFrame # OPTIMIZE replace with gpd.GeoDataFrame
from shapely.geometry import Point, LineString
import shapely
from math import log
from sklearn.neighbors import NearestNeighbors
import networkx as nx
from collections import OrderedDict
import os
import re
import pdb
from operator import add
from osgeo import ogr
from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg


# Local imports
from Pagree_df_max_BY import df_all
from user_input import use_tech_time, season, use_elev_th, elev_id, sfx, regions, prob_sfx, tree_sfx

# Formatting
sns.set_palette('colorblind')

path_layer = os.path.join('data', 'map_layers')

# input
df_raw = pd.read_csv(os.path.join('data','Outlines_AT-DE-Albina.csv'),sep=',')
# Load map layers and convert them to lon/lat coordinate system
layer_lakes = gpd.read_file(os.path.join(path_layer, 'stehende_Gewaesser.shp')).to_crs("EPSG:4326")
# layer_places = gpd.read_file(os.path.join(path_layer, 'places.shp')).to_crs("EPSG:4326")
# layer_river = gpd.read_file(os.path.join(path_layer, 'Riversegments.shp')).to_crs("EPSG:4326")
# layer_waterways = gpd.read_file(os.path.join(path_layer, 'gis_osm_waterways_free.shp')).to_crs("EPSG:4326")

# Extract regions of interest
df_raw = pd.concat([group for (name, group) in df_raw.groupby(['RegionCode'])
                if any(x in name for x in regions)]).sort_index()

df_raw['X1'] = df_raw['X1'].map(lambda x: '%.4f' % x).astype(float)
df_raw['X2'] = df_raw['X2'].map(lambda y: '%.4f' % y).astype(float)

df_raw = df_raw.drop_duplicates(subset=['X1','X2','RegionCode'])

# get points
df_raw = df_raw.drop(df_raw.columns[[0]],axis=1)
df_comb = df_raw.drop(['RegionCode'],axis=1)
df_comv = df_comb.values.tolist()

# transform points to Geodataframe
geometry = [Point(xy) for xy in zip(df_raw.X1,df_raw.X2)]
df_geoframe = GeoDataFrame(df_raw.RegionCode,geometry=geometry)

# groupby region
df_grouped = df_geoframe.groupby(['RegionCode'])['geometry'].apply(lambda x: LineString(x.tolist()))
df = GeoDataFrame(df_grouped, geometry='geometry').set_crs("EPSG:4326")

# group after points
df_areas_grouped = df_raw.groupby(['X1','X2']).agg(lambda column: "__".join(column)).reset_index()

#rearange
df_aggs = df_areas_grouped.groupby(['RegionCode'])[['X1','X2']].apply(lambda x: x.values.tolist())
df_aggs = df_aggs.reset_index()

# sort after original output.csv
df_aggs.columns = ['RegionCode','Vals']


# OPTIMIZE Idea: Finding Neighbourhood Relation (see below) might work better if the polygon points are closer togehter!?
# def segmentize(geom):
#     wkt = geom.wkt  # shapely Polygon to wkt
#     geom = ogr.CreateGeometryFromWkt(wkt)  # create ogr geometry
#     geom.Segmentize(2)  # densify geometry
#     wkt2 = geom.ExportToWkt()  # ogr geometry to wkt
#     new = shapely.wkt.loads(wkt2)  # wkt to shapely Polygon
#     return new

# ??? also use for df_raw (?)
# df_grouped = df_grouped.geometry.map(segmentize)

#%%
sub_str = '__'
def str_join(x, y):
    return(sub_str.join((x, y)))

#%% Hasty handle case with points shared by 4 regions

# # Handling points shared by 4 regions introduces bad plots (start and end
# # point of line not correct), but lines might not go until the border
# idx_4shared_points = [True if (len(re.findall(sub_str, k)) == 3) else False
#                       for k in df_aggs.RegionCode]

# shared_points = pd.concat([df_aggs[idx_4shared_points]] * 4,
#                       ignore_index=True).sort_values('RegionCode').reset_index().Vals
# index_shared = [(str_join(l, m), str_join(l, n), str_join(l, o), str_join(m, n),
#                     str_join(m, o), str_join(n, o)) for l, m, n, o in
#                 df_aggs[idx_4shared_points].RegionCode.str.split('__')]
# index_shared = np.array(index_shared).flatten()
# df_shared = pd.concat([pd.Series(index_shared), shared_points], axis=1)
# df_shared.rename(columns={0: 'RegionCode'}, inplace=True)#.reset_index(inplace=True)


# # Drop rows with points shared by 4 regions
# df_aggs = df_aggs[np.where(idx_4shared_points, False, True)]


# df_aggs = pd.concat([df_aggs, df_shared])
# # Add points to shared by 4 regions to borders of 2 regions
# df_aggs= df_aggs.groupby('RegionCode').sum(numeric_only=False).reset_index()


#%% Append points shared by 3 regions to border of 2 regions
# Index of 3 regions sharing one point using count of substrings
idx_shared_points = [True if (len(re.findall(sub_str, k)) == 2) else False
                      for k in df_aggs.RegionCode]

shared_points = pd.concat([df_aggs[idx_shared_points]] * 3,
                      ignore_index=True).sort_values('RegionCode').reset_index().Vals
index_shared = [(str_join(l, m), str_join(l, n), str_join(m, n)) for l, m, n in
                df_aggs[idx_shared_points].RegionCode.str.split('__')]
index_shared = np.array(index_shared).flatten()
df_shared = pd.concat([pd.Series(index_shared), shared_points], axis=1)
df_shared.rename(columns={0: 'RegionCode'}, inplace=True)#.reset_index(inplace=True)

# Drop rows with points shared by 3 regions
df_aggs = df_aggs[np.where(idx_shared_points, False, True)]

df_aggs = pd.concat([df_aggs, df_shared])

# Add points shared by 3 regions to borders of 2 regions
# Set "numeric_only" for compability betweem pandas version <1.3.0 and 1.3.0-1.3.2 (or higher?!)
# see: https://github.com/pandas-dev/pandas/issues/42395#issuecomment-882070234
df_aggs = df_aggs.groupby('RegionCode').sum(numeric_only=False).reset_index()

#%%

# In Austria (apart from Tyrol) some values are zero or nan
df_aggs.Vals.replace(0, np.nan, inplace=True)
df_aggs.dropna(subset=['Vals'], inplace=True)
df_aggs.reset_index(inplace=True)

df_all_2 = []
for m in range(df_aggs.Vals.size):
    # Get points of border regions
    df_bool_1 = [i for i in df_comv if i in df_aggs['Vals'][m]]
    df_all_2.append(df_bool_1)


# set up for geodataframe
df_neighbours_test = pd.DataFrame({0:df_all_2})
df_aggs_2 = pd.concat([df_aggs['RegionCode'], df_neighbours_test], axis=1)

# merge areas
df_merge = df_all['V1'] + '__' + df_all['V2']
df_merge = df_merge.to_frame()
df_merge.columns = ['merged']

# index and fit
list_merge = df_merge['merged'].to_list()
df_bool = df_aggs_2['RegionCode'].isin(list_merge)
df_fit = df_aggs_2.loc[df_bool]
# OPTIMIZE use cond in Pagree to make loop faster and create fit without df_merge/df_all like this: df_fit = df_aggs_2.loc[np.where(np.array([len(re.findall('__', k)) for k in df_aggs_2.RegionCode]) == 1, True, False)]
df_fit.columns = ['RegionCode','Lines']
df_fit = df_fit.reset_index(drop=True)

## kick duplicates (repeat)
list_org = []
for i in range(len(df_fit)):

    res = list(OrderedDict((tuple(x),x)for x in df_fit.Lines[i]))
    list_org.append(res)

# set up for geodataframe
df_neighbours_test_2 = pd.DataFrame({0:list_org})
df_fit = df_fit.drop(['Lines'], axis=1)
df_aggs_3 = pd.concat([df_fit['RegionCode'], df_neighbours_test_2], axis=1)

# index and fit
list_merge = df_merge['merged'].to_list()
df_bool = df_aggs_3['RegionCode'].isin(list_merge)
df_fit = df_aggs_3.loc[df_bool]
df_fit.columns = ['RegionCode','Lines']
df_fit = df_fit.reset_index(drop=True)

#%%
# make points to lines
region_points = []
all_points = []
counts = []
lines = []
new_regions = []

#loop to arange data
for i in range(len(df_fit.Lines)):
    region_points = []
    num = len(df_fit.Lines.iloc[i])
    new = df_fit.RegionCode.iloc[i]

    # threshold for points. This number interacts with the keyword n_neighbors
    # in this line -> clf = NearestNeighbors(n_neighbors=5).fit(points)
    # Skip if border line has less than 2 points
    if num < 2:
        continue

    for j in range(num):
        test =  Point(df_fit.Lines.iloc[i][j])

        region_points.append(test)

    # make list of points
    all_points.append(region_points)
    new_regions.append(new)

# little loop for converting points to lines in shapely format
for i in all_points:
    lines.append(LineString(i))

#%% Find intersection between border points and polygons

# Get boolean index of borders with more than two points
idx = [True if (len(k) >= 2) else False for k in df_fit.Lines]

df_borders = pd.DataFrame(
    {'RegionCode': df_fit.RegionCode[idx], 'points': all_points})

border_regions = [k.split('__') for k in new_regions]
border = []

# OPTIMIZE write functions here

for regs, points in zip (border_regions,df_borders.points):
    reg1, reg2 = regs

    # The code works for borders, but not for this one.
    if (('BYBVA' in regs) and  ('AT-07-04' in regs)):
        border.append(np.nan)
        continue

    test = []
    test2 = []
    for i in points:
        try:
            x, y = shapely.ops.split(df_grouped.loc[reg1], i)
            reg = reg1
            match = True
        except ValueError:
            match = False
            break
        # if match:
        test.append(y)
        test2.append(x)

    if not match:
        test = []
        test2 = []
        for i in points:
            try:
                x, y = shapely.ops.split(df_grouped.loc[reg2], i)
                reg = reg2
                match = True
            except ValueError:
                match = False
                break
        # if match:
            test.append(y)
            test2.append(x)

    if match:
        x = test[0]
        y = test2[0]
        for i, j in zip(test[1:], test2[1:]):
            x = x.intersection(i)
            y = y.intersection(j)

        border.append(df_grouped.loc[reg] - x - y)
    else:
        border.append(np.nan)

df_borders['Lines'] = border

#%% nearest neighbour for interpolation of linestrings (get start and end point correct)



list_itrp = []

for k in range(len(lines)):

    x,y = zip(*list(lines[k].coords))

    x = np.asarray(x)
    y = np.asarray(y)

    points = np.asarray(np.c_[x, y])

    # Adjusting the keyword n_neighbors affects if the lines are plotted correctly.
    # I tried values between 3-10, values greater 10 yield an error and 5 looked best.
    # Making sure the function does not through an error by defining this keyword
    n = 5 if (len(points) > 6) else (len(points) - 1)
    clf = NearestNeighbors(n_neighbors=n).fit(points)
    G = clf.kneighbors_graph()

    T = nx.from_scipy_sparse_matrix(G)

    paths = [list(nx.dfs_preorder_nodes(T, i)) for i in range(len(points))]

    mindist = np.inf
    minidx = 0

    for i in range(len(points)):
        p = paths[i]           # order of nodes
        ordered = points[p]    # ordered nodes
        # find cost of that order by the sum of euclidean distances between points (i) and (i+1)
        cost = (((ordered[:-1] - ordered[1:])**2).sum(1)).sum()
        if cost < mindist:
            mindist = cost
            minidx = i

    opt_order = paths[minidx]


    xx = x[opt_order]
    yy = y[opt_order]

    point_xy = [Point(xy) for xy in zip(xx,yy)]
    linestring_xy = LineString(point_xy)
    list_itrp.append(linestring_xy)
#%%


# TODO handle FutureWarning: The input object of type 'LineString' is an array-like implementing one of the corresponding protocols (`__array__`, `__array_interface__` or `__array_struct__`); but not a sequence (or 0-D). In the future, this object will be coerced as if it was first converted using `np.array(obj)`. To retain the old behaviour, you have to either modify the type 'LineString', or assign to an empty array created with `np.empty(correct_shape, dtype=object)`.
border_merge = np.where(df_borders.Lines.isna(), np.array(list_itrp, dtype='object'), np.array(border, dtype='object'))


# Improved version (Intersection of points and "perfect" border AND Nearest Neighbor)
df_neighbours_org = GeoDataFrame(pd.DataFrame(data=border_merge, columns=['geometry']),
                                  geometry='geometry').set_crs("EPSG:4326")

# arange regions and pagree
new_pagree = pd.concat([df_merge, df_all.pAgree], axis=1)
# Sort alphabetically after region borders for linking pAgree value and border line correctly
new_pagree = new_pagree.sort_values('merged').reset_index(drop=True)

df_bool_new = new_pagree['merged'].isin(new_regions)
df_fit_new = new_pagree.loc[df_bool_new].copy()
# For Non-albina Austria some pagree values are calculated at least two times (?!), drop them
df_fit_new.drop_duplicates(subset=['merged'], inplace=True)

# Create percentage values for plot annotation
df_neighbours_org['pagree'] = (df_fit_new['pAgree'].reset_index()
                                * 100).pAgree.astype('int').astype('str')
#%%
# make four cases for line width
lwd = np.array([0.5, 1, 1.5, 2.5, 3]) * 2
labels = ['>80%', '70.1-80%', '60.1-70%', '21.1-60%']#, '<21%']
output_linewidth = [
    lwd[0] if v > 0.8 else lwd[1] if 0.8 >= v > 0.7
    else lwd[2] if 0.7 >= v > 0.6 else lwd[3] if 0.60 >= v > 0.21
    else lwd[4] for v in df_fit_new['pAgree']]

ref_cols = ['k'] + [k for k in sns.color_palette()[0:4]]
colors = [
    ref_cols[0] if v > 0.8 else ref_cols[1] if 0.8 >= v > 0.7
    else ref_cols[2] if 0.7 >= v > 0.6 else ref_cols[3] if 0.60 >= v > 0.21
    else ref_cols[4] for v in df_fit_new['pAgree']]

df_linewidth = pd.DataFrame(output_linewidth)
df_linewidth.columns = ['linewidth']

# Colors like in Techel 2018, Fig. 5
col_ty = '#fbc9a0' # Tyrol
col_by = '#90c7c5' # Bavaria
col_vbg = '#fedc95' # Vorarlberg
col_szbg = '#f4b9ab' # Salzbourg
col_kar = '#dd9ba9' # Carinthia
col_ooe = '#eaaab6' # Upper Austria
col_sty = '#faeaa2' # Styria
col_nie = '#e9a3a1' # Lower Austria
col_bz = '#92d5c0' # South Tyrol
col_tn = '#fdca83' # Trentino
col_ch = '#f9bb8c' # Switzerland
# Not included in face_col yet
col_liv = '#cdbd74' # Livigno
col_lom = '#df9cc1' # Lombardia
col_ven = '#fdda8c' # Venetia
col_fri = '#caeb9f' #Friaul

face_col = [col_ty if 'AT-07' in k else col_szbg if 'AT-05' in k
            else col_vbg if 'AT-08' in k else col_by if 'BY' in k
            else col_bz if 'BZ' in k else col_tn if 'TN' in k
            else col_kar if 'AT-02' in k else col_ch if 'CH' in k
            else col_sty if 'AT-06' in k else col_nie if 'AT-03' in k
            else col_ooe for k in df.index]
#%%
# plot layout
fig, ax = plt.subplots(figsize=(20,20))
# Plot polygons
df.plot(ax=ax,linewidth=0.2, facecolor = face_col, alpha=0.5)
# Plot border lines with variable linewidth and color
df_neighbours_org.plot(ax=ax, linewidth=output_linewidth, color=colors, alpha=0.5)
# Annotate Text
df_neighbours_org.apply(
    lambda x: ax.annotate(text=x.pagree,#+'%',
                          xy=x.geometry.centroid.coords[0],
                          # It is possible to use arrows from text to point xy
                          # xytext=map(add, x.geometry.centroid.coords[0], (0, 0.05)),
                          # arrowprops=dict(facecolor='black', shrink=0.05)
                          # ha='right', va='bottom',
                          bbox=dict(fc='white', pad=2),
                          fontsize='medium', wrap=True), axis=1)
# Annotate the RegionCodes (for identification/development only)
# df.reset_index().apply(
#     lambda x: ax.annotate(text=x.RegionCode, xy=x.geometry.centroid.coords[0],
#                           bbox=dict(fc='white', pad=2), fontsize='small'), axis=1)

# Get xlim and ylim of plot before plotting geographical layers which might be outside
y_low, y_up = ax.get_ylim()
x_low, x_up = ax.get_xlim()

# Plot geographical layers from albina base map (scope: AT or middle Europe)
layer_lakes.plot(ax=ax, alpha=0.7, color='grey')
# layer_places[layer_places.TYPE == 'city'].plot(ax=ax, alpha=0.7, color='k') # Note: Bregenz is a town
# layer_river.groupby('STRAHLER').get_group(3).plot(ax=ax, alpha=0.5, color='grey', linestyle='dashed')
# layer_river.groupby('STRAHLER').get_group(4).plot(ax=ax, alpha=0.5, color='grey', linestyle='dashed')
# layer_river.groupby('STRAHLER').get_group(5).plot(ax=ax, alpha=0.5, color='grey', linestyle='dashed')
# layer_waterways.plot(ax=ax) # This are too many rivers in total



# Formatting

# Legend
ref_lines = [mlines.Line2D([], [], color=i, linewidth=j) for i, j in
             zip(ref_cols, lwd[:-1])]
plt.legend(handles=ref_lines, labels=labels, loc='lower right', fontsize='x-large')

# Layout and Extent
ax.set_xlim(x_low, x_up)
ax.set_ylim(y_low, y_up)
plt.tight_layout()

# Check if Danger rating is filteres by avalanche problem
if len(prob_sfx) > 1:
    prob = prob_sfx[1:]
    # Load image of Avalanche Problem
    im_prob = mpimg.imread(os.path.join('data', 'images', f'{prob}.webp'))

    imagebox = OffsetImage(im_prob, zoom=0.5)
    # Draw box for image (not very elegant)
    ab = AnnotationBbox(imagebox, (x_up*0.982, y_up*0.996))#(x_up, y_up))#

    ax.add_artist(ab)
    # This is more elegant but does not work
    # rend = fig.canvas.get_renderer()

    # w, h, x, d =  imagebox.get_extent(rend)
    # fig.images.append(imagebox)
    # imagebox.set_offset((fig.bbox.xmax-w, fig.bbox.ymax))


reg_id = '_'.join(regions)
seasns = '_'.join(season)
fig.savefig(os.path.join('output', f'pagree_map_{seasns}_{reg_id}_{elev_id}MaxPerDay{sfx}{prob_sfx}{tree_sfx}'), dpi=fig.dpi, bbox_inches='tight')
# fig.savefig(os.path.join('output', 'reg_IDs2'))