# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 15:00:26 2021

Correct avalanche reports.

@author: Alexander Kehl
"""

import os
import pandas as pd
import pdb


# Load data
path_folder = os.path.join('data')
fnames = ['20' + str(k) + '-20' + str(k+1) + '_avalanche_report_BY'
          for k in range(11,15)]
paths = [os.path.join(path_folder, fname + '.csv') for fname in fnames]

df1 = pd.read_csv(paths[0])
df2 = pd.read_csv(paths[1])
df3 = pd.read_csv(paths[2])
df4 = pd.read_csv(paths[3])

#%%

# Correct data
df1.iloc[138, 5] = 'treeline'
df1.iloc[145:150, 5] = 'treeline'
df1.iloc[151:156, 5] = 'treeline'
df1.iloc[167:173, 5] = 'treeline'
df1.iloc[175:178, 5] = 'treeline'
df1.iloc[236:249, 5] = 'treeline'
df1.iloc[352, 5] = 'treeline'
df1.iloc[357:363, 5] = 'treeline'
df1.iloc[[364, 365, 367, 368, 370, 371, 373, 374], 5] = 'treeline'
df1.iloc[395:400, 5] = 1600
df1.iloc[436:442, 5] = 'treeline'
df1.iloc[522:526, 5] = 'treeline'
df1.iloc[[528, 531], 5] = 'treeline'
df1.iloc[657:662, 5] = 1800
df1.iloc[733:739, 5] = 2000
df1.iloc[745:751, 5] = 2000
df1.iloc[757:763, 5] = 2000
df1.iloc[763:770, 5] = 1800
df1.iloc[776:782, 5] = 1800
df1.iloc[784:790, 5] = 1800
df1.iloc[796:802, 5] = 1800
df1.iloc[808:814, 5] = 1800
df1.iloc[820:832, 5] = 1800
df1.iloc[844:849, 5] = 1800
df1.iloc[855, 5] = 1800
df1.iloc[856:858, 5] = 2000
df1.iloc[861:865, 5] = 1800
df1.iloc[871:877, 5] = 1800
df1.iloc[878:884, 5] = 2000
df1.iloc[884:892, 5] = 1800
df1.iloc[893:897, 5] = 1800
df1.iloc[914:920, 5] = 2000
df1.iloc[996:1002, 5] = 2000
df1.iloc[1026:1029, 5] = 'treeline'
df1.iloc[1031, 5] = 'treeline'

df2.iloc[121:141, 5] = 'treeline'
df2.iloc[147:153, 5] = 'treeline'
df2.iloc[178:196, 5] = 1800
df2.iloc[284, 5] = 'treeline'
df2.iloc[288:293, 5] = 'treeline'
df2.iloc[341:362, 5] = 'treeline'
df2.iloc[590:596, 5] = 'treeline'
df2.iloc[602:608, 5] = 'treeline'
df2.iloc[614:620, 5] = 'treeline'
df2.iloc[662:668, 5] = 1800
df2.iloc[686:689, 5] = 'treeline'
df2.iloc[691, 5] = 'treeline'
df2.iloc[752:758, 5] = 1600
df2.iloc[784:788, 5] = 'treeline'

df3.iloc[6:23, 5] = 'treeline'
df3.iloc[271:276, 5] = 'treeline'
df3.iloc[277:283, 5] = 'treeline'
df3.iloc[285:291, 5] = 'treeline'
df3.iloc[303:306, 5] = 'treeline'
df3.iloc[435:441, 5] = 'treeline'
df3.iloc[533:539, 5] = 2000
df3.iloc[[601, 604, 607], 5] = 2000
df3.iloc[653:659, 5] = 2200
df3.iloc[[661, 665, 666, 668, 669, 670], 5] = 2200
df3.iloc[677:683, 5] = 2300

df4.iloc[[363, 364, 365, 368, 372, 373, 375, 376],5] = 1800
df4.iloc[[377, 378, 380, 383, 385, 388, 390],5] = 1800
df4.iloc[464, 5] = 2000
df4.iloc[[777, 778, 779], 5] = 'treeline'
df4.iloc[782:789, 5] = 'treeline'


# Check if all "Uncertain" entries are removed
for df in [df1, df2, df3, df4]:

    df.set_index('Date', inplace=True)


    df = df[df.DangerRatingElevation == 'Uncertain']
    if len(df) > 0:
        print(df)
        raise(ValueError('Please remove all "Uncertain" entries and re-run script'))



# Save data
fnames2 = [k + '_processed.csv' for k in fnames]
paths2corr = [os.path.join(path_folder, fname) for fname in fnames2]

df1.to_csv(paths2corr[0])
df2.to_csv(paths2corr[1])
df3.to_csv(paths2corr[2])
df4.to_csv(paths2corr[3])



# Merge the data and save it
df_concat = pd.concat([df1, df2, df3, df4])
df_concat.to_csv(os.path.join(path_folder, '2011-2015_avalanche_report_BY.csv'))

