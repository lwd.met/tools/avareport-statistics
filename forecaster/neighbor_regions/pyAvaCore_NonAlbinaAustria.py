# -*- coding: utf-8 -*-
"""
    Copyright (C) 2021 Friedrich MÃ¼tschele and other contributors
    This file is part of pyAvaCore.
    pyAvaCore is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    pyAvaCore is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with pyAvaCore. If not, see <http://www.gnu.org/licenses/>.

    Some adaptation made by Alexander Kehl. Created on Tue Jul  6 11:57:55 2021
"""
from datetime import datetime
from urllib.request import urlopen
from pathlib import Path
import re
import json
import logging
import typing

from processor_caaml_NonAlbinaAustria import parse_xml

### XML-Helpers

def get_xml_as_et(url):

    '''returns the xml-file from url as ElementTree'''

    with urlopen(url) as response:
        response_content = response.read()
    try:
        try:
            import xml.etree.cElementTree as ET
        except ImportError:
            import xml.etree.ElementTree as ET
        if "VORARLBERG" in url.upper():
            root = ET.fromstring(response_content.decode('latin-1'))
        else:
            root = ET.fromstring(response_content.decode('utf-8'))
    except Exception as r_e:
        print('error parsing ElementTree: ' + str(r_e))
    return root

def get_reports(url='', cache_path=str(Path('cache')), from_cache=False):

    '''returns array of AvaReports for requested url'''

    # Assign this variable, so I do not have to modify the return statement
    provider = ''

    logging.info('Fetching %s', url)
    root = get_xml_as_et(url)
    reports = parse_xml(root)

    return reports, provider, url


def try_parse_datetime(datetime_string):

    '''try to parse a datetime from string with matching format'''

    try:
        r_datetime = datetime.strptime(datetime_string, '%Y-%m-%dT%XZ')
    except:
        try:
            r_datetime = datetime.strptime(datetime_string[:19], '%Y-%m-%dT%X') # 2019-04-30T15:55:29+01:00
        except:
            r_datetime = datetime.now()
    return r_datetime

### Data-Classes

class Problem:
    '''
    Defines a avalanche problem with aspect and elevation
    '''
    problem_type: str
    aspect: list
    valid_elevation: str

    def __init__(self, problem_type: str, aspect: list, validElev: str) -> None:
        self.problem_type = problem_type
        self.aspect = aspect
        self.valid_elevation = clean_elevation(validElev)

    def __str__(self):
        return "{'problem_type':'" + self.problem_type + "', 'aspect':" + str(self.aspect) + ", 'valid_elevation':'" \
            + self.valid_elevation + "'}"

    def __repr__(self):
        return str(self)

class DangerMain:
    '''
    Defines Danger-Level with elevation
    '''
    main_value: int
    valid_elevation: str

    def __init__(self, mainValue: int, validElev: str):
        self.main_value = mainValue
        self.valid_elevation = clean_elevation(validElev)

class ReportText:
    '''
    Defines a report text with type.
    '''
    text_type: str
    text_content: str

    def __init__(self, text_type: str, text_content="") -> None:
        self.text_type = text_type
        self.text_content = text_content

    def __str__(self):
        return "{'text_type':'" + self.text_type + "', 'text_content':" + self.text_content + "'}"

    def __repr__(self):
        return str(self)

class AvaReport:
    '''
    Class for the AvaReport
    '''
    report_id: str
    '''ID of the Report'''
    valid_regions: typing.List[str]
    '''list of Regions'''
    rep_date: datetime
    '''Date of Report'''
    validity_begin: datetime
    '''valid time start'''
    validity_end: datetime
    '''valid time end'''
    predecessor_id: str
    '''ID of first report (AM) if Report is e. g. a PM-Report'''
    danger_main: typing.List[DangerMain]
    '''danger Value and elev'''
    dangerpattern: typing.List[str]
    '''list of Patterns'''
    problem_list: typing.List[Problem]
    '''list of Problems with Sublist of Aspect&Elevation'''
    report_texts: typing.List[ReportText]
    '''All textual elements of the Report'''

    def __init__(self):
        self.valid_regions = []
        self.danger_main = []
        self.dangerpattern = []
        self.problem_list = []
        self.report_texts = []

    def cli_out(self):
        print('ââââââ AvaReport ', self.report_id, ' ââââââ')
        if hasattr(self, 'predecessor_id'):
            print('â This is PM-Report to: ', self.predecessor_id)
        print('â Report from:          ', self.rep_date)
        print('â Validity:             ', self.validity_begin, ' -> ', self.validity_end)
        print('â Valid for:')
        for region in self.valid_regions:
            print('â |- ', region)

        print('ââââââ Danger Rating')
        for danger_main in self.danger_main:
            if danger_main.valid_elevation != None:
                print('â ', danger_main.valid_elevation, ' -> : ', danger_main.main_value)
            else:
                print('â ', danger_main.main_value, ' in entire range')

        print('ââââââ Av Problems')
        for problem in self.problem_list:
            print('â Problem: ', problem.problem_type, ' Elevation: ', problem.valid_elevation, ' Aspects: ', problem.aspect)

        if len(self.dangerpattern)  > 0:
            print('ââââââ Danger Patterns')
            for dangerpattern in self.dangerpattern:
                print('â ', dangerpattern)

        print('ââââââ Av Texts (if not html or img)')
        for texts in self.report_texts:
            if texts.text_type != 'html_report_local' and texts.text_type != 'prone_locations_img' and \
                texts.text_type != 'html_weather_snow':
                print('â ', texts.text_type, ': ', texts.text_content)

        print('âââââââââââââââââââââââââââââââââââââââââââ')


class JSONEncoder(json.JSONEncoder):
    """JSON serialization of datetime"""
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        try:
            return obj.toJSON()
        except: # pylint: disable=bare-except
            return obj.__dict__


def clean_elevation(elev: str):
    '''
    Cleans up the elevation description. Should move to the XML-Parsers.
    '''
    if elev in ['', '-', 'ElevationRange_Keine H\u00f6hengrenzeHi']:
        return None
    elev = re.sub(r'ElevationRange_(.+)Hi', r'>\1', elev)
    elev = re.sub(r'ElevationRange_(.+)(Lo|Lw)', r'<\1', elev)
    elev = elev.replace('Forestline', 'Treeline')
    return elev
