# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 16:13:42 2021

Merge existing outlines and neighbourhood relation csv's (created by
Outlines.R and FindNeighboorhoodRelation.R).

@author: Alexande Kehl
"""

import geopandas as gpd
import pandas as pd
import os
import numpy as np
from shapely.geometry import Point, shape
# import fiona


def mk_geo(df):
    """ Transform coordinates to points and further to Geodataframe. """
    df = df.astype({'X1': float, 'X2': float})
    geometry = [Point(xy) for xy in zip(df.X1, df.X2)]
    df_geo = gpd.GeoDataFrame(df.RegionCode, geometry=geometry)
    return(df_geo)

#######################
# Points of polygons

# Load data
df_albina = pd.read_csv(os.path.join('data', 'Outlines.csv'))
df_at_by = pd.read_csv(os.path.join('data', 'Outlines_AT-DE.csv'))

# Process data
# Drop Tyrol because it is in both 'Outline.csv' datasets
idx = ~df_albina.RegionCode.str.contains('AT-07')
df_albina = df_albina[idx]
# Convert to georeferenced Dataframe
df_albina = mk_geo(df_albina)
df_at_by = mk_geo(df_at_by)

# Set current projection
df_at_by.set_crs(crs='EPSG:4326', inplace=True)
df_albina.set_crs(crs='EPSG:32632', inplace=True)
# Convert to Lat/Lon projection
df_albina.to_crs("EPSG:4326", inplace=True)


# Merge data Frames
df_geoframe = pd.concat([df_albina, df_at_by]).reset_index(drop=True)

# Process geo data
# df_geoframe.drop_duplicates(inplace=True)
# df_geoframe.reset_index(drop=True, inplace=True)
# Convert back to single values
geoms = [ shape(k) for k in df_geoframe.geometry]

list_arrays = [ np.array((geom.xy[0][0], geom.xy[1][0])) for geom in geoms ]

coord = pd.DataFrame(list_arrays, columns=['X1', 'X2'])

df_geo_coord = pd.concat([df_geoframe.RegionCode, coord], axis=1)

#######################
# Neighborhood relationsships

# Load data
neighbor_albina = pd.read_csv(os.path.join('data', 'neighboursALBINA.csv'), usecols=[1,2])
neighbor_at = pd.read_csv(os.path.join('data', 'neighbours_AT-DE.csv'), usecols=[1,2])

# Merge neighbors
neighbor_merge = pd.concat([neighbor_albina, neighbor_at], axis=0)
# Drop duplicates, e.g. TY-TY borders are in both datasets
neighbor_merge.drop_duplicates(inplace=True)
neighbor_merge.reset_index(drop=True, inplace=True)

#######################
# Save data

df_geo_coord.to_csv(os.path.join('data', 'Outlines_AT_DE_Albina_python_merge.csv'))
neighbor_merge.to_csv(os.path.join('data','neighbours_AT_DE_ALBINA.csv'))