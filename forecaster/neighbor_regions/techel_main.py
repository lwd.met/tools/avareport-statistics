# -*- coding: utf-8 -*-
"""
Created on Tue Jul 27 10:59:09 2021

@author: Alexander Kehl
"""

import os
import numpy as np
import pandas as pd
from scipy.stats import spearmanr
import matplotlib.pyplot as plt
import seaborn as sns
import pdb

# Local imports
from get_danger_levels import extract_tech_periods, get_danger, extract_border
from user_input import use_tech_time, df_neighbours, sfx, elev_id, use_elev_th

# Formatting
sns.set_palette('colorblind')
ref_col = sns.color_palette()

#%%
def spearmanr_dr(ava, df_neighbours, use_elev_th=use_elev_th):

    rho_lst, p_lst = [], []
    for i,k in zip(df_neighbours['V1'].iloc[:],df_neighbours['V2'].iloc[:],):

        if use_elev_th:
            df_filter = ava.groupby(
                ['SubregionCode1']).get_group(str(i)).groupby(
                    ['SubregionCode2']).get_group(str(k))
            df_i = df_filter.MaxPerDay1
            df_k = df_filter.MaxPerDay2
        else:
            df_i = ava.groupby(['SubregionCode']
                               ).get_group(str(i)).MaxPerDay.astype('float')
            df_k = ava.groupby(['SubregionCode']
                               ).get_group(str(k)).MaxPerDay.astype('float')

        # Get intersection of index (Dates are sometimes not the same)
        idx = df_i.index.intersection(df_k.index)
        df_i = df_i.loc[idx]
        df_k = df_k.loc[idx]

        #calculate Spearman Rank correlation and corresponding p-value
        rho, p = spearmanr(df_i, df_k, nan_policy='omit')
        rho_lst.append(rho)
        p_lst.append(p)

    return(rho_lst, p_lst)
#%%


if use_elev_th:
    df          = get_danger('2011-2015', use_elev_th)
    df2021      = get_danger('2020-2021', use_elev_th)
    df1819      = get_danger('2018-2019', use_elev_th)
    df1920      = get_danger('2019-2020', use_elev_th)

techel_starts = ['2011-12-16', '2012-12-07', '2013-12-05', '2014-12-27']
techel_ends = ['2012-04-29', '2013-05-01', '2014-04-23', '2015-04-26']



if __name__ == '__main__':

    path_folder = os.path.join('data', '2011-2015')

    # Load data
    if use_elev_th:
        df_techel       = get_danger('2011-2015', use_elev_th)
        df_2021         = get_danger('2020-2021', use_elev_th)
    else:
        df_techel = pd.read_csv(os.path.join(
            'data', '2011-2015_avalanche_report_BY_TY_MaxPerDay.csv'),
            sep = ',', index_col=0, parse_dates=True, header=0)
        df_2021 = pd.read_csv(os.path.join(
            'data', '2020-2021_avalanche_report_3xAT_BY_MaxPerDay.csv'),
            index_col=0, parse_dates=True)

    # Process data
    df_2021 = extract_tech_periods(df_2021, use_tech_time)
    years = [2011, 2012, 2013, 2014]
    df_techel = pd.concat([extract_tech_periods(df_techel, use_tech_time, k) for k in years])
    df_neighbours = extract_border(df_neighbours, ['AT-07', 'BY'])

    # Initiate some DataFrames
    # Avalanche report of BY and TY
    df_both_regions = pd.DataFrame({})
    # SpearmanRankTest for MaxDangerPerDay
    spearman_dr = df_neighbours.copy()
    # SpearmanRankTest for DangerRatingElevation
    spearman_elev = df_neighbours.copy()


    # Calculate SpearmanRank

    # DangerRating MaxPerDay each season
    # for start, end in zip(techel_starts, techel_ends):
    #     max_per_day_season = df_techel.loc[start:end].copy()
    #     yr_start = start[:4]
    #     spearman_dr[f'rho_{yr_start}'], spearman_dr[f'p_{yr_start}'] = spearmanr_dr(
    #         max_per_day_season, df_neighbours)

    # DangerRating MaxPerDay complete season
    spearman_dr['rho_2011-2015'], spearman_dr['p_2011-2015'] = spearmanr_dr(
        df_techel, df_neighbours)
    spearman_dr['rho_2021'], spearman_dr['p_2021'] = spearmanr_dr(
        df_2021, df_neighbours)


    # Sort alpabetically after regions
    spearman_dr.sort_values(by='V1', inplace=True)



    # Plot spearman MaxDangerPerDay
    merged_borders = (spearman_dr['V1'] + '__' + spearman_dr['V2']).to_list()
    # Plot spearman results
    fig, ax = plt.subplots()
    # twin object for two different y-axis on the sample plot
    ax2=ax.twinx()
    ax2.set_yscale('log')
    ax2.plot(merged_borders, spearman_dr['p_2011-2015'].values, '.-',
             label='Techel', color=ref_col[1])
    ax.plot(merged_borders, spearman_dr['rho_2011-2015'].values, '.-',
            label='Techel', color=ref_col[0])


    ax.plot(merged_borders, spearman_dr['rho_2021'].values, '--', label='2021',
            color=ref_col[0])
    ax2.plot(merged_borders, spearman_dr['p_2021'].values, '--', label='2021',
             color=ref_col[1])
    # From scipy.stats.spearmanr documantation:
    # Interpreatation: p-Value < 0.05 (5%) reject the null hypothesis test that
    # the two data sets are uncorrelated, i.e. p < 0.05 data sets are probably
    # correlated. The p-values are not entirely reliable but are probably
    # reasonable for datasets larger than 500 or so.

    ax.tick_params(axis='x', labelrotation=90, labelsize=10)
    ax.set_ylim(-0.05, 1.05)
    # ax2.set_ylim(1e-61, 1e-2) # Fix the limits for comparison!?
    ax.set_ylabel('Spearman rho', color=ref_col[0])
    ax2.set_ylabel('Spearman p', color=ref_col[1])
    ax.tick_params(axis='y', colors=ref_col[0])
    ax2.tick_params(axis='y', colors=ref_col[1])
    plt.legend()
    ax.set_title(f'Spearman DangerRating {elev_id}MaxPerDay')

    plt.tight_layout()

    fig.savefig(os.path.join('output', f'spearman_{elev_id}MaxPerDay{sfx}'))


