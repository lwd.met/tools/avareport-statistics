# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 12:07:13 2021

Downloads .xml-file (=caaml) from a website. Can convert caaml to json.

@author: Alexander Kehl
"""



"""
CLI for pyAvaCore
"""
from datetime import datetime
from datetime import timedelta
from pathlib import Path
from urllib.request import urlopen
import json
import logging
import logging.handlers
import sys
import pdb
import numpy as np
import os
import warnings


from pyAvaCore_NonAlbinaAustria import AvaReport, JSONEncoder, get_reports
from parse_json import custom_formatwarning


def download_region(url, directory, ftype='json'):
    """Downloads the given region and converts it to JSON"""

    reports, _, url = get_reports(url)
    # Create a list for handling reports valid for more days
    reports_more_days = [reports]
    report: AvaReport
    for report in reports:
        validityDate = report.validity_begin
        validityEnd = report.validity_end.date()
        if validityDate.hour > 15:
            validityDate = validityDate + timedelta(days=1)
        validityDate = validityDate.date()
        valid_days_len = (validityEnd - validityDate).days

        report.report_texts = None
        report.valid_regions = [r.replace('AT8R', 'AT-08-0') for r in report.valid_regions]

    validityDates = [validityDate]

    # Copy report if it is valid for more than 1 day.
    if valid_days_len > 0:
        for delta_days in (np.arange(valid_days_len) + 1):
            # Timedelta can not handle np.int, but only int
            delta_days = int(delta_days)
            if delta_days == 1:
                reports1, _, url = get_reports(url)
                report1: AvaReport
                for report1 in reports1:
                    report1.validity_begin = report1.validity_begin + timedelta(days=delta_days)
                    report1.report_texts = None
                    report1.valid_regions = [r.replace('AT8R', 'AT-08-0') for r in report1.valid_regions]
                validityDates.append(validityDate + timedelta(days=delta_days))
                reports_more_days.append(reports1)
            if delta_days == 2:
                reports2, _, url = get_reports(url)
                report2: AvaReport
                for report2 in reports2:
                    report2.validity_begin = report2.validity_begin + timedelta(days=delta_days)
                    report2.report_texts = None
                    report2.valid_regions = [r.replace('AT8R', 'AT-08-0') for r in report2.valid_regions]
                validityDates.append(validityDate + timedelta(days=delta_days))
                reports_more_days.append(reports2)
    if valid_days_len > 2:
        warnings.warn('Report is valid for more days than the script can handle')

    validityDates = [x.isoformat() for x in validityDates]


    # Extract regionID from subregion-code
    regionID = report.valid_regions[0][:5]

    # Save only Austrian (='AT') Non-Albina regions (= not 'AT-07')
    if (('AT' in regionID) & ('AT-07' not in regionID)):
        for validDate, reports in zip (validityDates, reports_more_days):
            path = os.path.join(directory, f'{validDate}-{regionID}.{ftype}')
            if os.path.exists(path):
                warnings.warn(f'{path} exists and file will be overwritten.')
            if ftype == 'json':
                with open(path, mode='w', encoding='utf-8') as f:
                    # logging.info('Writing %s', f.name)
                    json.dump(reports, fp=f, cls=JSONEncoder, indent=2)
            if ftype == 'xml':
                # Write to xml
                with urlopen(url) as http, open(path, mode='wb') as f:
                    # logging.info('Writing %s to %s', url, f.name)
                    f.write(http.read())

    return(None)



if __name__ == "__main__":
    warnings.formatwarning = custom_formatwarning

    # IDs for season 20-21 for Austria, except Tyrol and Voralberg. Range from 11 to 978
    url_ids = np.arange(11,979).astype(str)
    # IDs for development
    # url_ids = np.arange(737, 738).astype(str)
    # url_ids = np.arange(972,973).astype(str)
    # url_ids = np.arange(74,80).astype(str)

    for url_id in url_ids:
        url = ''.join(
            ("http://www.avalanche-warnings.eu/public/caaml/", url_id, "/en"))
        # Note: checking if the website exists before execution of try-except
        # statement is NOT faster
        try:
            download_region(url, directory='data_xml', ftype='xml')
        except Exception as e: # pylint: disable=broad-except
            pass
            # logging.error('Failed to download %s', url, exc_info=e)
