# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 10:53:59 2021

@author: Alexander Kehl
"""

import pandas as pd

def repl_str_with_val(df):
    df = df.replace(to_replace =['no_snow'], value ="0")
    df = df.replace(to_replace =['low'], value ="1")
    df = df.replace(to_replace =['moderate'], value ="2")
    df = df.replace(to_replace =['considerable'], value ="3")
    df = df.replace(to_replace =['high'], value ="4")
    df = df.replace(to_replace =['very high'], value ="5")
    df = df.replace(to_replace =['very_high'], value ="5")
    return(df)

