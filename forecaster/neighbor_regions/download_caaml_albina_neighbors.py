# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 09:20:13 2021

@author: hadis
"""

import pandas as pd
import requests
import os
# from json import
import json



def download_albina_neighbor(RegCode, dates):
    urls = [''.join(
        ('https://lawinen.report/albina_neighbors/', k, f'-{RegCode}.json'))
        for k in dates.astype('str')]

    for url in urls:
        try:
            data = requests.get(url).json()
            url_exists = True
        except json.JSONDecodeError:
            url_exists = False
            print(f'Could not download {url}.')
        # Save to local storage
        if url_exists:
            with open(os.path.join('data_caaml', url.split('/')[-1]), 'w') as f:
                json.dump(data, f)

    return(None)

# Define periods to download
dates_vbg = pd.date_range('2021-01-25', '2021-04-18') # Daily report (more reports released later)
dates_by = pd.date_range('2021-01-25', '2021-04-30')

# Download json files
download_albina_neighbor('BY', dates_by)
# download_albina_neighbor('AT-08', dates_vbg)