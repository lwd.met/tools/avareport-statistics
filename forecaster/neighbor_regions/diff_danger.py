# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 18:13:53 2021

Difference of Max Danger Level per Day: Bavaria - Tyrol

User Input
use_tech_time ... boolean

@author: Alexander Kehl
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib as mpl
import seaborn as sns
import pdb

from get_danger_levels import extract_tech_periods, extract_border, get_danger
from user_input import use_tech_time, elev_id, use_elev_th, sfx, df_neighbours

# Formatting
sns.set_palette('colorblind')
ref_col = sns.color_palette()
X_LABS = ['TY greater 2 BY', 'TY greater 1 BY',
            'equal', 'BY greater 1 TY', 'BY greater 2 TY']

# Define some strings
path_output = 'output'

# Load data
if use_elev_th:
    df          = get_danger('2011-2015', use_elev_th)
    df2021      = get_danger('2020-2021', use_elev_th)
    df1819      = get_danger('2018-2019', use_elev_th)
    df1920      = get_danger('2019-2020', use_elev_th)
else:

    df = pd.read_csv(
        os.path.join('data', '2011-2015_avalanche_report_BY_TY_MaxPerDay.csv'),
        header=0, index_col=0, parse_dates=True)
    df2021 = pd.read_csv(
        os.path.join('data', '2020-2021_avalanche_report_3xAT_BY_MaxPerDay.csv'),
        header=0, index_col=0, parse_dates=True)
    df1819 = pd.read_csv(
        os.path.join('data', '2018-2019_avalanche_report_BY_TY_MaxPerDay.csv'),
        header=0, index_col=0, parse_dates=True)
    df1920 = pd.read_csv(
        os.path.join('data', '2019-2020_avalanche_report_BY_TY_MaxPerDay.csv'),
        header=0, index_col=0, parse_dates=True)
# Process data
df2021 = extract_tech_periods(df2021, use_tech_time)
df1819 = extract_tech_periods(df1819, use_tech_time)
df1920 = extract_tech_periods(df1920, use_tech_time)
years = [2011, 2012, 2013, 2014]
df = pd.concat([extract_tech_periods(df, use_tech_time, k) for k in years])

# # Regions abbrevations to plot/evaluate
# # Tyrol & BAY
regions = ['AT-07', 'BY']


#%% Functions



# Calculate DangerRating difference: BY - TY
def diff_dr(df_ava, df_neighbour):
    df_ava = df_ava.loc[:, ['MaxPerDay', 'SubregionCode']].copy()

    ava_neighbours = pd.DataFrame()
    for i,k in zip(df_neighbour ['V1'].iloc[:],
                   df_neighbour['V2'].iloc[:]):
        df_i = df_ava.groupby(['SubregionCode']).get_group(str(i))
        df_k = df_ava.groupby(['SubregionCode']).get_group(str(k))
        try:
            ava_neighbours = pd.concat([ava_neighbours, pd.concat([df_i, df_k], axis=1)], axis=0)
        except pd.errors.InvalidIndexError:
            pdb.set_trace()
    ava_neighbours = ava_neighbours.reset_index()
    ava_neighbours.columns = ['Date', 'MaxPerDay1', 'SubregionCode1',
                              'MaxPerDay2', 'SubregionCode2']

    # Calculate DangerRating difference: BY - TY (cond1 and cond2 in
    # extract_border ensure that always BY - AT and not vice versa is calculated)
    ava_neighbours['diff'] = ava_neighbours['MaxPerDay2'] - ava_neighbours['MaxPerDay1']

    # return(diff)
    return(ava_neighbours)

# Calculate DangerRating difference: BY - TY
def diff_dr_elev(df_ava, df_neighbour):

    # Calculate DangerRating difference: BY - TY (cond1 and cond2 in
    # extract_border ensure that always BY - AT and not vice versa is calculated)
    df_ava['diff'] = df_ava['MaxPerDay2'] - df_ava['MaxPerDay1']

    # return(diff)
    return(df_ava)



def diff_plot(data, labels, title_sfx='', save=True, fname_sfx=f'{sfx}',
              x_labels=X_LABS):
    # Create the plots
    x = np.arange(len(x_labels))  # the label locations
    width = 0.2  # the width of the bars

    fig, ax = plt.subplots()

    rects = []
    for counter, (dat, lab) in enumerate(zip(data, labels)):
        rects.append(ax.bar(x - (width*(1.5 - counter)), dat, width, label=lab))

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Relative frequency')
    ax.set_title(f'Difference Danger Rating ({elev_id}MaxPerDay){title_sfx}')
    ax.set_xticks(x)
    ax.set_xticklabels(x_labels, rotation=60)
    ax.legend()
    # Nice axis length, but harder to compare with other plots
    # y_bottom, y_up = ax.get_ylim()
    # ax.set_ylim(y_bottom, y_up * 1.15)
    # Axis for comparison with other plots
    ax.set_ylim(0, 0.95)

    for rec in rects:
        ax.bar_label(rec, fmt='%.2f', padding=3, rotation=90)

    fig.tight_layout()

    if save:
        fig.savefig(os.path.join('output', f'DifferenceDanger_{elev_id}MaxPerDay{fname_sfx}'))
    return(None)

def my_hist2d(df, xlab, ylab, fig, ax, show_title=True, log=False):

    df = df.dropna(subset=['MaxPerDay1', 'MaxPerDay2']).copy()

    # For handling different DataFrames (by switch: use_elev_th)
    if 'Date' in df.columns:
        season = pd.DatetimeIndex(df.Date.values[0:1]).year[0]
    else:
        season = df.index.year[0]
    season = str(season) + '-' + str(season+1)

    cmap = plt.cm.viridis  # define the colormap
    # extract all colors from the colormap
    cmaplist = [cmap(i) for i in range(cmap.N)]

    # create the new map
    cmap = mpl.colors.LinearSegmentedColormap.from_list(
        'Oranges', cmaplist, cmap.N)

    # define the bins and normalize
    upper_bound = df.loc[:,['MaxPerDay1', 'MaxPerDay2']].value_counts().max()
    # Logarithmic scale
    if log:
        bounds = np.logspace(1, np.log(upper_bound), 12, base=np.exp(1)).astype(int)
    else:
        # Linear scale
        bounds = np.linspace(0, upper_bound, 12).astype(int)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    # norm = mpl.colors.LogNorm(bounds)


    im = ax.hist2d(df.MaxPerDay1, df.MaxPerDay2, cmin=1,
                   bins=10, norm=norm, cmap=cmap)
    # im[0] = im[0]/4
    # For log scale, use this argument in hist2d: norm=mpl.colors.LogNorm()

    # Formatting
    ax.set_xlabel(xlab)
    ax.set_ylabel(ylab)
    # Check if DangerRating 5 was issued
    if np.any(df.loc[:,['MaxPerDay1', 'MaxPerDay2']] == 5):
        # and define ticks
        ticks = np.arange(1,6)
    else:
        ticks = np.arange(1,5)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)

    pad = 0.1
    ax.set_xlim(ticks[0] - pad, ticks[-1] + pad)
    ax.set_ylim(ticks[0] - pad, ticks[-1] + pad)


    if show_title:
        ax.set_title(f'{season} with {elev_id}Danger')


    # Create a second axes for the colormap next to the axes of the plot
    # rect = ax.get_position()._points.flatten()
    # rect += np.array([rect[2] * 1.1, 0, 0, 0]) # shift the axes to the right
    # rect[2] *= 0.1 # make the width narrower

    # ax2 = fig.add_axes(rect)
    cb = plt.colorbar(im[3], ax=ax, fraction=0.046, pad=0.04)
    # We want x ticks for each bin
    # ax2.set_yticks(bounds)

    return(None)


#%%
df_neighbour_filter         = extract_border(df_neighbours, ['AT-07', 'BY'])
df_neighbour_filter_szbg    = extract_border(df_neighbours, ['AT-05', 'BY'])
df_neighbour_filter_vbg     = extract_border(df_neighbours, ['AT-08', 'BY'])

# Use different functions
func = diff_dr_elev if use_elev_th else diff_dr

df_diff         = func(df, df_neighbour_filter)
df_diff1819     = func(df1819, df_neighbour_filter)
df_diff1920     = func(df1920, df_neighbour_filter)
# Season 2021
df_diff2021         = func(df2021, df_neighbour_filter)
df_diff2021_szbg    = func(df2021, df_neighbour_filter_szbg)
df_diff2021_vbg     = func(df2021, df_neighbour_filter_vbg)

diff            = df_diff['diff']
diff1819        = df_diff1819['diff']
diff1920        = df_diff1920['diff']
# Season 2021
diff2021        = df_diff2021['diff']
diff2021_szbg   = df_diff2021_szbg['diff']
diff2021_vbg    = df_diff2021_vbg['diff']
#%%

# The data for the plots
# Techel seasons
# idx = diff.value_counts().sort_index().index # general
idx = [-2.0, -1.0, 0.0, 1.0, 2.0] # specific
height_total = diff.value_counts().reindex(idx).fillna(0).values
height_rel = height_total / len(diff)
# Season 1819
height_total1819 = diff1819.value_counts().reindex(idx).fillna(0).values
height_rel1819 = height_total1819 / len(diff1819)
# Season 1920
height_total1920 = diff1920.value_counts().reindex(idx).fillna(0).values
height_rel1920 = height_total1920 / len(diff1920)
# Season 2021
height_total2021 = diff2021.value_counts().reindex(idx).fillna(0).values
height_rel2021 = height_total2021 / len(diff2021)
height_total2021_szbg = diff2021_szbg.value_counts().reindex(idx).fillna(0).values
height_rel2021_szbg = height_total2021_szbg / len(diff2021_szbg)
height_total2021_vbg = diff2021_vbg.value_counts().reindex(idx).fillna(0).values
height_rel2021_vbg = height_total2021_vbg / len(diff2021_vbg)



#%% Plot Histogram of Difference in Danger Rating for several years

labels = ['2011-2015', '2018-2019', '2019-2020', '2020-2021']
data = [height_rel, height_rel1819, height_rel1920, height_rel2021]
diff_plot(data, labels, save=True)

# Austria - Bavaria
x_labs_AT = ['greater 2 BY', 'greater 1 BY',
            'equal', 'BY greater 1', 'BY greater 2']
labels = ['TY', 'SZBG', 'VBG']
data = [height_rel2021, height_rel2021_szbg, height_rel2021_vbg]
diff_plot(data, labels, save=False, x_labels=x_labs_AT, fname_sfx=f'_AT_BY{sfx}')


#%% Plot 2d-Histogram of DangerRating at Tyrol and Bavaria for several years

# Create figure
fig, axes = plt.subplots(2,2, figsize=(15,15))
# Define x/y labels
xlab = 'TIR'
ylab = 'BAY'
# Define logarithmic or linear colorbar scale
log = True
log = False
# Define filename extension for saving the plot
cb_str = 'log' if log else 'linear'

for df, ax in zip([df_diff, df_diff1819, df_diff1920, df_diff2021], axes.flatten()):
    my_hist2d(df, xlab, ylab, fig, ax, log=log)

fig.savefig(os.path.join('output', f'hist2d_overview_{xlab}_{ylab}_{elev_id}{sfx}_{cb_str}'))


#%% Plot Histogramm of DangerRating Differences AND 2d-Histogram for each border

# 2d-Histogram Plot user-input
# Plot 2d-histogram for all years for each border -> True, or for one year and all borders -> False
plot_all_years = False

if not plot_all_years:
    # Initialise figures
    fig, axes = plt.subplots(2, 5, figsize=(30, 15))
    axes = axes.flatten()
    fig2, axes2 = plt.subplots(2, 5, figsize=(30, 15))
    axes2 = axes2.flatten()

# Define logarithmic or linear colorbar scale
log = True
# log = False
# Define filename extension for saving the plot
cb_str = 'log' if log else 'linear'




# Initialise list to store results of relative frequency for later plotting
all_heights_rel = []


# Do analysis for each border seperately
for i,k, ax, ax2 in zip(df_neighbour_filter ['V1'].iloc[:],
                df_neighbour_filter ['V2'].iloc[:],
                axes, axes2):

    if plot_all_years:
        # Create figure for 2d-Histogram
        fig, axes = plt.subplots(2,2, figsize=(15,15))
    # Define x/y labels
    xlab = i
    ylab = k


    df_border = df_diff.groupby(
        ['SubregionCode1']).get_group(str(i)).groupby(
            ['SubregionCode2']).get_group(str(k))
    df_border1819 = df_diff1819.groupby(
        ['SubregionCode1']).get_group(str(i)).groupby(
            ['SubregionCode2']).get_group(str(k))
    df_border1920 = df_diff1920.groupby(
        ['SubregionCode1']).get_group(str(i)).groupby(
            ['SubregionCode2']).get_group(str(k))
    df_border2021 = df_diff2021.groupby(
        ['SubregionCode1']).get_group(str(i)).groupby(
            ['SubregionCode2']).get_group(str(k))

    # DangerRating difference: BY - TY
    border_diff = df_border['diff']
    border_diff1819 = df_border1819['diff']
    border_diff1920 = df_border1920['diff']
    border_diff2021 = df_border2021['diff']

    # The data for the plots
    # Techel seasons
    height_total = border_diff.value_counts().reindex(idx).fillna(0).values
    height_rel = height_total / len(border_diff)
    # Season 1819
    height_total1819 = border_diff1819.value_counts().reindex(idx).fillna(0).values
    height_rel1819 = height_total1819 / len(border_diff1819)
    # Season 2021
    height_total1920 = border_diff1920.value_counts().reindex(idx).fillna(0).values
    height_rel1920 = height_total1920 / len(border_diff1920)
    # Season 2021
    height_total2021 = border_diff2021.value_counts().reindex(idx).fillna(0).values
    height_rel2021 = height_total2021 / len(border_diff2021)

    # pdb.set_trace()
    all_heights_rel.append(height_rel)
    all_heights_rel.append(height_rel1819)
    all_heights_rel.append(height_rel1920)
    all_heights_rel.append(height_rel2021)

    # Plot
    labels = ['2011-2015', '2018-2019', '2019-2020', '2020-2021']
    data = [height_rel, height_rel1819, height_rel1920, height_rel2021]
    # diff_plot(data, labels, title_sfx=f': {i} - {k}', fname_sfx=f'_{i}_{k}{sfx}',
    #           save=True)

    # 2d-Histogram plot
    if plot_all_years:
        for df, ax in zip([df_diff, df_diff1819, df_diff1920, df_diff2021],
                          axes.flatten()):
            my_hist2d(df, xlab, ylab, fig, ax, log=log)

        fig.savefig(os.path.join(
            'output', f'hist2d_{xlab}_{ylab}_{elev_id}{sfx}_{cb_str}'))
    else:
        my_hist2d(df_diff, xlab, ylab, fig, ax, log=log)
        my_hist2d(df_diff2021, xlab, ylab, fig2, ax2, log=log)

if not plot_all_years:
    fig.savefig(os.path.join(
        'output', f'hist2d_2011-2015_overview_{elev_id}{sfx}_{cb_str}'))
    fig.savefig(os.path.join(
        'output', f'hist2d_2020-2021_overview_{elev_id}{sfx}_{cb_str}'))


#%% Plot overview of relative frequencies for all borders
idx = pd.DataFrame([labels * 10, df_neighbour_filter.V1.values.repeat(4),
                       df_neighbour_filter.V2.values.repeat(4)]).T
idx.columns = ['Date', 'V1', 'V2']
vals = pd.DataFrame([*all_heights_rel], columns=x_labs_AT)
df_rel = pd.concat([idx, vals],axis=1)
df_rel['merge'] = (df_rel['V1'] + '__' + df_rel['V2']).to_list()
df_rel.set_index('Date', inplace=True)
#%%
fig, ax = plt.subplots()

line_type = ['dashed', 'dotted']
seasons = ['2011-2015', '2020-2021']
colors = ref_col[:len(seasons)]
# For fakeing patches
patches = []

for season in seasons:

    col = [ref_col[0] if season == '2011-2015' else ref_col[1] if season == '2018-2019' else ref_col[2] if season == '2019-2020' else ref_col[3] if season == '2020-2021' else None][0]

    df_season = df_rel.loc[season]

    line_eq, = ax.plot(df_season['merge'].values, df_season['equal'].values, linestyle='solid', color=col)
    line_atgr, = ax.plot(df_season['merge'].values, df_season['greater 1 BY'].values, linestyle='dashed', color=col)
    line_bygr, = ax.plot(df_season['merge'].values, df_season['BY greater 1'].values, linestyle='dotted', color=col)

    # Fake line for producing legend
    patches.append(mpatches.Patch(color=col, label=season))


# Formatting
ax.set_ylim(0, 1)

lgd1 = plt.legend([line_eq, line_atgr, line_bygr],
           ['equal', 'greater 1 BY', 'BY greater 1'], loc='upper left')
plt.legend(handles=patches, loc='upper right')
plt.gca().add_artist(lgd1)

ax.tick_params(axis='x', labelrotation=90, labelsize=10)


fig.savefig(os.path.join(
    'output', f'DifferenceDanger_overview_{elev_id}MaxPerDay{sfx}'))
#%%
# VBG
# Do analysis for each border seperately
for i,k in zip(df_neighbour_filter_vbg ['V1'].iloc[:],
                df_neighbour_filter_vbg ['V2'].iloc[:],):

    df_border2021 = df_diff2021_vbg.groupby(
        ['SubregionCode1']).get_group(str(i)).groupby(
            ['SubregionCode2']).get_group(str(k))

    # DangerRating difference: BY - TY
    border_diff2021 = df_border2021['diff']

    # The data for the plots
    # Season 2021
    height_total2021 = border_diff2021.value_counts().reindex(idx).fillna(0).values
    height_rel2021 = height_total2021 / len(border_diff2021)

    # Plot
    labels = ['', '', '', '2020-2021']
    data = [np.nan, np.nan, np.nan, height_rel2021]
    diff_plot(data, labels, title_sfx=f': {i} - {k}', fname_sfx=f'_{i}_{k}{sfx}',
               save=False,
              x_labels=x_labs_AT)

# SZBG
# Do analysis for each border seperately
for i,k in zip(df_neighbour_filter_szbg ['V1'].iloc[:],
                df_neighbour_filter_szbg ['V2'].iloc[:],):

    df_border2021 = df_diff2021_szbg.groupby(
        ['SubregionCode1']).get_group(str(i)).groupby(
            ['SubregionCode2']).get_group(str(k))

    # DangerRating difference: BY - TY
    border_diff2021 = df_border2021['diff']

    # The data for the plots
    # Season 2021
    height_total2021 = border_diff2021.value_counts().reindex(idx).fillna(0).values
    height_rel2021 = height_total2021 / len(border_diff2021)

    # Plot
    labels = ['', '', '', '2020-2021']
    data = [np.nan, np.nan, np.nan, height_rel2021]
    diff_plot(data, labels, title_sfx=f': {i} - {k}', fname_sfx=f'_{i}_{k}{sfx}',
               save=False,
              x_labels=x_labs_AT)

