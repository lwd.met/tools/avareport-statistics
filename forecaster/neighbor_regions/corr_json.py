# -*- coding: utf-8 -*-
"""
Correct json's which are erroneous or can not be parsed correctly by
parse_json.py .

Created on Thu Jul  8 14:40:38 2021

@author: Alexander Kehl
"""

import os
import json
import numpy

def load_json(path):
    with open(path, 'r') as myfile:
        data=myfile.read()
    # Parse file
    json_data = json.loads(data)
    return(json_data)

def write_json(path, obj):
    with open(path, mode='w', encoding='utf-8') as f:
        json.dump(obj, fp=f, indent=2)
    return(None)

def corr_json_general(path, key, value, to_replace=None):
    """
    1. Reads json file.
    2. Replace all matching values for a defined key in a json file.
    3. Write corrections to same json file.

    Parameters
    ----------
    path : str
        Path of the json file.
    key : str
        Key of the json/dictionary.
    value : str, (probably also working for other TYPES)
        The new value replacing the old one.
    to_replace : str, (probably also working for other TYPES)
        Value or part of the value which is replaced.

    Returns
    -------
    None.

    """

    reports = load_json(path)

    # OPTIMIZE
    # Extent function to replace specific values,
    #  example:reports[3]['danger_main'][1]['valid_elevation'] = value
    # test = reports
    # for kw in kwargs:
    #     test = test[kw]
    # test = value


    if to_replace is None:
        raise NotImplementedError()
    else:
        for report in reports:
            report[key] = report[key].replace(to_replace, value)

    write_json(path, reports)

    return(None)



if __name__ == '__main__':
    dir_json = 'data_caaml'
    corr_json_general(os.path.join(dir_json, '2021-01-21-AT-05.json'),
                      'validity_begin', '23:00:00', '17:44:49')
    corr_json_general(os.path.join(dir_json, '2021-02-05-AT-05.json'),
                      'validity_begin', '23:00:00', '17:10:52')
    corr_json_general(os.path.join(dir_json, '2021-02-05-AT-05.json'),
                      'validity_begin', '11:00:00', '05:10:52')
    # For this report an update was released
    corr_json_general(os.path.join(dir_json, '2021-01-14-AT-05.json'),
                      'validity_begin', "2021-01-13T23:00:00", "2021-01-14T09:11:49")

    path = os.path.join(dir_json, '2021-02-21-AT-05.json')
    obj = load_json(path)
    obj[3]['danger_main'] = [{'main_value': 2, 'valid_elevation': None}]
    write_json(path, obj)

