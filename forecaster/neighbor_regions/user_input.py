# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 11:02:39 2021

User input for several scripts in this folder.

Several variables are defined more more than once, just comment out the ones
you don't want.


@author: Alexander Kehl
"""

import os
import pandas as pd

# Import data
# Austria and Bavaria
df_neighbours = pd.read_csv(os.path.join('data', 'neighbours_AT_DE_ALBINA.csv'),
                            header=0,sep=',')
# arange and filter data
df_neighbours = df_neighbours.drop(df_neighbours.columns[[0]],axis=1)


################## User Input ##########################

# Select one or more seasons: '2011-2012', '2012-2013', '2013-2014', ...
# ... '2014-2015', '2018-2019', '2019-2020', '2020-2021'
season = ['2011-2012', '2012-2013', '2013-2014', '2014-2015']#, '2020-2021']
season = ['2020-2021']

# Use elevational thresholds True/False
use_elev_th = True
use_elev_th = False

# Regions abbrevations to plot for the pagree map
# Valid region codes are either full names of subregions (e.g. 'AT-07-01') or
# parts of it ('IT-32-BZ'/'AT-07' for South-/Tyrol or 'AT'/'BY'/'IT' for complete
# Austria/Bavaria/Italy), also see examples below. Region codes must
# be provided as polygons in Outlines[...].csv for beeing plotted and provided in [season]_avalanche_report[...].csv for pagree calculation.

# Tyrol & Euregio neighbours
regions = ['AT-07', 'BY', 'AT-05', 'AT-08', 'AT-02', 'IT-32-BZ']
# Border Tyrol to Bavaria
# regions = ['BY', 'AT-07-01', 'AT-07-02', 'AT-07-03', 'AT-07-04',
#             'AT-07-05', 'AT-07-06']
# Euregio
# regions = ['AT-07', 'IT']
# Automatically select (border) Tyrol and Bavaria for elevational threshold approach
if use_elev_th:
    regions = ['AT-07', 'BY']
    regions = ['BY', 'AT-07-01', 'AT-07-02', 'AT-07-03', 'AT-07-04',
            'AT-07-05', 'AT-07-06']


# Reduce to Techel periods for a one year dataset (14.December to 16. April) True/False
use_tech_time = True
# use_tech_time = False

# Treeline must be set to a numerical value for comparison of Danger ratings
# at equal elevations. Treeline can differ from subregion to subregion. However,
# for simplicity, it is set for the complete analysis to a constant value.
treeline = 1900
treeline = 2100



# Extract days with a specific Avalanche Problem issued - only ...
# ... FIRST AvalancheProblem and only Tyrolian subregions considered
extract_ava_prob = True
# extract_ava_prob = False
# These are the five Avalanche Problems...
ava_problems = ['wet_snow', 'new_snow', 'wind_drifted_snow', 'gliding_snow',
                'persistent_weak_layers']
# ... Select one of them (0-4)
num_prob = 4

# Extract days with DangerRatingElevation = treeline
# only Tyrolian subregions considered or ...
extract_treeline = True
extract_treeline = False
# ... only NON-Tyrolian subregions considered (set extract_treeline = False)
extract_nty_treeline = True
extract_nty_treeline = False



# Define elevation thresholds for some borders, region1 has higher mountains than reg2
# Note: in the current version this approach is only working for borders AT-BY, because of extract_borders function in get_danger_levels.py
elev_thresholds = {
    'reg1': ['AT-07-01', 'AT-07-02', 'AT-07-02', 'AT-07-02',
             'AT-07-03', 'AT-07-04', 'AT-07-05', 'AT-07-06',
             'AT-07-06', 'AT-07-04'],
    'reg2': ['BYALL', 'BYALL', 'BYAMM', 'BYWFK',
             'BYWFK', 'BYWFK', 'BYBVA', 'BYCHG',
             'BYBVA', 'BYBVA'],
    'elev': [2400, 2400, 2000, 2400,
             2400, 2400, 2000, 2000,
             2000, 2000]}
########################################################



# Some further processing of the user input
mindex = pd.MultiIndex.from_arrays([elev_thresholds['reg1'],
                                    elev_thresholds['reg2']])
df_elevs = pd.DataFrame(elev_thresholds['elev'], index=mindex, columns=['elev'])

elev_id = f'Treeline{str(treeline)}Elev' if use_elev_th else ''
sfx = '_TechelPeriods' if use_tech_time else '_AllSeason'
ava_prob = ava_problems[num_prob] if extract_ava_prob else ''
prob_sfx = f'_{ava_prob}' if extract_ava_prob else ''
tree_sfx = '_OnlyNonTirTreeline' if extract_nty_treeline else '_OnlyTirTreeline' if extract_treeline else ''