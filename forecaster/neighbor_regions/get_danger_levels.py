# -*- coding: utf-8 -*-
"""
Created on Mon Jun 21 10:20:01 2021

@author: Alexander Kehl
"""

import pandas as pd
import numpy as np
import os
import pdb
import matplotlib.pyplot as plt
import matplotlib as mpl
import warnings

# Local imports
from user_input import df_elevs, df_neighbours, use_elev_th, treeline, ava_prob, extract_ava_prob, ava_problems, extract_treeline, extract_nty_treeline


def elev_hist(df, no_elev = 1400, subreg_col = 'SubregionCode', density=False):
    # Matplotlib default colors for plotting
    cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
    to_rgba = mpl.colors.ColorConverter().to_rgba

    n = len(df[subreg_col].unique())
    n_ty = 6
    colors = np.append(np.repeat(cycle[0], n_ty), np.repeat(cycle[1], n - n_ty))
    alphas = np.append(np.linspace(1, 0.2, n_ty), np.linspace(1, 0.2, n - n_ty))
    rgbs = [to_rgba(col, alpha) for col, alpha in zip(colors, alphas)]


    # Replace NaNs with no_elev, to see in the barplot
    df.DangerRatingElevation.replace(np.nan, no_elev+50, inplace=True)
    # Make sure data has int format
    df['DangerRatingElevation'] = df['DangerRatingElevation'].astype('int')
    # Prepare data
    subreg_sort = df[subreg_col].drop_duplicates().sort_values().values
    grouped = df.groupby([subreg_col])
    subreg_dr_elev = [grouped.get_group(subreg).DangerRatingElevation for
            subreg in subreg_sort]
    if treeline != 2100:
        raise(NotImplementedError('Treeline must be set to 2100m for this plot.'))


    # Plot
    fig, ax = plt.subplots()
    bins = np.arange(min(df.DangerRatingElevation.values),
                     max(df.DangerRatingElevation.values)+100, 100)
    ax.hist(subreg_dr_elev, bins=bins, stacked=True, color=rgbs, density=density)
    # Formatting
    ax.legend(labels=subreg_sort, ncol=2, columnspacing=0.5)
    if density:
        ax.set_ylabel('Relative Frequency')
    else:
        ax.set_ylabel('Counts')
    ax.set_xlabel('Elevation [m]')

    return(fig, ax)


def extract_border(df_neighbours, regions):
    # Extract regions
    # Check if region is in V1
    df_neighbours = pd.concat([group for (name, group) in
                              df_neighbours.groupby(['V1'])
                              if any(x in name for x in regions)]).sort_index()
    # Check if region is in V2
    df_neighbours = pd.concat([group for (name, group) in
                              df_neighbours.groupby(['V2'])
                              if any(x in name for x in regions)]).sort_index()


    # Filter for one entry per neighbourhood relationship AND BY-AT borders only
    cond1 = [True if i.find('AT') >= 0 else False for i in df_neighbours['V1']]
    cond2 = [True if i.find('BY') >= 0 else False for i in df_neighbours['V2']]
    df_neighbour_filter = df_neighbours[np.all([cond1, cond2], axis=0)]

    return(df_neighbour_filter)


def max_dr(df, new_col = 'MaxPerDay'):
    """ Maximum Danger Rating per Day"""

    # Do not change original Data Frame
    df_ = df.copy()
    # Maximum per Daytime
    df_[new_col] = df_.loc[:,['DangerRatingBelow', 'DangerRatingAbove']].max(axis=1)
    # Maximum per Day
    df_day = df_.groupby(['SubregionCode', 'Date'])[new_col].max().reset_index()
    return(df_day)


def elev_per_day(df):
    """ Maximum Danger Rating Elevation per Day"""
    df_day = df.groupby(['SubregionCode', 'Date']).DangerRatingElevation.max().reset_index().copy()

    return(df_day)


def elev_max_dr(df_ava, df_neighbour, df_elevs):
    df_ava = df_ava.copy()

    ava_neighbours = pd.DataFrame()
    for i, k in zip(df_neighbour ['V1'].iloc[:],
                   df_neighbour['V2'].iloc[:],):


        df_i = df_ava.groupby(['SubregionCode']).get_group(str(i)).copy()
        df_k = df_ava.groupby(['SubregionCode']).get_group(str(k)).copy()

        # ??? Evaluate each region only once?

        if (i, k) in df_elevs.index:
            elev = df_elevs.loc[i, k].values[0]
            elev_cond = df_i.DangerRatingElevation > elev
            # Set DangerAbove to DangerBelow if DR Elev is above elev
            df_i.DangerRatingAbove = np.where(elev_cond, df_i.DangerRatingBelow,
                                              df_i.DangerRatingAbove)
        if (k, i) in df_elevs.index:
            elev = df_elevs.loc[k, i].values[0]
            elev_cond = df_k.DangerRatingElevation > elev
            # Set DangerAbove to DangerBelow if DR Elev is above elev
            df_k.DangerRatingAbove = np.where(elev_cond, df_k.DangerRatingBelow,
                                              df_k.DangerRatingAbove)

        # Calculate Max Danger per Day
        df_i = max_dr(df_i)
        df_k = max_dr(df_k)
        #
        df_i.set_index('Date', drop=True, inplace=True)
        df_k.set_index('Date', drop=True, inplace=True)
        # Merge with previous results
        ava_neighbours = pd.concat([ava_neighbours, pd.concat([df_i, df_k], axis=1)], axis=0)

    # Handle column names
    ava_neighbours.columns = ['SubregionCode1', 'MaxPerDay1', 'SubregionCode2', 'MaxPerDay2']
    # Delete rows if one or botg SubregionCodes are NaN (introduced by unaligned Dates)
    ava_neighbours.dropna(subset=['SubregionCode1', 'SubregionCode2'],
                          how='any', inplace=True)


    return(ava_neighbours)


def elev_hist_static(df_day, density = True, save=True, title='', season=''):
    """ Plot histogram of DangerRatingElevation with predefined formats"""
    # TODO put to other script


    fig, ax = elev_hist(df_day, no_elev=1150, density=density)

    ticks = np.array(np.arange(1250,3050, 100))
    ticks_str = (ticks - 50).astype(str)
    ticks_str[0] = 'NoElevThr' # Keine Höhe or NoElevThr (No elevational threshold)
    ticks_str[9] = 'Treeline' # treeline or Baumgrenze
    # pdb.set_trace()

    tit = title if len(title) > 1 else f'Season {season[:4]}/{season[-2:]}'
    ax.set_title(tit)
    ax.set_xticks(ticks)
    ax.set_xticklabels(ticks_str, rotation=90)
    ax.set_xlim(1150, 3050)
    y_low, y_up = ax.get_ylim()
    if density:
        # Set a fixed upper ylim for easier comparison among different years
        y_lim_fixed = 0.0055
        if y_up < y_lim_fixed:
            ax.set_ylim(0, y_lim_fixed)
        marker = 'RelFreq_'
    else:
        y_lim_fixed = 950
        if y_up < y_lim_fixed:
            ax.set_ylim(0, y_lim_fixed)
        marker = 'Counts_'

    fig.tight_layout()

    if save:
        fig.savefig(
            f'output/histogram_DangerRatingElevation_PerDay_{marker}{season}')

    return(None)


def extract_tech_periods(df, use_tech_time = True, year=1):
    """
    Should work for 1 avalanche report season.

    Example:
        years = [2011, 2012, 2013, 2014]
        df = pd.concat([extract_tech_periods(df, use_tech_time, k) for k in years])
    """
    # Reduce to Techel periods (14.December to 16. April)
    if use_tech_time:
        # Use Date as index for easier handling with .loc etc
        try:
            df.set_index('Date', inplace=True)
            date_is_col = True
        except KeyError:
            date_is_col = False
        df.index = pd.to_datetime(df.index)
        # Define some variables
        # Optimze set default value to None
        if year == 1:
            year = df.index.year[0]
        else:
            pass
        idx_mon = df.index.month
        idx_day = df.index.day
        # Are Techel start and end days in the DataFrame?
        cond_start = np.any((idx_mon == 12) & (idx_day == 14))
        cond_end = np.any((idx_mon == 4) & (idx_day == 16))
        # Reduce to Techel periods

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            if cond_start:
                tech_start = '-'.join((str(year), '12-14'))
                # Check if the date is in index, if not, add one day and try again
                # because it can raise a FutureWarning and KeyError in the Future
                # OPTIMIZE implement that (code below does not work yet)
                # while True:
                #     if np.any(tech_start == df.index):
                #         break
                #     day = tech_start[-2:]
                #     new_day = str(int(day) + 1)
                #     tech_start = tech_start[:-2] + new_day
                #     if day == '32':
                #         raise(NotImplementedError)
                df = df.loc[tech_start:].copy()
            if cond_end:
                # OPTIMIZE do the same as for tech_start, but substract one day
                tech_end = '-'.join((str(int(year)+1), '04-16'))
                df = df.loc[:tech_end].copy()
        # Use Date as column again
        if date_is_col:
            df.reset_index(inplace=True)
    return(df)

def get_danger(season, use_elev_th):
    """ Create DataFrame of Maximum Danger Rating"""

    fname = f'{season}_avalanche_report_euregio_neighbours.csv'

    # Load data
    df = pd.read_csv(os.path.join('data', fname), index_col=0)
    # Check if SubregionCode contains no NaN's
    if np.any(df.SubregionCode.isna()):
        raise(NotImplementedError('Column SubregionCode contains NaN, remove them'))

    # Select by Tyrolian DangerRatingElevation = treeline
    if extract_treeline:
        # Split DataFrames into ...
        # Tyrolian and ...
        df_ty = pd.concat([df.groupby('SubregionCode').get_group(k) for k in df.SubregionCode.unique() if 'AT-07' in k])
        # Not Tyrolian regions
        df_nty = pd.concat([df.groupby('SubregionCode').get_group(k) for k in df.SubregionCode.unique() if 'AT-07' not in k])
        # Filter
        if extract_nty_treeline:
            df_nty = df_nty[df_nty.DangerRatingElevation == 'treeline'].copy()
        else:
            df_ty = df_ty[df_ty.DangerRatingElevation == 'treeline'].copy()
        # Merge Tyrolian and Not-Tyrolian regions again
        df = pd.concat([df_ty, df_nty], axis=0)

    # Replace treeline with numerical value
    df.replace('treeline', treeline, inplace=True)
    # Convert to float
    df.DangerRatingElevation = df.DangerRatingElevation.astype('float')

    # Select by Tyrolian AvalancheProblem1
    if extract_ava_prob:
        # Non-Tyrolean regions use other name
        df.replace('persistent_weak_layer', 'persistent_weak_layers', inplace=True)
        # Check if the Avalanche Problem is in the DataFrame
        if ava_prob not in df.AvalancheProblem1.unique():
            raise(KeyError(f'{ava_prob} not in DataFrame'))
        # Split DataFrames into ...
        # Tyrolian and ...
        df_ty = pd.concat([df.groupby('SubregionCode').get_group(k) for k in df.SubregionCode.unique() if 'AT-07' in k])
        # Not Tyrolian regions
        df_nty = pd.concat([df.groupby('SubregionCode').get_group(k) for k in df.SubregionCode.unique() if 'AT-07' not in k])
        # Filter for one AvalancheProblem
        df_ty = df_ty[df_ty.AvalancheProblem1 == ava_prob].copy()
        # Merge Tyrolian and Not-Tyrolian regions again
        df = pd.concat([df_ty, df_nty], axis=0)


    # Calculate Values per Day
    # Elevation DangerRating
    if use_elev_th:
        df_neighbour_filter = extract_border(df_neighbours, ['AT-07', 'BY'])
        df = elev_max_dr(df, df_neighbour_filter, df_elevs)
    # Simple Analysis
    else:
        mx_dr = max_dr(df)
        elev = elev_per_day(df).DangerRatingElevation
        df_lst = [mx_dr, elev]
        df = pd.concat(df_lst, axis=1)
        df.set_index('Date', inplace=True)

    return(df)


#%%

if __name__ == '__main__':

    all_seasons = ['2011-2012', '2012-2013', '2013-2014', '2014-2015',
                   #'2015-2016', '2016-2017', '2017-2018'
                   # '2018-2019', '2019-2020',
                   '2020-2021']

    # Initialise a Dataframe for merging all Techel years
    df_techel = pd.DataFrame()

    for season in all_seasons:

        df = get_danger(season, use_elev_th)


        # The analysis only makes sense if use_elev_th = False
        if not use_elev_th:
            fname = f'{season}_avalanche_report_euregio_neighbours'

            # Save as csv
            # df.to_csv(os.path.join('data', fname + '_MaxPerDay.csv'))


            # Reduce to Techel periods before creating plot
            df = extract_tech_periods(df)

            # Extract BY and Tyrol border
            reg_codes = df.SubregionCode.unique()
            border = ['BYWFK', 'BYBVA', 'BYCHG', 'BYBGD', 'BYAMM', 'BYALL',
                      'AT-07-01', 'AT-07-02', 'AT-07-03', 'AT-07-04', 'AT-07-05', 'AT-07-06']
            df = pd.concat([df.groupby('SubregionCode').get_group(k) for k in reg_codes
                            if k in border])

            if season in ['2011-2012', '2012-2013', '2013-2014', '2014-2015']:
                df_techel = pd.concat([df_techel, df])


            # Plot AND save histogram
            # TODO move to other script
            # TODO check if DangerRatingElevation values with 2100m exist ...
            # ... before replacing "Treeline" with this values, because the ...
            #  ... xtick_label "Treeline" is automatically set at xtick 2100
            # FIXME  ValueError for season 2018-2019
            elev_hist_static(df, density = True, save=True, season=season)
            # elev_hist_static(df, density = False, save=False, season=season)
    elev_hist_static(df_techel, density = True, save=True,
                     title='Seasons 2011/12-2014/15', season='2011-2015')

