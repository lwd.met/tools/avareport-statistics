Avalanche reports of different countries are compared with each other.
The data structure varies from year to year and from country to country, that is why the scripts are a bit messy and not very handy, i.e. adaptation for a new region/year requires some typing.

# Workflow
1. Merge geojson files and convert it to a .shp file (Alexander did that in QGIS).
	- Merge: https://freegistutorial.com/how-to-merge-vector-layers-on-qgis-3-2/
	- Convert to .shp: https://freegistutorial.com/how-to-export-layer-to-shapefile-on-qgis/
2. Run Outlines.R and FindNeighboorhoodRelation.R
3. Set your parameters in user_input.py
4. Run one of the following scripts	
	- pagree_map_BY.py
	- get_danger_levels.py
	- techel_main.py
	
# Data
The complete data set for season is on the external hard drive "Seagate 1TB Model SRD00F1", located usually in the "Zentrale". Path: D:\Gitlab-data\avareport-statistics\neighbor_regions\data . The data folder might be a bit messy and some of the files might be already outdated - sorry. The other "data" folders are just some raw data which were processed further afterwards. 
In the folder "Literature" you find the Techel paper and a short presentation of the results.

# Scripts

## Downloading reports
- corr_json (run before parse_json.py)
- parse_json.py (download from ALBINA homepage or convert locally stored json to ALBINA format and each season as .csv; possible for every season)
- parse_BY_http.py (parses Bavarian reports; possible for every season)
- pyAvaCore_convert_caaml2json.py (download & process historic caaml/xml files from a homepage and convert to json; here: season 2020/21 )
## Correction of reports
- corr_BY.py (season 2011-2015)
- merge_avas.py (season 2020/21 for Tyrol and Bavaria; merge and process)
- functions.py
## Calculate
- Outlines.R (create polygons of borders)
- FindNeighboorhoodRelation.R (Find neighbourhood relations)
- Pagree_df_max_BY.py (calculate probability of agreement at each border)
- pyAvaCore_NonAlbinaAustria.py (helper script for pyAvaCore_convert_caaml2json.py) 
- processor_caaml_NonAlbinaAustria.py (helper script for pyAvaCore_convert_caaml2json.py) 
## Calculate and Plot
- data_comparison_techel_vs_alexander.py (season 2011-2015, Bavaria and Tyrol; just listed for completeness)
- diff_danger.py (Calculate difference in AvalancheDangerRating at borders and plot the results)
- get_danger_levels.py (Calculate MaxDangerPerDay and plots histogramm of DangerRatingElevation)
- pagree_map_BY.py (plot a map of Pagree)
- techel_main.py (calculate and plot spearman rank test for Danger Rating)

# Folders
- Output: some plots (you find more on the external hard drive -> see Data)


Compare danger rating at the border for Bavaria and Tyrol. 

# Supported regions with RegionID for JSON parser (parse_json.py), season: 2020-21

## Austria

### Lower Austria: AT-03
### Carinthia: AT-02
### Upper Austria: AT-04
### Salzbourg: AT-05
### Styria: AT-06
### Vorarlberg: AT-08

## Germany

### Bavaria: BY

## Italy

### South Tyrol: IT-32-BZ
### Trentino: IT-32-TN