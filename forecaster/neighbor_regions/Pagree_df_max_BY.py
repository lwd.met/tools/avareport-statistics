#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 14:23:56 2020

@author: Julius Loos, modified by Alexander Kehl
"""

import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import pdb
import warnings

# Local imports
from functions import repl_str_with_val
from user_input import use_tech_time, season, use_elev_th, df_neighbours
from get_danger_levels import extract_tech_periods, get_danger


def pagree(df_stats, df_neighbours):
    # filter dates and regions
    date_in_col = True if 'Date' in df_stats.columns else False
    if date_in_col:
        df_regions = df_stats[['Date','SubregionCode', 'MaxPerDay']].copy()
    else:
        df_regions = df_stats[['SubregionCode', 'MaxPerDay']].copy()
    ava_regs = df_regions.SubregionCode.unique()
    df_regions['MaxPerDay'] = df_regions['MaxPerDay'].astype('int')
    # Date as index for comparing datasets with different Date
    if date_in_col:
        df_regions.set_index('Date', inplace=True)


    df_pagree = []
    df_neighbours_filt = []



    # loop for pairs of each date

    for i,k in zip(df_neighbours['V1'].iloc[:], df_neighbours['V2'].iloc[:],):
        # Loop only once through each border # This makes code faster, but
        # needs adjustment of code in pagree, see 'optimize' tag
        # cond = ~((k, i) in df_neighbours_filt)


        # Check if Neighbourhood Relationship is in Avalanche Report
        if (all(x in ava_regs for x in (i, k))):# & cond):
            try:

                df_i = df_regions.groupby(
                    ['SubregionCode']).get_group(str(i))
                df_j = df_regions.groupby(
                    ['SubregionCode']).get_group(str(k))
                df_diff = df_i['MaxPerDay'] - df_j['MaxPerDay']
                df_diff = df_diff.value_counts()
                df_sum  = df_diff.sum()
                pAgree  = df_diff.loc[0] / df_sum
                df_pagree.append(pAgree)
                # Use only neighbours which are in df_stats
                df_neighbours_filt.append((i, k))
            except(KeyError):
                pdb.set_trace()

    #Arange new pagree
    df_pagree = pd.DataFrame(df_pagree)
    df_pagree.columns = ['pAgree']

    #New dataframe with regions and pAgree
    df_all = pd.concat(
        [pd.DataFrame(df_neighbours_filt, columns=['V1', 'V2']), df_pagree],
        axis=1)

    return(df_all)


def pagree_elev(df_stats, df_neighbours):
    # filter dates and regions
    df_regions = df_stats.copy()

    unique_regions = df_stats.drop_duplicates(
        subset=['SubregionCode1', 'SubregionCode2'])
    regs1 = unique_regions.SubregionCode1.values
    regs2 = unique_regions.SubregionCode2.values

    df_regions['MaxPerDay1'] = df_regions['MaxPerDay1'].astype('int')
    df_regions['MaxPerDay2'] = df_regions['MaxPerDay2'].astype('int')
    # Use Date as index for easier handling with .loc etc
    try:
       df_regions.set_index('Date', inplace=True)
    except KeyError:
        pass


    df_pagree = []



    # loop for pairs of each date

    for i, k in zip(regs1, regs2):


            df = df_stats.groupby(
                'SubregionCode1').get_group(i).groupby(
                    'SubregionCode2').get_group(k)
            df_diff = df['MaxPerDay1'] - df['MaxPerDay2']
            df_diff = df_diff.value_counts()
            df_sum  = df_diff.sum()
            pAgree  = df_diff.loc[0] / df_sum
            df_pagree.append(pAgree)

    #Arange new pagree
    df_pagree = pd.DataFrame(df_pagree)
    df_pagree.columns = ['pAgree']

    df_regs = pd.DataFrame([regs1, regs2]).T
    df_regs.columns = ['V1', 'V2']

    #New dataframe with regions and pAgree
    df_all = pd.concat([df_regs, df_pagree],axis=1)

    return(df_all)


##############################################################################
# Initiate a merged DataFrame with the desired years
df_merge = pd.DataFrame()

# Load each year seperately
for seasn in season:

    # Calculate MaxDangerPerDay
    df_stats = get_danger(seasn, use_elev_th)

    # Load DataFrame containing MaxPerDay
    if seasn <= '2014-2015':
        fname_techel = f'{seasn}_avalanche_report_techel.csv'
        # Load data
        df_techel = pd.read_csv(os.path.join('data', fname_techel), sep=",",
                                header=0, index_col=0)
        # Merge with calculated MaxPerDay
        df_stats = pd.concat([df_stats, df_techel])

    # Merge year with other years
    df_merge = pd.concat([df_merge, df_stats])





#%%

# "Rename" df_merge to df_stats for usage in other scripts
# pdb.set_trace()
df_stats = df_merge.sort_index().copy()

years = [k[:4] for k in season]

# Extract reduced season (as done by Techel)
df_stats = pd.concat([extract_tech_periods(df_stats, use_tech_time, k) for k in years])



# TODO check if df_stats has continous (=daily) report
# For compability with DataFrames with columns SubregionCode1, SubregionCode2:
    # df_stats.columns[df_stats.columns.str.find('Sub') == 0][0]
# for region in df_stats.SubregionCode.unique():
#     df = df_stats.groupby('SubregionCode').get_group(region)
#     diff = len(pd.date_range(df.Date.iloc[0], df.Date.iloc[-1])) - len(df.Date)
#     if diff != 0:
#         warnings.warn(f'{region} has no daily report. {diff} days are missing.')
#         break # Show warning only once

# Do the calculations
if use_elev_th:
    df_all = pagree_elev(df_stats, df_neighbours)
else:
    df_all = pagree(df_stats, df_neighbours)





# Old versions
###### VERSION Techel paper data with common period of observation
# # Note: use time Version 3 in VERSION TECHEL DATA USED IN PAPER
# # This is the same dataset as used in VERSION ALEXANDER (MaxDanger per DAY)
# df_stats = pd.read_csv(os.path.join('data', '2011-2015', 'BY-TY_maxDR_2011-2015.csv'),
#                   header=0)

# df_stats = pd.concat([df_techel, df_stats])
# df_stats.drop_duplicates(subset=['Date', 'SubregionCode'], inplace=True)
#######


###### VERSION ALEXANDER (MaxDanger per DAY)
# season = '2011-2015'
# df_stats = pd.read_csv(os.path.join('data', '2011-2015', 'BY-TY_maxDR_2011-2015.csv'),
#                   header=0)
#######