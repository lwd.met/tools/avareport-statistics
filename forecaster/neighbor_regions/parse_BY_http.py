# -*- coding: utf-8 -*-
"""
Created on Mon Jul 12 14:26:11 2021

@author: Alexander Kehl
"""
import re
import os
from urllib.request import urlopen
import numpy as np
import pandas as pd
import itertools
import pdb
import warnings
# import ssl
# import certifi

def check_list(list):
    return len(set(list)) == 1


url_base = 'https://www.lawinenwarndienst-bayern.de/res/archiv/lageberichte/'

def dates_n_urls(season, url_base=url_base):


    # Load the main page of the archive for the respective season
    url_season = ''.join((url_base, 'index.php?s_saison=', season))
    path = 'temp_main_season_page'
    # pdb.set_trace()

    with urlopen(url_season) as http, open(path, mode='wb') as f:
        f.write(http.read())
    with open(path, mode='r') as f:
        season_http = f.read()

    # Get all URLs of one season

    # Bayern changed report URL in season 2017/18
    if season <= '2017/2018':
        url_prefix = 'lagebericht.php'
    else:
        url_prefix = 'lagebericht_neu.php'
    report_str = 'href="' + url_prefix


    pos_ids = np.array([m.start() for m in re.finditer(report_str, season_http)])

    # Delete temporary file of main page
    os.remove(path)

    # String containing extension of link, date and DangerRating summary (max DR or no DR)
    daily_str = [season_http[(k + len(report_str)):].split('</a>')[0] for k
                 in pos_ids]
    daily_str_filter = [k for k in daily_str
                        if 'keine Gefahrenstufen ausgegeben' not in k]

    dates = [k.split('>')[1][:10] for k in daily_str_filter]
    url_ids = [k.split('">')[0] for k in daily_str_filter]

    # For parsing the report change back to previous used version of Bavarian report
    if season >= '2017/2018':
        url_prefix = 'lagebericht.php'

    urls = [''.join((url_base, url_prefix, k)) for k in url_ids]
    # pdb.set_trace()

    # Sort them ascending by date
    dates = dates[::-1]
    urls = urls[::-1]

    return(dates, urls)

#%% Get Danger Rating and Elevation

def ava_report(season):

    dates, urls = dates_n_urls(season)

    date_lst = []
    reg = []
    subreg = []
    dr_below = []
    dr_abov = []
    dr_elev = []
    dr_elev_reg = []

    below = 'unterhalb'
    above = 'über'
    above3 = 'oberhalb'
    img_str_id = 'img src="gspng_mobile.php'


    # for date, url in zip(dates[:10], urls[:10]):
    for date, url in zip(dates, urls):

        # Temporarily save the file
        path = 'temp_file.xml'

        with urlopen(url) as http, open(path, mode='wb') as f:
            f.write(http.read())
        with open(path, mode='r') as f:
            data = f.read()

        # pos_img = [m.start() for m in re.finditer(img_str_id, data)]
        dr_ab = np.array([k[0] for k in data.split('gso=')[1:]]).astype('int')
        dr_bel = np.array([k[0] for k in data.split('gsu=')[1:]]).astype('int')
        delta_dr_ab = np.array([k[0] for k in data.split('gsao=')[1:]]).astype('int')
        delta_dr_bel = np.array([k[0] for k in data.split('gsau=')[1:]]).astype('int')
        dr_bel_pm = dr_bel + delta_dr_bel
        dr_ab_pm = dr_ab + delta_dr_ab

        dr_abov.append(list(dr_ab) + list(dr_ab_pm))
        dr_below.append(list(dr_bel) + list(dr_bel_pm))

        # DangerRatingElevation = NaN
        elev_am = np.where(dr_ab == dr_bel, np.nan, True)
        elev_pm = np.where(dr_ab_pm == dr_bel_pm, np.nan, True)
        elev_day = np.concatenate([elev_am, elev_pm])


        if np.any(~np.isnan(elev_day)):

            if season >= '2017/2018':
                id_str = '<td align="left" style="padding-bottom:5px;"><small>'
                if data.find(id_str) == -1:
                    id_str = '<td style="padding-bottom:5px;" align="left"><small>'
                id_str_end = '</small>'
                # Parse elevation information for each region per day
                dr_elev_reg = [k[:k.find(id_str_end)] for k in data.split(id_str)[1:]]
                # Create value for AM and PM
                dr_elev_reg *= 2

                elev_day = np.where(elev_day == True, dr_elev_reg, np.nan)

            # Try to guess DangerRatingElevation
            else:

                elev_guess = []
                sub_data = data[data.find(img_str_id):data.find('Beurteilung der Lawinengefahr:')]
                    # IDEA: also use the field 'Beurteilung'
                    # sub_data_beurteilung =  data[data.find('Beurteilung der Lawinengefahr:'):]
                    # sub_data_beurteilung =  sub_data_beurteilung[
                    #     :sub_data_beurteilung.find('<br /><br />\n')]
                    # sub_data = sub_data + sub_data_beurteilung
                    # Get the item which was used most often
                    # for key, item in {x:elev_guess.count(x) for x in elev_guess}.items():
                    #     if item == 2:
                    #         print(key)
                # Below
                pos_elev = sub_data.find(below)
                pos = pos_elev + len(below) + 1
                elev_str = sub_data[pos:(pos+4)]
                # Above
                pos_elev2 = sub_data.find(above)
                pos2 = pos_elev2 + len(above) + 1
                elev_str2 = sub_data[pos2:(pos2+4)]
                # Above type 2
                pos_elev3 = sub_data.find(above3)
                pos3 = pos_elev3 + len(above3) + 1
                elev_str3 = sub_data[pos3:(pos3+4)]
                if elev_str == 'der ':
                    elev_guess.append('treeline')
                elif pos_elev > 0:
                    elev_guess.append(elev_str)
                if elev_str2 == 'der ':
                    elev_guess.append('treeline')
                elif pos_elev2 > 0:
                    elev_guess.append(elev_str2)
                if elev_str3 == 'der ':
                    elev_guess.append('treeline')
                elif pos_elev3 > 0:
                    elev_guess.append(elev_str3)

                # Check if elev_guess is unique
                if check_list(elev_guess):
                    elev = elev_guess[0]
                else:
                    elev = 'Uncertain'

                elev_day = np.where(elev_day == True, elev, np.nan)

        for elev_d in elev_day:
            dr_elev.append(elev_d)



    ###### Complete Data for avalanche report
    half_days = len(dr_elev) / 6
    days = len(dr_elev)/12
    dr_abov = list(itertools.chain(*dr_abov))
    dr_below = list(itertools.chain(*dr_below))
    daytime = (list(np.concatenate([np.repeat('AM', 6), np.repeat('PM', 6)]))
               * int(days))
    reg = ['BYALL', 'BYAMM', 'BYWFK', 'BYBVA', 'BYCHG', 'BYBGD'] * int(half_days)

    idx = np.repeat(dates, 12)
    d = {'Date': idx,'Daytime': daytime, 'Subregion': reg,
         'DangerRatingAbove': dr_abov,
         'DangerRatingBelow': dr_below, 'DangerRatingElevation': dr_elev}
    df_ava = pd.DataFrame(d)
    # Remove duplicates during AM/PM
    df_ava.drop_duplicates(subset=['Date', 'Subregion', 'DangerRatingAbove',
                                   'DangerRatingBelow', 'DangerRatingElevation'],
                           inplace=True)

    df_ava.set_index('Date', inplace=True)
    df_ava.replace(to_replace='Waldgrenze', value='treeline', inplace=True)
    # Do a little correction here
    if season == '2018/2019':
            df_ava.replace('Keine HÃ¶hengrenze', '2200', inplace=True)
    if np.any(df_ava.DangerRatingElevation.str.contains('Keine')):
        warnings.warn('Undefined string in column DangerRatingElevation.')
    if season >= '2017/2018':
        df_ava.DangerRatingElevation = df_ava.DangerRatingElevation.str.replace('m', '')
    if season == '2017/2018':
        df.replace('oberhalb 2200', '2200', inplace=True)
        df.replace('oberhalb 2000', '2000', inplace=True)
        df.replace('Wald', 'treeline', inplace=True)

    print(season)
    print(df_ava.DangerRatingElevation.value_counts())

    # TODO handle corrections
    # df2corr = df_ava[df_ava.DangerRatingElevation == 'Uncertain']

    # Remove temporary file
    os.remove(path)

    return(df_ava)




if __name__ == '__main__':

    # Unmark the season you want to download
    # Techel seasons
    # seasons =['2011/2012', '2012/2013', '2013/2014', '2014/2015']
    # After techel
    seasons =['2015/2016', '2016/2017', '2017/2018']
    # Seasons 2017-2021
    # seasons = ['2018/2019', '2019/2020', '2020/2021']

    for season in seasons:
        season_abbr = season[:4] + '-' + season[-4:]
        fname = ''.join((season_abbr, '_avalanche_report_BY.csv'))
        df = ava_report(season)
        df.to_csv(os.path.join('data', fname))

        # Before season 2017/18 DangerRatingElevation can not be parsed with
        # high accuracy and some "Uncertain" entries are created. They have to
        # be corrected (in corr_BY.py). For convenience they can be saved as
        # an temporary file, for further development.:
        # df[df.DangerRatingElevation == 'Uncertain'].to_csv(
        #     os.path.join('data', '2011-2015', '0_temporary_file_' + fname))