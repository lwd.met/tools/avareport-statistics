# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 17:09:08 2021

@author: Alexander Kehl
"""

import os
import pandas as pd
import numpy as np
import pdb


# Load data
path_folder = os.path.join('data', 'raw')
# Raw files converted to csv
fnames = ['LWD Berichte 2015_2016 fuer SLF.csv', 'GST Auswertungen VTG 16_17.csv', 'LWD Berichte bearbeitet_2017_2018.csv', 'LWD Berichte adaptiert 2018_2019.csv', 'Auswertung LWD Berichte 2019_2020.csv']
paths = [os.path.join(path_folder, fname) for fname in fnames]

df1 = pd.read_csv(paths[0], parse_dates=True, dayfirst=True, sep=';', encoding='cp1252', index_col=1)
df2 = pd.read_csv(paths[1], parse_dates=True, dayfirst=False, sep=';', encoding='cp1252', index_col=1, skiprows=[0,1])
df3 = pd.read_csv(paths[2], parse_dates=True, dayfirst=True, sep=';', encoding='cp1252', index_col=1)
df4 = pd.read_csv(paths[3], parse_dates=True, dayfirst=True, sep=';', encoding='cp1252', index_col=1)
df5 = pd.read_csv(paths[4], parse_dates=True, dayfirst=True, sep=';', encoding='cp1252', index_col=0)


probs = ['Problem1', 'Problem2']

df2.rename(columns={'Allg. Stufe': 'Allgem. Stufe'}, inplace=True)

def cols():
    return(['Allgem. Stufe', 'SMS-Text', 'Nachm. eingebl.'])


def parse_at08(df, cols2drop):
    # Do AvalancheProblems occur in the columns?
    probs_in_df = np.all([True if k in df.columns else False for k in probs])

    # There might be in some Dataframes rows with all NaN's - drop them
    df.dropna(how='all', inplace=True)

    if probs_in_df:
        df_probs = df[probs].copy()
        cols2drop += probs
    # Check if id/ID is in columns
    if 'id' in df.columns:
        cols2drop.append('id')
    if 'ID' in df.columns:
        cols2drop.append('ID')

    # Drop some useless columns
    df.drop(cols2drop, axis='columns', inplace=True)



    idx = np.repeat(df.index, 12)
    df_process = pd.DataFrame(index=idx, columns=['DangerRatingAbove', 'DangerRatingBelow', 'DangerRatingElevation'])

    df_process.DangerRatingAbove = df[df.columns[::3]].values.flatten()
    df_process.DangerRatingBelow = df[df.columns[1::3]].values.flatten()
    df_process.DangerRatingElevation = df[df.columns[2::3]].values.flatten() * 1000
    df_process['Daytime'] = ['AM', 'PM'] * int(len(idx)/2)
    regs = ['AT-08-0' + str(k) for k in range(1, 7)]
    df_process['SubregionCode'] = list(np.repeat(regs,2)) * int(len(idx)/12)

    # Correct incorrect DangerRatingElevation
    df_process.DangerRatingElevation = np.where(
        df_process.DangerRatingAbove == df_process.DangerRatingBelow,
        np.nan, df_process.DangerRatingElevation)

    if probs_in_df:
        df_probs = df_probs.iloc[np.repeat(np.arange(len(df_probs)), len(regs)*2)]
        df_probs.rename(columns={probs[0]: 'AvalancheProblem1',
                                 probs[1]: 'AvalacnheProblem2'}, inplace=True)
        df_probs.replace(
            {'Nassschnee': 'wet_snow', 'Neuschnee': 'new_snow',
             'Triebschnee': 'wind_drifted_snow', 'Gleitschnee': 'gliding_snow',
             'Altschnee': 'persistent_weak_layers',
             'Günstige Situation': 'favourable_situation'},
            inplace=True)
        df_process = pd.concat([df_process, df_probs], axis=1)

    # TODO fill missing dates

    return(df_process)

df1 = parse_at08(df1, cols() + ['Unnamed: 41'])
df2 = parse_at08(df2, cols())
df3 = parse_at08(df3, cols())
df4 = parse_at08(df4, cols())
df5 = parse_at08(df5, cols())



# Save Files
df1.to_csv(os.path.join('data', '2015-2016_avalanche_report_AT-08.csv'))
df2.to_csv(os.path.join('data', '2016-2017_avalanche_report_AT-08.csv'))
df3.to_csv(os.path.join('data', '2017-2018_avalanche_report_AT-08.csv'))
df4.to_csv(os.path.join('data', '2018-2019_avalanche_report_AT-08.csv'))
df5.to_csv(os.path.join('data', '2019-2020_avalanche_report_AT-08.csv'))
