# Comparison of LOLA observer informations (e.g. AvaObs) with the avalanche bulletin
import os
import sys

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

import pandas as pd
import helpers

# Define file names and paths
kronos_xlsx_filename = 'kronos-evaluations.xlsx'
bels_xlsx_filename = 'bels-lola-evaluations.xlsx'
albina_csv_filename = 'statistic_2022-11-01_2023-05-02_e_de.csv'
matrix_pic_file = 'eaws_matrix/EAWS-Matrix-DE_empty.png'
matrix_table_file = 'eaws_matrix/EAWS_matrix.csv'
result_xlsx_filename = 'lola-albina_comparison.xlsx'

# Define the list of regions to include
region_list = ['AT-07']

input_dir = 'input/'
output_dir = 'output/'

problem_lola_list = ['freshSnowProblem','windDriftetSnowProblem','persistentWeakLayersProblem','glidingSnowProblem','wetSnowProblem']

df_kronos = pd.read_excel(input_dir+kronos_xlsx_filename, sheet_name=0, dtype=str)
df_albina = pd.read_csv(input_dir+albina_csv_filename, delimiter=";", na_values='N/A', index_col=False, dtype=str)
df_compare = pd.DataFrame(columns=['dangerlevel_lola' ,'stability_lola', 'frequency_lola', 'size_lola', 
                                   'dangerlevel_albina' ,'stability_albina', 'frequency_albina', 'size_albina'])

for i, row in df_kronos.iterrows():
    userow = False
    comment = str(row['comment'])
    region = str(row['position.adsRegion.loc_ref'])
    userow = helpers.checkrow(comment, region_list, region)
    if userow:
        for problem_lola in problem_lola_list:
            stability_lola = row[f"{problem_lola}.snowCoverStabilityLabel"]
            frequency_lola = row[f"{problem_lola}.distributionDangersLabel"]
            size_lola = row[f"{problem_lola}.avalancheSize"]
            date_lola = row["createdAt"][:10]
            hour_lola = int(row["createdAt"][11:13])
            daytime_lola = helpers.hour_to_daytime(hour_lola)
            lat_lola = float(row["position.lat"])
            lng_lola = float(row["position.lng"])
            
            # calculate region of coordinates
            p = helpers.Point(lat_lola, lng_lola)
            totalregion_lola = helpers.get_micro_region(p)
            region_lola = totalregion_lola[:-3]
            subregion_lola = totalregion_lola[-2:]
            

            
            if not (pd.isna(stability_lola) or pd.isna(frequency_lola) or pd.isna(size_lola)):
                albina_values = helpers.get_bulletin_infos(df_albina, date_lola, daytime_lola, region_lola, subregion_lola, problem_lola)
                if albina_values:
                    stability_albina, frequency_albina, size_albina = albina_values
                    #calculate danger levels
                    dangerlevel_lola = helpers.matrix_values(stability_lola, frequency_lola, size_lola)
                    dangerlevel_albina = helpers.matrix_values(stability_albina, frequency_albina, size_albina)
                    
                    row_compare = {'problem_lola': problem_lola,
                                   'micro-region': totalregion_lola,
                                   'dangerlevel_lola': dangerlevel_lola,
                                   'stability_lola': stability_lola, 
                                   'frequency_lola': frequency_lola, 
                                   'size_lola': size_lola,
                                   'dangerlevel_albina': dangerlevel_albina,
                                   'stability_albina': stability_albina, 
                                   'frequency_albina': frequency_albina, 
                                   'size_albina': size_albina}
                    df_compare = pd.concat([df_compare, pd.DataFrame(row_compare, index=[0])], ignore_index=True)

#%%
# Putting results into dataframe
df_results = helpers.compare_metrics(df_compare)
df_results.to_excel(output_dir + result_xlsx_filename)

#%%





