# LOLA comparison
Compares detailed LOLA observations (e.g. AvaObs) with the bulletin information.
At the moment the result is presented as a table, if you are motivated you could maybe include a map visualization.

## Workflow
- The avalanche report statistics file has to be exported from the ADMIN GUI. This is not possible with the lawine@tirol.gv.at account, so ask somebody with more permissions. Or ask Norbert to activate this for your account.
- You need a statistics file from LOLA, in this case 'kronos-evaluations.xlsx'.
- Run the script.

## Results
Calculations are mainly based on Techel F. et al. (2018): Spatial consistency and bias in avalanche forecasts – a case study in the European Alps.

- N_delta_D is the number of observations where danger level and matrix values are given for ALBINA and LOLA datasets.
- delta_D_average is the average difference between ALBINA and LOLA danger level. A positive value indicates that ALBINA danger levels are higher.
- delta_STAB_average is the average difference between ALBINA and LOLA snowpack stability. A positive value indicates that ALBINA snowpack stability levels are higher.
- delta_FREQ_average is the average difference between ALBINA and LOLA frequency. A positive value indicates that ALBINA frequency levels are higher.
- delta_SIZE_average is the average difference between ALBINA and LOLA avalanche size. A positive value indicates that ALBINA avalanche size levels are higher.
- P_agree_D is the percentage of observations where the danger level is equal for ALBINA and LOLA.
- P_agree_3 is the percentage of observations where all matrix variables are equal for ALBINA and LOLA.
- P_agree_2 is the percentage of observations where exactly two matrix variables are equal for ALBINA and LOLA.
- P_agree_1 is the percentage of observations where exactly one matrix variable is equal for ALBINA and LOLA.
- P_agree_STAB-FREQ is the percentage of observations where snowpack stability and frequency are equal for ALBINA and LOLA.
- P_agree_STAB-SIZE is the percentage of observations where snowpack stability and avalanche size are equal for ALBINA and LOLA.
- P_agree_FREQ-SIZE is the percentage of observations where frequency and avalanche size are equal for ALBINA and LOLA.
- P_agree_STAB is the percentage of observations where snowpack stability is equal for ALBINA and LOLA.
- P_agree_FREQ is the percentage of observations where frequency is equal for ALBINA and LOLA.
- P_agree_SIZE is the percentage of observations where avalanche size is equal for ALBINA and LOLA.
- B_ij is the bias ratio for the danger level. Bij > 1 indicates LOLA data more frequently having higher danger levels than ALBINA , Bij = 1 indicates a perfectly balanced distribution, and indicates Bij <1 a skew towards more often higher danger levels in ALBINA compared to LOLA data.
