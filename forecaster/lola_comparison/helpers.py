# Helper functions for lola_comparison.py

import json
import pandas as pd


class Point:
    """
    Represents a point on the earth.

    Attributes
    ----------
    lat : float
        The latitude of the point.
    lng : float
        The longitude of the point.
    height : float
        The height of the point in meters above sea level.
    """

    def __init__(self, lat, lng):
        self._lat = lat
        self._lng = lng
        self._height = None

    def is_in_polygon(self, polygon):
        # check if point is in polygon
        # https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
        x = self._lng
        y = self._lat

        inside = False
        for i in range(len(polygon)):
            j = i - 1
            if ((polygon[i][1] > y) != (polygon[j][1] > y)) and (
                x
                < (polygon[j][0] - polygon[i][0])
                * (y - polygon[i][1])
                / (polygon[j][1] - polygon[i][1])
                + polygon[i][0]
            ):
                inside = not inside
        return inside

def hour_to_daytime(hour):
    if hour < 12:
        daytime = 'AM'
    else:
        daytime = 'PM'
    return daytime

def checkrow(comment, region_list, region):
    # check if row is not a comment and region is in list
    testmarker1 = 'test'
    testmarker2 = 'Test'
    testmarker3 = 'Trst'
    userow = False
    if not (comment.startswith(testmarker1) 
            or comment.startswith(testmarker2) 
            or comment.startswith(testmarker3)):
        for prefix in region_list:
            if region.startswith(prefix):
                userow = True
    return userow

def get_bulletin_infos(df_albina, date_lola, daytime_lola, region_lola, subregion_lola, problem_lola):
    problem_dict = {'freshSnowProblem': 'new_snow',
                    'windDriftetSnowProblem': 'wind_slab',
                    'persistentWeakLayersProblem': 'persistent_weak_layers',
                    'glidingSnowProblem': 'gliding_snow',
                    'wetSnowProblem': 'wet_snow'}
    stability_dict = {'fair': 'moderate',
                      'poor': 'weak',
                      'very_poor': 'veryWeak'}
    frequency_dict = {'few': 'rare',
                      'some': 'some',
                      'many': 'many'}
    size_dict = {'small': 'small',
                 'medium': 'medium',
                 'large': 'large',
                 'very_large': 'veryLarge',
                 'extremely_large': 'extremelyLarge'}
    
    df_albina = df_albina[
        (df_albina["Date"] == date_lola) &
        (df_albina["Region"] == region_lola) &
        (df_albina["Subregion"] == subregion_lola)]
    if len(df_albina) < 1:
        print(f"{date_lola} - {region_lola}-{subregion_lola}: No bulletin infos found")
        return
    
    if len(df_albina) > 1:
        df_albina = df_albina[df_albina["Daytime"] == daytime_lola]
    
    problem_albina = problem_dict[problem_lola]
    if problem_albina in df_albina.values[0]:
        # Get the column name(s) where 'problem_lola' exists
        columns_with_problem = str(df_albina.columns[df_albina.values[0] == problem_albina][0])
        stability_albina = df_albina[f"{columns_with_problem}SnowpackStability"].values[0]
        frequency_albina = df_albina[f"{columns_with_problem}Frequency"].values[0]
        size_albina = df_albina[f"{columns_with_problem}AvalancheSize"].values[0]
        stability_lola = stability_dict[f"{stability_albina}"]
        frequency_lola = frequency_dict[f"{frequency_albina}"]
        size_lola = size_dict[f"{size_albina}"]
        return stability_lola, frequency_lola, size_lola
    else:
        print(f"{date_lola} - {region_lola}-{subregion_lola}: '{problem_lola}' not present in bulletin")
        return

def get_micro_region (point: Point):
    # open micro-regions/AT-07_micro-regions.geojson.json

    with open("micro-regions/AT-07_micro-regions.geojson.json") as f:
        data = json.load(f)

    # find micro-region for given point
    for feature in data["features"]:
        if point.is_in_polygon(feature["geometry"]["coordinates"][0][0]):
            return feature["properties"]["id"]

def get_micro_region_list (region):
    filename = 'micro-regions/' + region + '_micro-regions.geojson.json'

    with open(filename) as f:
        data = json.load(f)
    
    micro_region_list = []
    # fill micro region list
    for feature in data["features"]:
        if not feature["properties"]["end_date"]:
            micro_region_list.append(feature["properties"]["id"])
    return micro_region_list

def matrix_values (stability, frequency, size):
    input_dir = 'input/'
    matrix_table_file = 'eaws_matrix/EAWS_matrix.csv'
    stability_dict = {'moderate': 'fair',
                      'weak': 'poor',
                      'veryWeak': 'very_poor'}
    frequency_dict = {'rare': 'few',
                      'some': 'some',
                      'many': 'many'}
    size_dict = {'small': 'small',
                 'medium': 'medium',
                 'large': 'large',
                 'veryLarge': 'very_large',
                 'extremelyLarge': 'extremely_Large'}
    
    # Define the values of the EAWS Matrix
    df_matrix = pd.read_csv(input_dir+matrix_table_file, delimiter=";",
                            index_col=['snowpack_stability','frequency','avalanche_size'], dtype=str)
    stability_albina = stability_dict[f"{stability}"]
    frequency_albina = frequency_dict[f"{frequency}"]
    size_albina = size_dict[f"{size}"]
    danger_rating = df_matrix.loc[stability_albina, frequency_albina, size_albina]['danger_rating']
    return danger_rating

def compare_metrics(df_compare):
    region = 'AT-07'

    columns = ['N_delta_D', 'N_delta_D0', 'N_delta_Dplus', 'N_delta_Dminus', 
               'N_delta_STAB-FREQ-SIZE0', 'N_delta_STAB-FREQ0',
               'N_delta_STAB-SIZE0', 'N_delta_FREQ-SIZE0',
               'N_delta_STAB0', 'N_delta_FREQ0', 'N_delta_SIZE0', 'N_delta_two0', 'N_delta_one0',
               'delta_D_average', 'delta_STAB_average', 'delta_FREQ_average',
               'delta_SIZE_average', 'P_agree_D', 'P_agree_3', 'P_agree_2', 'P_agree_1',
               'P_agree_STAB-FREQ', 'P_agree_STAB-SIZE', 
               'P_agree_FREQ-SIZE', 'P_agree_STAB', 'P_agree_FREQ', 'P_agree_SIZE','B_ij']
    columns_to_export = ['N_delta_D', 'delta_D_average', 'delta_STAB_average', 
                         'delta_FREQ_average','delta_SIZE_average', 'P_agree_D',
                         'P_agree_3', 'P_agree_2', 'P_agree_1', 'P_agree_STAB-FREQ', 
                         'P_agree_STAB-SIZE', 'P_agree_FREQ-SIZE', 'P_agree_STAB', 
                         'P_agree_FREQ', 'P_agree_SIZE', 'B_ij']
    micro_region_list = get_micro_region_list(region)
    problem_list = ['ALL','freshSnowProblem','windDriftetSnowProblem',
                    'persistentWeakLayersProblem','glidingSnowProblem','wetSnowProblem']
    row_list = problem_list + micro_region_list
    df_results = pd.DataFrame(columns=columns, index=row_list)
    
    for i, row in df_compare.iterrows():
        problem = row['problem_lola']
        subregion = row['micro-region']
        dangerlevel_lola = int(row['dangerlevel_lola'])
        dangerlevel_albina = int(row['dangerlevel_albina'])
        stability_value_lola = get_value_to_matrix_var(row['stability_lola'])
        stability_value_albina = get_value_to_matrix_var(row['stability_albina'])
        frequency_value_lola = get_value_to_matrix_var(row['frequency_lola'])
        frequency_value_albina = get_value_to_matrix_var(row['frequency_albina'])
        size_value_lola = get_value_to_matrix_var(row['size_lola'])
        size_value_albina = get_value_to_matrix_var(row['size_albina'])

        df_results.loc['ALL','N_delta_D'] = add_to_df(df_results.loc['ALL','N_delta_D'], 1)
        df_results.loc[problem,'N_delta_D'] = add_to_df(df_results.loc[problem,'N_delta_D'], 1)
        df_results.loc[subregion,'N_delta_D'] = add_to_df(df_results.loc[subregion,'N_delta_D'], 1)
        #N_delta_D += 1
        delta_D = dangerlevel_albina - dangerlevel_lola
        delta_STAB = stability_value_albina - stability_value_lola
        delta_FREQ = frequency_value_albina - frequency_value_lola
        delta_SIZE = size_value_albina - size_value_lola
        df_results.loc['ALL','delta_D_average'] = add_to_df(df_results.loc['ALL','delta_D_average'], delta_D)
        df_results.loc[problem,'delta_D_average'] = add_to_df(df_results.loc[problem,'delta_D_average'], delta_D)
        df_results.loc[subregion,'delta_D_average'] = add_to_df(df_results.loc[subregion,'delta_D_average'], delta_D)
        df_results.loc['ALL','delta_STAB_average'] = add_to_df(df_results.loc['ALL','delta_STAB_average'], delta_STAB)
        df_results.loc[problem,'delta_STAB_average'] = add_to_df(df_results.loc[problem,'delta_STAB_average'], delta_STAB)
        df_results.loc[subregion,'delta_STAB_average'] = add_to_df(df_results.loc[subregion,'delta_STAB_average'], delta_STAB)
        df_results.loc['ALL','delta_FREQ_average'] = add_to_df(df_results.loc['ALL','delta_FREQ_average'], delta_FREQ)
        df_results.loc[problem,'delta_FREQ_average'] = add_to_df(df_results.loc[problem,'delta_FREQ_average'], delta_FREQ)
        df_results.loc[subregion,'delta_FREQ_average'] = add_to_df(df_results.loc[subregion,'delta_FREQ_average'], delta_FREQ)
        df_results.loc['ALL','delta_SIZE_average'] = add_to_df(df_results.loc['ALL','delta_SIZE_average'], delta_SIZE)
        df_results.loc[problem,'delta_SIZE_average'] = add_to_df(df_results.loc[problem,'delta_SIZE_average'], delta_SIZE)
        df_results.loc[subregion,'delta_SIZE_average'] = add_to_df(df_results.loc[subregion,'delta_SIZE_average'], delta_SIZE)
        
        count2 = 0
        count1 = 0
        if dangerlevel_lola == dangerlevel_albina:
            df_results.loc['ALL','N_delta_D0'] = add_to_df(df_results.loc['ALL','N_delta_D0'], 1)
            df_results.loc[problem,'N_delta_D0'] = add_to_df(df_results.loc[problem,'N_delta_D0'], 1)
            df_results.loc[subregion,'N_delta_D0'] = add_to_df(df_results.loc[subregion,'N_delta_D0'], 1)
        if dangerlevel_lola > dangerlevel_albina:
            df_results.loc['ALL','N_delta_Dplus'] = add_to_df(df_results.loc['ALL','N_delta_Dplus'], 1)
            df_results.loc[problem,'N_delta_Dplus'] = add_to_df(df_results.loc[problem,'N_delta_Dplus'], 1)
            df_results.loc[subregion,'N_delta_Dplus'] = add_to_df(df_results.loc[subregion,'N_delta_Dplus'], 1)
        if dangerlevel_lola < dangerlevel_albina:
            df_results.loc['ALL','N_delta_Dminus'] = add_to_df(df_results.loc['ALL','N_delta_Dminus'], 1)
            df_results.loc[problem,'N_delta_Dminus'] = add_to_df(df_results.loc[problem,'N_delta_Dminus'], 1)
            df_results.loc[subregion,'N_delta_Dminus'] = add_to_df(df_results.loc[subregion,'N_delta_Dminus'], 1)
        if ((stability_value_albina == stability_value_lola) 
            and (frequency_value_albina != frequency_value_lola) 
            and (size_value_albina != size_value_lola)):
            count1 += 1
            df_results.loc['ALL','N_delta_STAB0'] = add_to_df(df_results.loc['ALL','N_delta_STAB0'], 1)
            df_results.loc[problem,'N_delta_STAB0'] = add_to_df(df_results.loc[problem,'N_delta_STAB0'], 1)
            df_results.loc[subregion,'N_delta_STAB0'] = add_to_df(df_results.loc[subregion,'N_delta_STAB0'], 1)
        if ((stability_value_albina != stability_value_lola) 
            and (frequency_value_albina == frequency_value_lola) 
            and (size_value_albina != size_value_lola)):
            count1 += 1
            df_results.loc['ALL','N_delta_FREQ0'] = add_to_df(df_results.loc['ALL','N_delta_FREQ0'], 1)
            df_results.loc[problem,'N_delta_FREQ0'] = add_to_df(df_results.loc[problem,'N_delta_FREQ0'], 1)
            df_results.loc[subregion,'N_delta_FREQ0'] = add_to_df(df_results.loc[subregion,'N_delta_FREQ0'], 1)
        if ((stability_value_albina != stability_value_lola) 
            and (frequency_value_albina != frequency_value_lola) 
            and (size_value_albina == size_value_lola)):
            count1 += 1
            df_results.loc['ALL','N_delta_SIZE0'] = add_to_df(df_results.loc['ALL','N_delta_SIZE0'], 1)
            df_results.loc[problem,'N_delta_SIZE0'] = add_to_df(df_results.loc[problem,'N_delta_SIZE0'], 1)
            df_results.loc[subregion,'N_delta_SIZE0'] = add_to_df(df_results.loc[subregion,'N_delta_SIZE0'], 1)
        if ((stability_value_albina == stability_value_lola) 
            and (frequency_value_albina == frequency_value_lola) 
            and (size_value_albina == size_value_lola)):
            df_results.loc['ALL','N_delta_STAB-FREQ-SIZE0'] = add_to_df(df_results.loc['ALL','N_delta_STAB-FREQ-SIZE0'], 1)
            df_results.loc[problem,'N_delta_STAB-FREQ-SIZE0'] = add_to_df(df_results.loc[problem,'N_delta_STAB-FREQ-SIZE0'], 1)
            df_results.loc[subregion,'N_delta_STAB-FREQ-SIZE0'] = add_to_df(df_results.loc[subregion,'N_delta_STAB-FREQ-SIZE0'], 1)
        if ((stability_value_albina == stability_value_lola) 
            and (frequency_value_albina == frequency_value_lola) 
            and (size_value_albina != size_value_lola)):
            count2 += 1
            df_results.loc['ALL','N_delta_STAB-FREQ0'] = add_to_df(df_results.loc['ALL','N_delta_STAB-FREQ0'], 1)
            df_results.loc[problem,'N_delta_STAB-FREQ0'] = add_to_df(df_results.loc[problem,'N_delta_STAB-FREQ0'], 1)
            df_results.loc[subregion,'N_delta_STAB-FREQ0'] = add_to_df(df_results.loc[subregion,'N_delta_STAB-FREQ0'], 1)
        if ((stability_value_albina == stability_value_lola) 
            and (frequency_value_albina != frequency_value_lola) 
            and (size_value_albina == size_value_lola)):
            count2 += 1
            df_results.loc['ALL','N_delta_STAB-SIZE0'] = add_to_df(df_results.loc['ALL','N_delta_STAB-SIZE0'], 1)
            df_results.loc[problem,'N_delta_STAB-SIZE0'] = add_to_df(df_results.loc[problem,'N_delta_STAB-SIZE0'], 1)
            df_results.loc[subregion,'N_delta_STAB-SIZE0'] = add_to_df(df_results.loc[subregion,'N_delta_STAB-SIZE0'], 1)
        if ((stability_value_albina != stability_value_lola) 
            and (frequency_value_albina == frequency_value_lola) 
            and (size_value_albina == size_value_lola)):
            count2 += 1
            df_results.loc['ALL','N_delta_FREQ-SIZE0'] = add_to_df(df_results.loc['ALL','N_delta_FREQ-SIZE0'], 1)
            df_results.loc[problem,'N_delta_FREQ-SIZE0'] = add_to_df(df_results.loc[problem,'N_delta_FREQ-SIZE0'], 1)
            df_results.loc[subregion,'N_delta_FREQ-SIZE0'] = add_to_df(df_results.loc[subregion,'N_delta_FREQ-SIZE0'], 1)
        df_results.loc['ALL','N_delta_two0'] = add_to_df(df_results.loc['ALL','N_delta_two0'], count2)
        df_results.loc[problem,'N_delta_two0'] = add_to_df(df_results.loc[problem,'N_delta_two0'], count2)
        df_results.loc[subregion,'N_delta_two0'] = add_to_df(df_results.loc[subregion,'N_delta_two0'], count2)
        df_results.loc['ALL','N_delta_one0'] = add_to_df(df_results.loc['ALL','N_delta_one0'], count1)
        df_results.loc[problem,'N_delta_one0'] = add_to_df(df_results.loc[problem,'N_delta_one0'], count1)
        df_results.loc[subregion,'N_delta_one0'] = add_to_df(df_results.loc[subregion,'N_delta_one0'], count1)
        
        
                
    #P_agree_D = (N_delta_D0/N_delta_D)*100
    #P_agree_M = (N_delta_STAB-FREQ-SIZE0/N_delta_D)*100
    #B_ij = (N_delta_D0 + N_delta_Dplus)/(N_delta_D0 + N_delta_Dminus)
    #delta_D = sum(delta_D_list)/len(delta_D_list)
    
    # Find rows where 'N_delta_D' is not empty
    non_empty_rows = df_results['N_delta_D'].notna()
    # Fill empty cells in 'N_delta_D0', 'N_delta_Dplus', and 'N_delta_Dminus' with 0 for non-empty rows
    df_results.loc[non_empty_rows, 'N_delta_D0'] = df_results.loc[non_empty_rows, 'N_delta_D0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_Dplus'] = df_results.loc[non_empty_rows, 'N_delta_Dplus'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_Dminus'] = df_results.loc[non_empty_rows, 'N_delta_Dminus'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_STAB0'] = df_results.loc[non_empty_rows, 'N_delta_STAB0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_FREQ0'] = df_results.loc[non_empty_rows, 'N_delta_FREQ0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_SIZE0'] = df_results.loc[non_empty_rows, 'N_delta_SIZE0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_STAB-FREQ-SIZE0'] = df_results.loc[non_empty_rows, 'N_delta_STAB-FREQ-SIZE0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_STAB-FREQ0'] = df_results.loc[non_empty_rows, 'N_delta_STAB-FREQ0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_STAB-SIZE0'] = df_results.loc[non_empty_rows, 'N_delta_STAB-SIZE0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_FREQ-SIZE0'] = df_results.loc[non_empty_rows, 'N_delta_FREQ-SIZE0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_two0'] = df_results.loc[non_empty_rows, 'N_delta_two0'].fillna(0)
    df_results.loc[non_empty_rows, 'N_delta_one0'] = df_results.loc[non_empty_rows, 'N_delta_one0'].fillna(0)
        
    df_results['P_agree_D'] = (df_results['N_delta_D0'] / df_results['N_delta_D'])*100
    df_results['P_agree_3'] = (df_results['N_delta_STAB-FREQ-SIZE0'] / df_results['N_delta_D'])*100
    df_results['P_agree_2'] = (df_results['N_delta_two0'] / df_results['N_delta_D'])*100
    df_results['P_agree_1'] = (df_results['N_delta_one0'] / df_results['N_delta_D'])*100
    df_results['P_agree_STAB-FREQ'] = (df_results['N_delta_STAB-FREQ0'] / df_results['N_delta_D'])*100
    df_results['P_agree_STAB-SIZE'] = (df_results['N_delta_STAB-SIZE0'] / df_results['N_delta_D'])*100
    df_results['P_agree_FREQ-SIZE'] = (df_results['N_delta_FREQ-SIZE0'] / df_results['N_delta_D'])*100
    df_results['P_agree_STAB'] = (df_results['N_delta_STAB0'] / df_results['N_delta_D'])*100
    df_results['P_agree_FREQ'] = (df_results['N_delta_FREQ0'] / df_results['N_delta_D'])*100
    df_results['P_agree_SIZE'] = (df_results['N_delta_SIZE0'] / df_results['N_delta_D'])*100
    df_results['B_ij'] = (df_results['N_delta_D0'] + df_results['N_delta_Dplus']) / (df_results['N_delta_D0'] + df_results['N_delta_Dminus'])
    df_results['delta_D_average'] = df_results['delta_D_average'] /  df_results['N_delta_D']
    df_results['delta_STAB_average'] = df_results['delta_STAB_average'] /  df_results['N_delta_D']
    df_results['delta_FREQ_average'] = df_results['delta_FREQ_average'] /  df_results['N_delta_D']
    df_results['delta_SIZE_average'] = df_results['delta_SIZE_average'] /  df_results['N_delta_D']
    
    return df_results[columns_to_export]

def add_to_df (value, add):
    if pd.isna(value):
        value = add
    else:
        value += add
    return(value)

def get_value_to_matrix_var (matrix_var):
    value_dict = {'moderate': 1,
                  'weak': 2,
                  'veryWeak': 3,
                  'rare': 1,
                  'some': 2,
                  'many': 3,
                  'small': 1,
                  'medium': 2,
                  'large': 3,
                  'veryLarge': 4,
                  'extremelyLarge': 5}
    return value_dict[matrix_var]


