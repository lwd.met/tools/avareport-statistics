
library(dplyr)
library(ggplot2)
library(reshape2)
library(gtools)
rm(list = ls())
graphics.off()

## ----- HANDLE INPUT DATA -----------------------
# Load the DB output of all assessments for avalanche.report during one season
report.data = read.table('../data/avalanche_report_statistics_19-20.csv', header=T, sep=";", na.strings="N/A")

# Load CSV file with information on technical updates (website, server, apminGUI)
tech.data = read.table('../data/AR_season19-20_sofware_versions_V2.csv', header=T, sep=",")

# Convert tech.data dates to POSIXct date format
tech.data$Date = strptime(tech.data$Date,format = "%d.%m.%y", tz="GMT")
tech.data$Date = as.POSIXct(tech.data$Date)
tech.data = tech.data[,-1] # delete versioning

# Load CSV file with information on training days for forecasters
training.data = read.table('../data/2019-2020-AvalancheReportProfessionalTraining.csv', header=T, sep=",")

# Convert tech.data dates to POSIXct date format
training.data$Date = strptime(training.data$Date, format="%d.%m.%y", tz="GMT")
training.data$Date = as.POSIXct(training.data$Date)

# Load CSV file with informon on blog activity
blog.data = read.table('../data/2019-20_blogs.csv', header=T, sep=";")

# Convert blog.data dates to POSIXct date format
names(blog.data)[1]="Date"
blog.data$Date = strptime(blog.data$Date, format="%d.%m.%Y",tz="GMT")
blog.data$Date = as.POSIXct(blog.data$Date)

# Load CSV file with user behaviour data of the website based on Matomo exports 
user.data = read.table('../data/2019-2020-AR-UniqueVisitors.csv', header=T, sep=",")

# Convert user.data dates to POSIXct date format
user.data$Date = strptime(user.data$Date, format="%d.%m.%y", tz="GMT")
user.data$Date = as.POSIXct(user.data$Date)


## ----- data MANIPULATION -----------------------
# Create a continuous time series for the anylsed season(s)
timeseries = data.frame(seq(as.POSIXlt("2019-10-01"),as.POSIXlt("2020-06-01"),"days"))
names(timeseries) = "Date"

# Find unique reports according to the BulletinId
u.reports = report.data[!duplicated(report.data[,c(1,3)]),]

# Convert tech.data dates to POSIXct pate format
u.reports$Date = strptime(u.reports$Date, format="%Y-%m-%d")
u.reports$Date = as.POSIXct(u.reports$Date)

# Create indices for AM / PM
ind.am = seq(1,nrow(u.reports),2)
ind.pm = seq(2,nrow(u.reports),2)

# Find the days with an effective change in the avalanche danger rating for AM/PM
diurnal.change = ifelse(u.reports$DangerRatingBelow[ind.am]==u.reports$DangerRatingBelow[ind.pm] &
                        u.reports$DangerRatingAbove[ind.am]==u.reports$DangerRatingAbove[ind.pm] ,
                        "FALSE","TRUE")


# Get rid of all PM entries without any diurnal change
u.reports = u.reports[sort(c(ind.am,ind.pm[diurnal.change=="TRUE"])),]


# Count and print the number of unique assessments. 
# An effective change in AM/PM danger rating is assumed to be full second assessment
n.unique.assessments = nrow(u.reports)
print(paste("Unique danger assessments = ",n.unique.assessments,sep=""))
print("Nnrow(u.reports[is.na(u.reports$DangerRatingElevation),]")

# Count and print the days with full avalanche danger assessment in one of the Euregio Provinces/States
warning.days = length(unique(report.data$Date))
print(paste("Total warning days = ",warning.days,sep=""))

# Create a data frame with the number of unique warning regions per day
u.reports.am.pm = u.reports[!duplicated(u.reports[,1]),]
n.warning.regions = data.frame(u.reports.am.pm$Date,ave(!is.na(u.reports.am.pm$Date), substr(u.reports.am.pm$Date, 1,10), FUN=sum))
names(n.warning.regions)[2]="NumberOfWarningRegions"
names(n.warning.regions)[1]="Date"
n.warning.regions = n.warning.regions[!duplicated(n.warning.regions[,1]),]
n.warning.regions$NumberOfWarningRegions = n.warning.regions$NumberOfWarningRegions*10

bulletin.id = unique(report.data$BulletinId)
warning.region.union = rep(NA,length(bulletin.id))
for(i in 1:length(bulletin.id))
{
  AM = which(bulletin.id[i]==report.data$BulletinId & report.data$Daytime == "AM")
  PM = which(bulletin.id[i]==report.data$BulletinId & report.data$Daytime == "PM")
  d.change =  ifelse(report.data$DangerRatingBelow[AM]==report.data$DangerRatingBelow[PM] &
                       report.data$DangerRatingAbove[AM]==report.data$DangerRatingAbove[PM] ,
                     "FALSE","TRUE")

  w.id= sort(c(AM,PM[d.change=="TRUE"]))
  
  warning.region.union[i] = ifelse("AT-07" %in% report.data$Region[w.id] & "IT-32-BZ" %in% report.data$Region[w.id] & "IT-32-TN" %in% report.data$Region[w.id],"TY-BZ-TN",
         ifelse("AT-07" %in% report.data$Region[w.id] & "IT-32-BZ" %in% report.data$Region[w.id],"TY-BZ",
                ifelse("AT-07" %in% report.data$Region[w.id] & "IT-32-TN" %in% report.data$Region[w.id],"TY-TN",
                       ifelse("IT-32-TN" %in% report.data$Region[w.id] & "IT-32-BZ" %in% report.data$Region[w.id],"BZ-TN",
                              ifelse("AT-07" %in% report.data$Region[w.id],"TY",
                                     ifelse("IT-32-BZ" %in% report.data$Region[w.id],"BZ",
                                            ifelse("IT-32-TN" %in% report.data$Region[w.id],"TN","NA")
                                            )
                                     )
                       )
                )
                )
  )
  
  
}
barplot(c(length(warning.region.union[warning.region.union =="TY-BZ-TN"]),length(warning.region.union[warning.region.union =="TY-TN"]),
          length(warning.region.union[warning.region.union =="TY-BZ"]),length(warning.region.union[warning.region.union =="BZ-TN"]),
          length(warning.region.union[warning.region.union =="TY"]), length(warning.region.union[warning.region.union =="BZ"]),
          length(warning.region.union[warning.region.union =="TN"])),
        ylim = c(0,300),names.arg = c("Ty-Bz-Tn","Ty-Tn","Ty-Bz","Bz-Tn","Ty","Bz","Tn"),
        ylab = "Warnregionen | Regioni di previsione")
text(paste("Ty total = 466",sep=""), x= 1, y= 250)
text(paste("Bz total = 272",sep=""), x= 1, y= 240)
text(paste("Tn total = 299",sep=""), x= 1, y= 230)
box()





# Rearrange the blog data set

blog.ty = blog.data[blog.data$Warndienst == "Tirol",]
names(blog.ty)[2]="BlogTy"
blog.ty$BlogTy = 10

blog.bz = blog.data[blog.data$Warndienst == "Suedtirol",]
names(blog.bz)[2]="BlogBz"
blog.bz$BlogBz = 10

blog.tn  = blog.data[blog.data$Warndienst == "Trentino",]
names(blog.tn)[2]="BlogTn"
blog.tn$BlogTn = 10

# now merge all pifferent data sets into one data frame
data = merge(x=timeseries,y=tech.data, by="Date",all.x=T)
data = merge(x=data,y=training.data, by="Date",all.x=T)
data = merge(x=data,y=blog.ty, by="Date", all.x=T)
data = merge(x=data,y=blog.bz, by="Date", all.x=T)
data = merge(x=data,y=blog.tn, by="Date", all.x=T)
data = merge(x=data,y=n.warning.regions, by="Date",all.x=T)



## ----- PLOTTING -----------------------
# prepare data for plotting in ggplot
m.data = melt(data[,],"Date")

start.date = as.POSIXct("2019-11-01")
stop.date = as.POSIXct("2020-06-01")
m.data$Date <- as.Date(m.data$Date)
min = 0
max = 150

ggplot(m.data, aes(x=Date, y=value, fill=variable)) + 
  geom_bar(stat="identity") +
  theme_light() +
 scale_x_date(date_labels = "%d-%m", 
              date_breaks="1 month", 
              date_minor_breaks = "weeks", 
              limits = c(as.Date(start.date), 
                         as.Date(stop.date)),
              expand = c(0, 0),
              name = "Winter | inverno 2019-2020") +
  scale_y_continuous(expand = c(0,0), limits = c(min,max)) +
  theme(axis.title.y = element_blank(),
        axis.text.y = element_blank())
  


par(las=1,xaxs="i", yaxs="i", tcl=0.5, mgp=c(3,0.3,0))
plot(x = user.data$Date,
     y = user.data$Unique.visitors,
     type = "l",
     xlim = c(start.date, stop.date),
     ylim = c(0,50000),
     col = "darkblue",
     lwd = 2,
     xlab = "Winter | Inverno 2019-2020",
     ylab = "Unique visitors",
     xaxt="n")
axis.POSIXct(1,at = seq(as.POSIXct("2019-11-01"),as.POSIXct("2020-06-01"),by="months"),format = "%d-%m")
axis.POSIXct(3,at = seq(as.POSIXct("2019-11-01"),as.POSIXct("2020-06-01"),by="months"),format = "%d-%m", labels=F)
axis(4,at=seq(0,50000,10000))

## ----- IN DEPTH ANALYSIS -----------------------

# Write unique reports for AM/PM in a shorter variable
u = u.reports.am.pm
av.prb = c("gliding_snow","new_snow","weak_persistent_layer","wet_snow","wind_drifted_snow")
perm = permutations(2,8,c(0,1), repeats=T)

p = as.character(c())
for (i in 1:nrow(perm))
{
  dummy = paste(perm[i,1],perm[i,2],perm[i,3],perm[i,4],perm[i,5],perm[i,6],perm[i,7],perm[i,8],sep="")
  p = c(p,dummy)
  remove(dummy)
}

data.asp.stats = data.frame(p)
data.elv.stats = list()
N = length(av.prb)*2
data.elv.stats <- vector("list", N)

for(ii in 1:length(av.prb))
{
  print(paste("Processing aspect stastistcs for Avalanche Problem --> ",av.prb[ii],sep=""))
  # Find rows with AvalancheProblem1 and AvalancheProblem1 
  wpAP1 = which(u$AvalancheProblem1 == paste(av.prb[ii],sep=""))
  wpAP2 = which(u$AvalancheProblem2 == paste(av.prb[ii],sep=""))
  
  # Find corresponping Aspect information columns
  colAspAvPrb1 = grep("AvalancheProblem1Aspect*",names(u))
  colAspAvPrb2 = grep("AvalancheProblem2Aspect*",names(u))
  
  
  wpAP1.data = u[wpAP1,c(2,colAspAvPrb1)]
  wpAP2.data = u[wpAP2,c(2,colAspAvPrb2)]
  
  elAP1.data = u[wpAP1,c(2,10,11)]
  elAP2.data = u[wpAP2,c(2,21,22)]
  
  data.elv.stats[[ii]] = table(elAP1.data$AvalancheProblem1ElevationAbove)
  names(data.elv.stats)[ii] = paste("Above: Elevation distribution AP --> ", av.prb[ii], sep=" ")
  data.elv.stats[[(length(data.elv.stats)/2+ii)]] = table(elAP1.data$AvalancheProblem1ElevationBelow)
  names(data.elv.stats)[(length(data.elv.stats)/2+ii)] = paste("Below: Elevation distribution AP --> ", av.prb[ii], sep=" ")

  # Create data set with 
  aspect.sum.AP1 = apply(u[wpAP1,colAspAvPrb1],1,sum)
  aspect.sum.AP2 = apply(u[wpAP2,colAspAvPrb2],1,sum)
  
  wpAP1.data = data.frame(wpAP1.data,aspect.sum.AP1)
  wpAP2.data = data.frame(wpAP2.data,aspect.sum.AP2)
  
  d = as.character(c())
  for (i in 1:nrow(wpAP1.data))
  {
    dummy = paste(wpAP1.data[i,2],wpAP1.data[i,3],wpAP1.data[i,4],wpAP1.data[i,5],wpAP1.data[i,6],wpAP1.data[i,7],wpAP1.data[i,8],wpAP1.data[i,9],sep="")
    d = c(d,dummy)
    remove(dummy)
  }
  
  for (i in 1:nrow(wpAP2.data))
  {
    dummy = paste(wpAP2.data[i,2],wpAP2.data[i,3],wpAP2.data[i,4],wpAP2.data[i,5],wpAP2.data[i,6],wpAP2.data[i,7],wpAP2.data[i,8],wpAP2.data[i,9],sep="")
    d = c(d,dummy)
    remove(dummy)
  }
  
  c.asp = c()
  for(i in 1:length(p))
  {
    dummy= length(which(d == p[i]))
    c.asp = c(c.asp,dummy)
    remove(dummy)
  }
  
  res.asp = data.frame(p,c.asp)
  names(res.asp)[2]=paste("CountAspect_",av.prb[ii],sep="")
  data.asp.stats = merge(x=data.asp.stats,y=res.asp, all.x=T, by="p")
}

data.asp.stats = data.asp.stats[which(apply(data.asp.stats[,c(2:6)],1,sum)>0),]

data.asp.stats = data.frame(data.asp.stats,apply(data.asp.stats[,c(2:6)],1,sum))
names(data.asp.stats)[7]="TotalSum"
data.asp.stats[order(data.asp.stats$TotalSum,decreasing = T),]


sum(data.asp.stats$TotalSum)


values <- subset(u.reports, select = c(Date,AvalancheProblem1ElevationAbove,AvalancheProblem1ElevationBelow,
                                           AvalancheProblem2ElevationAbove,AvalancheProblem2ElevationBelow))

nrow(values[!is.na(values$AvalancheProblem1ElevationAbove) & !is.na(values$AvalancheProblem1ElevationBelow),])

nrow(values[!is.na(values$AvalancheProblem2ElevationAbove) & !is.na(values$AvalancheProblem2ElevationBelow),])

                                           
