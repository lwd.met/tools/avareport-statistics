# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 16:17:52 2021

@author: hadis
"""

import os
import pandas as pd
import numpy as np
from PIL import Image, ImageDraw, ImageFont
import warnings
from pandas.core.common import SettingWithCopyWarning
from pdf2image import convert_from_path
from functools import reduce
import pdb

#%%

def extract_ava_prob(df_ava, ava_prob):
    """Extract DataFrame for each avalanche problem.
    """

    df_ava1 = df_ava.loc[
        :, 'AvalancheProblem1':'AvalancheProblem1NaturalHazardSiteDistribution']
    df_ava2 = df_ava.loc[
        :, 'AvalancheProblem2':'AvalancheProblem2NaturalHazardSiteDistribution']
    df_ava3 = df_ava.loc[
        :, 'AvalancheProblem3':'AvalancheProblem3NaturalHazardSiteDistribution']

    df_ava1_filt = df_ava1.groupby('AvalancheProblem1').get_group(ava_prob)
    df_ava2_filt = df_ava2.groupby('AvalancheProblem2').get_group(ava_prob)
    df_ava3_filt = df_ava3.groupby('AvalancheProblem3').get_group(ava_prob)

    with warnings.catch_warnings():
        # this will suppress SettingWithCopyWarning  in this block
        warnings.simplefilter(action='ignore', category=SettingWithCopyWarning)
        # Unify column names in order to get tidy DataFrame after concatenation
        for df in [df_ava1_filt, df_ava2_filt, df_ava3_filt]:
            df.rename(columns=lambda x:
                      x[:16] if (len(x) == 17) else (x[:16] + x[17:]),
                      inplace=True)

    df_ava_prob = pd.concat([df_ava1_filt, df_ava2_filt, df_ava3_filt], axis=0)

    return(df_ava_prob)

def ava_cross(df_ava_prob):
    """
    Create list of crosstables (=contingency tables, dt. Kontingenztabelle)
    in 4 different ways: Natural, Artificial in percent and total count.

    Parameters
    ----------
    df_ava_prob : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """

    # Natural (abbr. nat.)
    ava_crosstab_nat_per = pd.crosstab(
         df_ava_prob.AvalancheProblemNaturalDangerRating,
         [df_ava_prob.AvalancheProblemNaturalHazardSiteDistribution,
          df_ava_prob.AvalancheProblemNaturalAvalancheReleaseProbability],
         margins = False, normalize='index').fillna(0)
    ava_crosstab_nat_count = pd.crosstab(
         df_ava_prob.AvalancheProblemNaturalDangerRating,
         [df_ava_prob.AvalancheProblemNaturalHazardSiteDistribution,
          df_ava_prob.AvalancheProblemNaturalAvalancheReleaseProbability],
         margins = False).fillna(0)
    # Artificial (abbr. art.)
    ava_crosstab_art_per = pd.crosstab(
        df_ava_prob.AvalancheProblemArtificialDangerRating,
        [df_ava_prob.AvalancheProblemArtificialHazardSiteDistribution,
         df_ava_prob.AvalancheProblemArtificialAvalancheReleaseProbability,
         df_ava_prob.AvalancheProblemArtificialAvalancheSize],
         margins = False, normalize='index').fillna(0)
    ava_crosstab_art_count = pd.crosstab(
        df_ava_prob.AvalancheProblemArtificialDangerRating,
        [df_ava_prob.AvalancheProblemArtificialHazardSiteDistribution,
         df_ava_prob.AvalancheProblemArtificialAvalancheReleaseProbability,
         df_ava_prob.AvalancheProblemArtificialAvalancheSize],
         margins = False).fillna(0)
    # Convert to percent
    ava_crosstab_nat_per *= 100
    ava_crosstab_art_per *= 100

    return(ava_crosstab_nat_per, ava_crosstab_nat_count,
           ava_crosstab_art_per, ava_crosstab_art_count)



def plot_matrix(ava_prob, crosstabs, **kwargs):
    """ Plot counts and relative frequencies (percent) on empty EAWS matrix.
    """

    # Load image of empty eaws matrix
    image = convert_from_path(os.path.join('Input', 'eaws_matrix_empty.pdf'))[0]

    # Define fonts for large/small counts/percent
    fnt_size = 55
    fnt_title = ImageFont.truetype("arial.ttf", 70)
    fnt_count_l = ImageFont.truetype("arial.ttf", fnt_size)
    fnt_count_s = ImageFont.truetype("arial.ttf", int(fnt_size * 0.5))
    fnt_perc_l = ImageFont.truetype("arial.ttf", int(fnt_size * 0.9))
    fnt_perc_s = ImageFont.truetype("arial.ttf", int(fnt_size * 0.4))

    # Define size/location of objects, specifically for this pdf(!)
    height = image.height
    width = image.width
    width_pdf_inch = 17.5                   # (measured in AdobeAcrobate)
    inch  =  width / width_pdf_inch         # inch in PIL image units (=Pixel!?)
    # Formula:
    #   object = [size in inch from pdf] * [1 inch in PIL image units]
    # Note: PIL coordinate system: (x=0, y=0) is upper left corner also for draw.text().
    cell_l  = 1.34 * inch       # x and y length of large cells
    cell_s_x = 0.33 * inch      # x length of small cells
    cell_s_y = 0.36 * inch      # y length of small cells
    cell_art_x = 4.09 * inch    # x-Distance from (0, 0) of furthest left art. cell
    cell_nat_x = 10.05 * inch   # x-Distance from (0, 0) of furthest left nat. cell
    cell_y = 2.25 * inch        # y-Distance from (0, 0) of uppermost cells
    # Distance between percent and count text
    dy = cell_l * 0.2             # Large cells
    dy_s = cell_s_y * 0.4           # Small cells
    # Center text of large cells
    dx_l = 0.3 # OPTIMIZE needs adjustment to length of string for centering
    dx_s = 0.2 # OPTIMIZE needs adjustment to length of string for centering

    # Define coordinates of large artificial cells
    # OPTIMIZE handle completely white cells (l_cells_unused)
    # Note:  (Did not occur in the last two seasons 2018-2020)
    # l_cells_unused = [(1, 4, 1), (1, 4, 2), (1, 4, 3), (1, 4, 4),
    #                   (4, 1, 1), (4, 1, 2), (4, 1, 3), (4, 1, 4)] # Unfinished
    l_cells = [(1, 1, 2), (1, 2, 2), (1, 3, 2), # First raw of EAWS Matrix
               (2, 1, 2), (2, 2, 2), (2, 3, 3), (2, 4, 3), # Second raw
               (3, 1, 2), (3, 2, 2), (3, 3, 3), (3, 4, 3),
               (4, 3, 3), (4, 4, 3),
               (5, 4, 3)]

    # Assign crosstables
    (ava_crosstab_nat_per, ava_crosstab_nat_count,
     ava_crosstab_art_per, ava_crosstab_art_count) = crosstabs


    # Draw numbers
    draw = ImageDraw.ImageDraw(image)
    # Note: draw.text(x,y) same coordinate system as PIL: (x=0, y=0) is upper left corner

    # ??? different color for DangerRating=5 -> fill=None (=white)


    # Natural
    for y, x in ava_crosstab_nat_per.columns:
        # Array over all DangerRatings of contingency table column
        cont_arr = ava_crosstab_nat_per.loc[:, (y, x)].values
        cont_arr_count = ava_crosstab_nat_count.loc[:, (y, x)].values
        cond_select_val = cont_arr > 0
        perc = round(cont_arr[cond_select_val][0])
        count = cont_arr_count[cond_select_val][0]

        # Only draw numbers greater zero
        if count > 0:
            x_cord = x - 1
            y_cord = y - 1

            x_pos_ref = cell_nat_x + (x_cord + dx_l)*cell_l
            y_pos_ref = cell_y + (y_cord + 0.4)*cell_l

            draw.text((x_pos_ref, y_pos_ref),
                      f'{perc}%', fill=1,font=fnt_perc_l)
            draw.text((x_pos_ref, y_pos_ref + dy),
                      f'{count}', fill=1, font=fnt_count_l)

    # Artificial
    for y, x, z in ava_crosstab_art_count.columns:
        # Array of contigency table
        cont_arr_per = ava_crosstab_art_per.loc[:, (y, x, z)].values
        cont_arr_count = ava_crosstab_art_count.loc[:, (y, x, z)].values
        # Condition to select one and only value greater 0
        cond_select_val = cont_arr_count > 0
        perc = round(cont_arr_per[cond_select_val][0])
        count = cont_arr_count[cond_select_val][0]

        if count > 0:
            x_cord = x - 1 # x-Coordinate of art. cell. (0,0)=upper-left corner
            y_cord = y - 1 # y-Coordinate of art. cell. (0,0)=upper-left corner
            z_cord = z - 1 # "z"-Coordinate of small art. cell. 0=farthest left cell

            x_pos_ref = cell_art_x + x_cord*cell_l
            y_pos_ref = cell_y + y_cord*cell_l

            # Large cells
            if (y, x, z) in l_cells:
                x_pos_ref += dx_l * cell_l
                y_pos_ref += 0.4 * cell_l

                draw.text((x_pos_ref, y_pos_ref),
                          f'{perc}%', fill=1,font=fnt_perc_l)
                draw.text((x_pos_ref, y_pos_ref + dy),
                          f'{count}', fill=1, font=fnt_count_l)
            # Small cells
            else:
                x_pos_ref += (z_cord + dx_s) * cell_s_x
                y_pos_ref += 0.05 * cell_s_y

                draw.text((x_pos_ref, y_pos_ref),
                          f'{perc}%', fill=1, font=fnt_perc_s)
                draw.text((x_pos_ref, y_pos_ref + dy_s),
                          f'{count}', fill=1, font=fnt_count_s)

    # Title
    draw.text((width * 0.35, height * 0.01), f'Avalanche Problem: {ava_prob}',
              fill=1, font=fnt_title)

    #Draw No Field choosen text
    if 'perc' in kwargs:

        x = cell_art_x + 0.05*cell_l
        y = cell_y + 4.1*cell_l

        draw.text((x, y), f'NO FIELD\n{kwargs["perc"]}%\n{kwargs["count"]}',
                  fill=1, font=fnt_count_l)


    # image.show()
    # Keyword quality=95 is recomannded maximum
    image.save(os.path.join(path_out, f'{ava_prob}.pdf'), quality=95)

    return(None)

#%%
# Define paths
season = '2020-2021'
path_out = '_'.join(('Output', season[2:4], season[7:]))


# Load files

# Avalanche report
df_ava = pd.read_csv(
    os.path.join('Input',
                 'avalanche_report_statistics_20-21_extendedWithoutCoP.csv'),
    low_memory=False, usecols=range(0,76), parse_dates=True)


# Process data
# Dont use every subregion, but only merged regions
df_ava.drop_duplicates(subset=['BulletinId', 'Daytime'], inplace=True)
# If an avalanche report update was released, keep latest release
df_ava.drop_duplicates(subset=['Date', 'Daytime', 'Region', 'Subregion'],
                       keep='last', inplace=True)

# For easier sorting of crosstables, edit AvalancheReleaseProbability values
df_ava.replace({'four': 4, 'three': 3, 'two': 2, 'one': 1}, inplace=True)
# For easier sorting of crosstable, edit AvalancheHazardSiteDistribution values
df_ava.replace(
    {"many_most": 4, "many": 3, "some": 2, "single": 1}, inplace=True)
# For easier sorting of crosstables, edit DangerRating values
df_ava.replace({'no_snow': 0, 'low': 1, 'moderate': 2, 'considerable': 3,
            'high': 4, 'very high': 5, 'very_high': 5}, inplace=True)
# For easier sorting of crosstables, edit AvalancheSize values
df_ava.replace(
    {'small': 1, 'medium': 2, 'large': 3, 'very_large': 4}, inplace=True)

if __name__ == '__main__':

    # No EAWS matrix field selected
    no_field = df_ava[df_ava.AvalancheProblem1.isna()]

    # Assuming each possible Avalanche Problem is used as first Problem
    ava_probs = df_ava.AvalancheProblem1.dropna().unique()

    # Later save crosstab of each AvaProblem
    crosstabs_lst = []

    for ava_prb in ava_probs:

        df_ava_prob = extract_ava_prob(df_ava, ava_prb)
        crosstabs = ava_cross(df_ava_prob)
        plot_matrix(ava_prb, crosstabs)

        crosstabs_lst.append(crosstabs)

######
    # Calculate total crosstables
    total_cross = []

    # Loop over the 2 Crosstables for Artificial/Natural and Count
    for i in (1,3):
        # Sum crosstables of all avalanche problems
        total_crs = reduce(
            lambda x, y: y.add(x, fill_value=0),
            [crosstabs_lst[0][i], crosstabs_lst[1][i],
             crosstabs_lst[2][i], crosstabs_lst[3][i],  crosstabs_lst[4][i]])
        # During summation some NaN are introduced
        total_crs.fillna(0, inplace=True)
        # Calculate percentage
        perc_crs = total_crs.div(total_crs.sum(axis=1), axis=0) * 100
        # Append to List
        total_cross.append(perc_crs)
        total_cross.append(total_crs.astype('int'))

    count_no_field = len(no_field)
    perc_no_count = len(df_ava)/count_no_field
    plot_matrix('all_problems', total_cross, count = count_no_field,
                perc = perc_no_count)




