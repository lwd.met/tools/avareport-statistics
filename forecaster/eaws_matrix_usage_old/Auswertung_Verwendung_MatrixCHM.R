
# Copyright (c) 2019, LWD Tirol
# All rights reserved.


"
Analysing Avalanche Danger Rating Decision Process

Data Winter 2018/19

Euregio Data

Contingency analysis of the combination of Avalanche Hazard Distribution, Avalanche Size and Avalanche Release Probability leading to Danger Level
Following the EAWS Matrix (https://lawinen.report/education/matrix)


Data Structure:

Danger Rating Above and Below defined Elevation
(if no Elevation defined, same value in both columns)

divided into Artificial and Natural

- Danger Rating: low, moderate, considerable, high, very_high
- Avalanche Size: small, medium, large, very_large
- Release Probability:one, two, three, four
- Hazard site Distribution: single, some, many, many_most

Daytime: AM / PM, if there is no daytime dependency, only AM exists

Region: AT-07, IT-32-BZ, IT-32-TN

Subregion: ID Number of Subregions, Regions are only unique using Region & Subregion


Output:
 
- Plots for artificial/natural, above/below/sum, absolute values and percentages
- Data in csv files, artificial/natural, above/below/sum, as contingency table and as long dataframe


To Do before you run the script:

- define correct Input Path
- define season
- check levels of release probability


Date created        06 Nov 2019
Date last modified  14 Jul 2020

"

# author: Andrea Mayer
# ac_mayer@web.de
# co-author: Christoph Mitterer
# chris.mitterer@tirol.gv.at
# LWD Tirol



##----- DEPENDENCIES -----
# clear environment
rm(list=ls())


##----- OS SETTINGS -----

if (.Platform$OS.type == "windows"){
  os       <- "windows"
  extype   <- "shell"
  cPath    <- "C:/"
  xPath    <- "X:/"
}

# check R Version, adapt directory exist? function
versionR <- paste(version$major, version$minor, sep=".")
if (versionR < "3.2.0") dir.exists <- file.exists



##----- LIBRARIES & FUNCTIONS -----

# check existence of required packages
package_list <- c("tidyverse", 'plot.matrix', "dplyr", "plyr", "data.table", "plotrix", "rstudioapi")
new_packages <- package_list[!(package_list %in% installed.packages()[,"Package"])]
if (length(new_packages)) install.packages(new_packages)

## load necessary packages

# work with datasets
library(tidyverse)

# wirk with data frames and data tables
library(dplyr) #part of tidyverse
library(plyr)
library(data.table)

# save images from temp to file
library(rstudioapi)

# remove spaces next to strings
library(stringr)

##----- TMP SETTINGS -----

# useful to have, but not necessary
#if (os=="windows") dirTmp <- "C:/Rtmp"    # path to my Tmp directory
#if(!dir.exists(dirTmp)) { dir.create(dirTmp, recursive = TRUE)} # if not exist: create tmp diectory
setwd("~/Work/ALBINA-local/AnalyseSeason/2019-2020/script")



##------ INPUT OUTPUT SETTINGS ---------------

# set paths and settings

# dirIn 
# dirIn <- "../data/" # Christoph Mitterer
dirIn <- "C:/Users/hadis/Documents/LWD/avareport-statistics/eaws_matrix_usage/Input" # Alexander Kehl

# dirOut 
dirOut <- "../results/Output"
if(!dir.exists(dirOut)) { dir.create(dirOut, recursive = TRUE)} # if not exist: create output diectory


## Files ##

# Input 2018
# InputFile <- paste0(dirIn, "/avalanche_report_statistics_18-19_extended.csv")
# 
# season <- "2018/19"

# Input 2019
InputFile <- paste0(dirIn, "/avalanche_report_statistics_19-20_extended.csv")

# Input 2020
# InputFile <- paste0(dirIn, "/avalanche_report_statistics_20-21_extendedWithoutCoP.csv")


# needed to plot the correct header
season <- "2020/21"


##----- DEFINITIONS TO RUN SCRIPT -----

# levels of release probability
# were all levels used during the winter? important for correct sorting of the data

# artificial
levels_rel_prob_art <- c("one","two", "three", "four")

# natural
# comment @Andrea: Potentially we have four levels, however, if I use four levels the script does not run properly
levels_rel_prob_nat <- c("one","two", "three")         # Season 2019/20
# levels_rel_prob_nat <- c("one","two", "three", "four") # Season 2020/21

# if you don't know the levels, run the script until the creation of the dataframe and take a look at the columns names
# for example names(contdf_ba) and names(contdf_bn)


##----- PROCESSING -----

# read data in dataframe, ; separated, header True, na values are N/A
all_values <- read.csv(InputFile, sep=";", header=TRUE, na.strings = "N/A")

#names(all_values)

# subset dataframe to colums needed
values <- subset(all_values, select = c(
  BulletinId, Date, Daytime, Region, Subregion, 
  DangerRatingBelowArtificialDangerRating, 
  DangerRatingBelowArtificialAvalancheSize, 
  DangerRatingBelowArtificialAvalancheReleaseProbability, 
  DangerRatingBelowArtificialHazardSiteDistribution,
  DangerRatingBelowNaturalDangerRating, 
  DangerRatingBelowNaturalAvalancheReleaseProbability, 
  DangerRatingBelowNaturalHazardSiteDistribution, 
  DangerRatingAboveArtificialDangerRating, 
  DangerRatingAboveArtificialAvalancheSize, 
  DangerRatingAboveArtificialAvalancheReleaseProbability, 
  DangerRatingAboveArtificialHazardSiteDistribution,
  DangerRatingAboveNaturalDangerRating, 
  DangerRatingAboveNaturalAvalancheReleaseProbability, 
  DangerRatingAboveNaturalHazardSiteDistribution))

# get all levels
#levels(values$DangerRatingBelowArtificialDangerRating)


values = values[!duplicated(values[,1]),]


#which(values$DangerRatingAboveArtificialDangerRating == values$DangerRatingBelowArtificialDangerRating)

# divide df according to Daytime
#values_AM <- values %>% filter(Daytime == "AM")
 
#values_PM <- values %>% filter(Daytime == "PM")


# identify AM rows not having corresponding PM
#values_AM_no_PM <- anti_join(values_AM, subset(values_PM, select = c(BulletinId, Date, Region, Subregion)))

# replace AM with PM
#values_AM_no_PM$Daytime <- replace(values_AM_no_PM$Daytime, values_AM_no_PM$Daytime == "AM", "PM")

# join dataframes together
#newdf <- full_join(values_AM, values_AM_no_PM)

# replace missing with NA
#newdf[newdf == "missing"] <- NA

# remove useless blanks text to strings
newdf = values
newdf <- data.frame(lapply(newdf, str_trim), stringsAsFactors = FALSE)


##----- Analyze contingency (Kontingenztabelle) -----


### --- artificial ---- 


### --- below ---- 
contingency_table_belowart <- ftable(newdf$DangerRatingBelowArtificialHazardSiteDistribution, newdf$DangerRatingBelowArtificialDangerRating, newdf$DangerRatingBelowArtificialAvalancheSize, newdf$DangerRatingBelowArtificialAvalancheReleaseProbability)

### change 0 values to NA
is.na(contingency_table_belowart) <- !contingency_table_belowart

# turn into dataframe
contdf_ba <- as.data.frame(stats:::format.ftable(contingency_table_belowart, quote = FALSE))

# rename cols with content of first row, and remove useless spaces
names(contdf_ba) <- make.names(str_trim((contdf_ba %>% slice(1) %>% unlist()), side="both"), unique = T)
contdf_ba <- contdf_ba %>% slice(-1)

# remove useless whitespaces
contdf_ba <- data.frame(lapply(contdf_ba, str_trim), stringsAsFactors = FALSE)

# fill empty places with NA
contdf_ba <- contdf_ba %>% mutate_all(na_if,"")

# check which rows contain only NA vals
index_na_rows <- apply(contdf_ba, 1, function(x) all(is.na(x)))
# remove empty rows
contdf_ba <- contdf_ba[ !index_na_rows, ]


index_na_cols <- apply(contdf_ba, 2, function(x) all(is.na(x)))
# remove emptycols
contdf_ba <- contdf_ba[ ,!index_na_cols]

# fill first column to use it as id 
contdf_ba <- contdf_ba %>% fill(X)

# fill second col to use as second id
contdf_ba <- contdf_ba %>% fill(X.1)

# sort dataframe 
contdf_ba$X <- factor(contdf_ba$X, levels=c("many_most", "many", "some", "single"))
contdf_ba <- contdf_ba[order(contdf_ba$X), ]
# Uncomment next line to show table
# contdf_ba

# sort levels
contdf_ba$X.1 <- factor(contdf_ba$X.1, levels = c("low", "moderate", "considerable", "high", "very_high"))
levels(contdf_ba$X.1)

contdf_ba$X.2 <- factor(contdf_ba$X.2, levels = c("small", "medium", "large", "very_large"))
levels(contdf_ba$X.2)

# sort cols among names (release probability)
names(contdf_ba)
col_order <- c("X", "X.1", "X.2", levels_rel_prob_art)
contdf_ba <- contdf_ba[, col_order]


############# melt 

con_bart_melt <- reshape2::melt(contdf_ba, id.vars = c("X", "X.1", "X.2"))

#change format from sting to integer, drop na vals
con_bart_melt$value <- as.integer(con_bart_melt$value)
con_bart_melt <- con_bart_melt %>% drop_na(value)

# calculate percentages (ave, prop.table), grouped by danger rating (X.1) for counts (values)
con_bart_melt$percent <- with(con_bart_melt, ave(value, X.1, FUN=prop.table))

# round to 0 digits, paste together with % 
con_bart_melt$percent <- paste0(round(con_bart_melt$percent*100,0),"%")

######### plot 
legend_ord <- sort(con_bart_melt$X.1)

# reorganise if faceted is used for y axes as well: facet_grid(X ~ variable) +
#con_bart_melt$X <- factor(con_bart_melt$X, levels=c("single", "some", "many", "many_most"))

r <- ggplot(con_bart_melt, aes(x=X.2, y=X, label=value, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level", values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "below artificial", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)+
  facet_grid( ~ variable) +
  theme(axis.text.y=element_text(angle=90, hjust = 0.5))
r


## save plot to file
r_path <- paste0(dirOut, "/below_art_absolute.png")
ggsave(r_path, width = 12, height = 10)


rs <- ggplot(con_bart_melt, aes(x=X.2, y=X, label=percent, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level",values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + # define colors
  geom_text(colour="black") + # plot values
  labs(title = paste("EAWS Matrix", season), subtitle = "below artificial in %", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2) +#color
  facet_grid( ~ variable) +
  theme(axis.text.y=element_text(angle=90, hjust = 0.5))

rs

## save plot to file
rs_path <- paste0(dirOut, "/below_art_percent.png")
ggsave(rs_path, width = 12, height = 10)


## save data to file
outFile <- paste0(dirOut, "/Table_below_artificial.csv")
write.csv(contdf_ba, file = outFile)

outFile_long <- paste0(dirOut, "/Table_below_artificial_long.csv")
write.csv(con_bart_melt, file = outFile_long)



### --- above ---- 
contingency_table_aboveart <- ftable(newdf$DangerRatingAboveArtificialHazardSiteDistribution, newdf$DangerRatingAboveArtificialDangerRating, newdf$DangerRatingAboveArtificialAvalancheSize, newdf$DangerRatingAboveArtificialAvalancheReleaseProbability)

### change 0 values to NA
is.na(contingency_table_aboveart) <- !contingency_table_aboveart

# turn into df
contdf_aa <- as.data.frame(stats:::format.ftable(contingency_table_aboveart, quote = FALSE))

# rename cols with content of first row, and remove useless spaces
names(contdf_aa) <- make.names(str_trim((contdf_aa %>% slice(1) %>% unlist()), side="both"), unique = T)
contdf_aa <- contdf_aa %>% slice(-1)

# remove useless whitespaces
contdf_aa <- data.frame(lapply(contdf_aa, str_trim), stringsAsFactors = FALSE)

# fill empty places with NA
contdf_aa <- contdf_aa %>% mutate_all(na_if,"")

# check which rows contain only NA vals
index_na_rows <- apply(contdf_aa, 1, function(x) all(is.na(x)))
# remove empty rows
contdf_aa <- contdf_aa[ !index_na_rows, ]


index_na_cols <- apply(contdf_aa, 2, function(x) all(is.na(x)))
# remove emptycols
contdf_aa <- contdf_aa[ ,!index_na_cols]

# fill first column to use it as id 
contdf_aa <- contdf_aa %>% fill(X)

# fill second col to use as second id
contdf_aa <- contdf_aa %>% fill(X.1)

# sort dataframe 
contdf_aa$X <- factor(contdf_aa$X, levels=c("many_most", "many", "some", "single"))
contdf_aa <- contdf_aa[order(contdf_aa$X), ]
contdf_aa

# define order of levels for danger rating and avalanche size
contdf_aa$X.1 <- factor(contdf_aa$X.1, levels = c("low", "moderate", "considerable", "high", "very_high"))
levels(contdf_aa$X.1)

contdf_aa$X.2 <- factor(contdf_aa$X.2, levels = c("small", "medium", "large", "very_large"))
levels(contdf_aa$X.2)


# searching for elegant way to sort names
# sort cols amon names (release probability)
names(contdf_aa)
col_order <- c("X", "X.1", "X.2", levels_rel_prob_art)
contdf_aa <- contdf_aa[, col_order]

# melt 
con_aart_melt <- reshape2::melt(contdf_aa, id.vars = c("X", "X.1", "X.2"))

#change format from sting to integer, drop na vals
con_aart_melt$value <- as.integer(con_aart_melt$value)
con_aart_melt <- con_aart_melt %>% drop_na(value)

# calculate percentages (ave, prop.table), grouped by danger rating (X.1) for counts (values)
con_aart_melt$percent <- with(con_aart_melt, ave(value, X.1, FUN=prop.table))

# round to 2 digits, paste together with % for nicer plotting
con_aart_melt$percent <- paste0(round(con_aart_melt$percent*100,0),"%")


#### plot 
legend_ord <- sort(con_aart_melt$X.1)

# reorganise if facet is used for y axes as well
#con_aart_melt$X <- factor(con_aart_melt$X, levels=c("single", "some", "many", "many_most"))

r <- ggplot(con_aart_melt, aes(x=X.2, y=X, label=value, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level", values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "above artificial", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)+
  facet_grid(~ variable) +
  theme(axis.text.y=element_text(angle=90, hjust = 0.5))
r

## save plot to file
r_path <- paste0(dirOut, "/above_art_absolute.png")
ggsave(r_path, width = 12, height = 10)


rs <- ggplot(con_aart_melt, aes(x=X.2, y=X, label=percent, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level",values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + # define colors
  geom_text(colour="black") + # plot values
  labs(title = paste("EAWS Matrix", season), subtitle = "above artificial in %", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2) +#color
  facet_grid( ~ variable) +
  theme(axis.text.y=element_text(angle=90, hjust = 0.5))

rs

## save plot to file
rs_path <- paste0(dirOut, "/above_art_percent.png")
ggsave(rs_path, width = 12, height = 10)


## save data to file
outFile <- paste0(dirOut, "/Table_above_artificial.csv")
write.csv(contdf_aa, file = outFile)

outFile_long <- paste0(dirOut, "/Table_above_artificial_long.csv")
write.csv(con_aart_melt, file = outFile_long)


### --- sum artificial ---- 

contingency_table_all_art <- contingency_table_belowart + contingency_table_aboveart

### change 0 values to NA
is.na(contingency_table_all_art) <- !contingency_table_all_art

# turn into dataframe
contdf_all_art <- as.data.frame(stats:::format.ftable(contingency_table_all_art, quote = FALSE))

# rename cols with content of first row, and remove useless spaces
names(contdf_all_art) <- make.names(str_trim((contdf_all_art %>% slice(1) %>% unlist()), side="both"), unique = T)
contdf_all_art <- contdf_all_art %>% slice(-1)

# remove useless whitespaces
contdf_all_art <- data.frame(lapply(contdf_all_art, str_trim), stringsAsFactors = FALSE)

# fill empty places with NA
contdf_all_art <- contdf_all_art %>% mutate_all(na_if,"")

# check which rows contain only NA vals
index_na_rows <- apply(contdf_all_art, 1, function(x) all(is.na(x)))
# remove empty rows
contdf_all_art <- contdf_all_art[ !index_na_rows, ]


index_na_cols <- apply(contdf_all_art, 2, function(x) all(is.na(x)))
# remove emptycols
contdf_all_art <- contdf_all_art[ ,!index_na_cols]

# fill first column to use it as id 
contdf_all_art <- contdf_all_art %>% fill(X)

# fill second col to use as second id
contdf_all_art <- contdf_all_art %>% fill(X.1)

# sort dataframe 
contdf_all_art$X <- factor(contdf_all_art$X, levels=c("many_most", "many", "some", "single"))
contdf_all_art <- contdf_all_art[order(contdf_all_art$X), ]
contdf_all_art

# sort levels
contdf_all_art$X.1 <- factor(contdf_all_art$X.1, levels = c("low", "moderate", "considerable", "high", "very_high"))
levels(contdf_all_art$X.1)

contdf_all_art$X.2 <- factor(contdf_all_art$X.2, levels = c("small", "medium", "large", "very_large"))
levels(contdf_all_art$X.2)


# sort cols amon names (release probability)
names(contdf_all_art)
col_order <- c("X", "X.1", "X.2", levels_rel_prob_art)
contdf_all_art <- contdf_all_art[, col_order]

############# melt 

con_all_art_melt <- reshape2::melt(contdf_all_art, id.vars = c("X", "X.1", "X.2"))

#change format from sting to integer, drop na vals
con_all_art_melt$value <- as.integer(con_all_art_melt$value)
con_all_art_melt <- con_all_art_melt %>% drop_na(value)

# calculate percentages (ave, prop.table), grouped by danger rating (X.1) for counts (values)
con_all_art_melt$percent <- with(con_all_art_melt, ave(value, X.1, FUN=prop.table))

# round to 2 digits, paste together with % 
con_all_art_melt$percent <- paste0(round(con_all_art_melt$percent*100,0),"%")

##### plot 

legend_ord <- sort(con_all_art_melt$X.1)

# reorganise if faceted is used for y axe as well
#con_all_art_melt$X <- factor(con_all_art_melt$X, levels=c("single", "some", "many", "many_most"))

r <- ggplot(con_all_art_melt, aes(x=X.2, y=X, label=value, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level", values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "artificial", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)+
  facet_grid( ~ variable) +
  theme(axis.text.y=element_text(angle=90, hjust = 0.5))
r

## save plot to file
r_path <- paste0(dirOut, "/all_art_absolute.png")
ggsave(r_path, width = 10, height = 10)


rs <- ggplot(con_all_art_melt, aes(x=X.2, y=X, label=percent, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level",values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + # define colors
  geom_text(colour="black") + # plot values
  labs(title = paste("EAWS Matrix", season), subtitle = "artificial in %", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2) +#color
  facet_grid( ~ variable) +
  theme(axis.text.y=element_text(angle=90, hjust = 0.5))

rs

## save plot to file
rs_path <- paste0(dirOut, "/all_art_percent.png")
ggsave(rs_path, width = 10, height = 10)


## save data to file
outFile <- paste0(dirOut, "/Table_all_artificial.csv")
write.csv(contdf_all_art, file = outFile)

outFile_long <- paste0(dirOut, "/Table_all_artificial_long.csv")
write.csv(con_all_art_melt, file = outFile_long)






### --- natural ----


### --- below ----
### calc contingency table
contingency_table_belownat <- ftable(newdf$DangerRatingBelowNaturalHazardSiteDistribution, newdf$DangerRatingBelowNaturalDangerRating, newdf$DangerRatingBelowNaturalAvalancheReleaseProbability)

### change 0 values to NA
is.na(contingency_table_belownat) <- !contingency_table_belownat

# turn into df
contdf_bn <- as.data.frame(stats:::format.ftable(contingency_table_belownat, quote = FALSE))

# get all levels of all cols
#contdf_bn %>% sapply(levels)

# drop col without content (less than one level)
#contdf_bn[which(contdf_bn %>% map(levels) %>% map(length) <= 1)] <- NULL

# rename cols with content of first row, and remove useless spaces
names(contdf_bn) <- make.names(str_trim((contdf_bn %>% slice(1) %>% unlist()), side="both"), unique = T)
contdf_bn <- contdf_bn %>% slice(-1)

# remove useless whitespaces in all places
contdf_bn <- data.frame(lapply(contdf_bn, str_trim), stringsAsFactors = FALSE)

# fill empty places with NA
contdf_bn <- contdf_bn %>% mutate_all(na_if,"")

# check which rows contain only NA vals
index_na_rows <- apply(contdf_bn, 1, function(x) all(is.na(x)))
# remove empty rows
contdf_bn <- contdf_bn[ !index_na_rows, ]

# check which cols contain only NA vals
index_na_cols <- apply(contdf_bn, 2, function(x) all(is.na(x)))
# remove emptycols
contdf_bn <- contdf_bn[ ,!index_na_cols]


# searching for elegant way to sort names
names(contdf_bn)
col_order <- c("X", "X.1", levels_rel_prob_nat)
contdf_bn <- contdf_bn[, col_order]

# fill first column to use it as id 
contdf_bn <- contdf_bn %>% fill(X)

# sort dataframe 
contdf_bn$X <- factor(contdf_bn$X, levels=c("many_most", "many", "some", "single"))
contdf_bn <- contdf_bn[order(contdf_bn$X), ]
#contdf_bn

# melt data frame among id columns distribution (X) and probability (X.1)
con_bnat_melt <- reshape2::melt(contdf_bn, id.vars = c("X", "X.1"))

# change format from sting to integer, drop na vals
con_bnat_melt$value <- as.integer(con_bnat_melt$value)
con_bnat_melt <- con_bnat_melt %>% drop_na(value)

# change order of levels
con_bnat_melt$X.1 <- factor(con_bnat_melt$X.1, levels = c("low", "moderate", "considerable", "high", "very_high"))
#levels(con_bnat_melt$V2)

# calculate percentages (ave, prop.table), grouped by danger rating (X.1) for counts (values)
con_bnat_melt$percent <- with(con_bnat_melt, ave(value, X.1, FUN=prop.table))

# round to 2 digits, paste together with % 
con_bnat_melt$percent <- paste0(round(con_bnat_melt$percent*100,0),"%")

# PLOT #

# define sorting for legend
legend_ord <- sort(con_bnat_melt$X.1)

# create plot with absolute values
r <- ggplot(con_bnat_melt, aes(x=variable, y=X, label=value, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level", values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "below natural", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)

# show plot
r

## save plot to file
r_path <- paste0(dirOut, "/below_nat_absolute.png")
savePlotAsImage(r_path, format = "png",width=800, height=850)


# create plot with percentages
rs <- ggplot(con_bnat_melt, aes(x=variable, y=X, label=percent, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level",values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "below natural in %", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)

rs

## save plot to file
rs_path <- paste0(dirOut, "/below_nat_percent.png")
savePlotAsImage(rs_path, format = "png",width=800, height=850)


## save data to file
outFile <- paste0(dirOut, "/Table_below_natural.csv")
write.csv(contdf_bn, file = outFile)

outFile_long <- paste0(dirOut, "/Table_below_natural_long.csv")
write.csv(con_bnat_melt, file = outFile_long)


### --- above ----

# above
contingency_table_abovenat <- ftable(newdf$DangerRatingAboveNaturalHazardSiteDistribution, newdf$DangerRatingAboveNaturalDangerRating, newdf$DangerRatingAboveNaturalAvalancheReleaseProbability)

### change 0 values to NA
is.na(contingency_table_abovenat) <- !contingency_table_abovenat

# turn into df
contdf_an <- as.data.frame(stats:::format.ftable(contingency_table_abovenat, quote = FALSE))

# rename cols with content of first row, and remove useless spaces
names(contdf_an) <- make.names(str_trim((contdf_an %>% slice(1) %>% unlist()), side="both"), unique = T)
contdf_an <- contdf_an %>% slice(-1)

# remove useless whitespaces in all places
contdf_an <- data.frame(lapply(contdf_an, str_trim), stringsAsFactors = FALSE)

# fill empty places with NA
contdf_an <- contdf_an %>% mutate_all(na_if,"")

# check which rows contain only NA vals
index_na_rows <- apply(contdf_an, 1, function(x) all(is.na(x)))
# remove empty rows
contdf_an <- contdf_an[ !index_na_rows, ]

# check which cols contain only NA vals
index_na_cols <- apply(contdf_an, 2, function(x) all(is.na(x)))
# remove emptycols
contdf_an <- contdf_an[ ,!index_na_cols]


# searching for elegant way to sort names
names(contdf_an)
col_order <- c("X", "X.1", levels_rel_prob_nat)
contdf_an <- contdf_an[, col_order]

# fill first column to use it as id 
contdf_an <- contdf_an %>% fill(X)

# sort dataframe 
contdf_an$X <- factor(contdf_an$X, levels=c("many_most", "many", "some", "single"))
contdf_an <- contdf_an[order(contdf_an$X), ]
#contdf_an

# melt data frame among id columns distribution (X) and probability (X.1)
con_anat_melt <- reshape2::melt(contdf_an, id.vars = c("X", "X.1"))

# change format from sting to integer, drop na vals
con_anat_melt$value <- as.integer(con_anat_melt$value)
con_anat_melt <- con_anat_melt %>% drop_na(value)

# change order of levels
con_anat_melt$X.1 <- factor(con_anat_melt$X.1, levels = c("low", "moderate", "considerable", "high","very_high"))

# calculate percentages (ave, prop.table), grouped by danger rating (X.1) for counts (values)
con_anat_melt$percent <- with(con_anat_melt, ave(value, X.1, FUN=prop.table))

# round to 2 digits, paste together with % 
con_anat_melt$percent <- paste0(round(con_anat_melt$percent*100, 1),"%")

# PLOT #

# define sorting for legend
legend_ord <- sort(con_anat_melt$X.1)

# create plot with absolute values
r <- ggplot(con_anat_melt, aes(x=variable, y=X, label=value, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level", values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "above natural", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)

# show plot
r

## save plot to file
r_path <- paste0(dirOut, "/above_nat_absolute.png")
savePlotAsImage(r_path, format = "png",width=800, height=850)


# create plot with percentages
rs <- ggplot(con_anat_melt, aes(x=variable, y=X, label=percent, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level",values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "above natural in %", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)

rs

## save plot to file
rs_path <- paste0(dirOut, "/above_nat_percent.png")
savePlotAsImage(rs_path, format = "png",width=800, height=850)


## save data to file

outFile <- paste0(dirOut, "/Table_above_natural.csv")
write.csv(contdf_an, file = outFile)

outFile_long <- paste0(dirOut, "/Table_above_natural_long.csv")
write.csv(con_anat_melt, file = outFile_long)



### --- sum natural ----

# contingency all natural
contingency_table_all <- contingency_table_belownat + contingency_table_abovenat

### change 0 values to NA
is.na(contingency_table_all) <- !contingency_table_all

# turn into df
contdf_all <- as.data.frame(stats:::format.ftable(contingency_table_all, quote = FALSE))

# rename cols with content of first row, and remove useless spaces
names(contdf_all) <- make.names(str_trim((contdf_all %>% slice(1) %>% unlist()), side="both"), unique = T)
contdf_all <- contdf_all %>% slice(-1)

# remove useless whitespaces in all places
contdf_all <- data.frame(lapply(contdf_all, str_trim), stringsAsFactors = FALSE)

# fill empty places with NA
contdf_all <- contdf_all %>% mutate_all(na_if,"")

# check which rows contain only NA vals
index_na_rows <- apply(contdf_all, 1, function(x) all(is.na(x)))
# remove empty rows
contdf_all <- contdf_all[ !index_na_rows, ]

# check which cols contain only NA vals
index_na_cols <- apply(contdf_all, 2, function(x) all(is.na(x)))
# remove emptycols
contdf_all <- contdf_all[ ,!index_na_cols]


# searching for elegant way to sort names
names(contdf_all)
col_order <- c("X", "X.1", levels_rel_prob_nat)
contdf_all <- contdf_all[, col_order]

# fill first column to use it as id 
contdf_all <- contdf_all %>% fill(X)

# sort dataframe 
contdf_all$X <- factor(contdf_all$X, levels=c("many_most", "many", "some", "single"))
contdf_all <- contdf_all[order(contdf_all$X), ]

# melt data frame among id columns distribution (X) and probability (X.1)
con_all_nat_melt <- reshape2::melt(contdf_all, id.vars = c("X", "X.1"))

# change format from sting to integer, drop na vals
con_all_nat_melt$value <- as.integer(con_all_nat_melt$value)
con_all_nat_melt <- con_all_nat_melt %>% drop_na(value)

# change order of levels
con_all_nat_melt$X.1 <- factor(con_all_nat_melt$X.1, levels = c("low", "moderate", "considerable", "high", "very_high"))

# calculate percentages (ave, prop.table), grouped by danger rating (X.1) for counts (values)
con_all_nat_melt$percent <- with(con_all_nat_melt, ave(value, X.1, FUN=prop.table))

# round to 2 digits, paste together with % 
con_all_nat_melt$percent <- paste0(round(con_all_nat_melt$percent*100, 0),"%")

# PLOT #

# define sorting for legend
legend_ord <- sort(con_all_nat_melt$X.1)

# create plot with absolute values
r <- ggplot(con_all_nat_melt, aes(x=variable, y=X, label=value, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level", values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "all natural", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)

# show plot
r

## save plot to file
r_path <- paste0(dirOut, "/all_nat_absolute.png")
savePlotAsImage(r_path, format = "png",width=800, height=850)


# create plot with percentages
rs <- ggplot(con_all_nat_melt, aes(x=variable, y=X, label=percent, fill=(X.1))) + 
  scale_fill_manual(breaks=legend_ord, name = "Danger Level",values = c("low" = "green", "moderate" = "yellow", "considerable" = "orange", "high" = "red", "very_high" = "darkred")) + 
  geom_text(colour="black") +
  labs(title = paste("EAWS Matrix", season), subtitle = "all natural in %", x = "Release Probability", y = "Hazard Site Distribution") + 
  geom_tile(alpha=0.2)

rs

## save plot to file
rs_path <- paste0(dirOut, "/all_nat_percent.png")
savePlotAsImage(rs_path, format = "png",width=800, height=850)


## save data to file

outFile <- paste0(dirOut, "/Table_all_natural.csv")
write.csv(contdf_all, file = outFile)

outFile_long <- paste0(dirOut, "/Table_all_natural_long.csv")
write.csv(con_all_nat_melt, file = outFile_long)





cat("finished.")
