## Description
Make a movie of the daily albina maps. 

The size and position of text and other objects are a bit try-and-error. But should work independant from the year. For season 2021 the gif was running much faster than for season 1920. If you want to see the version of the season 1920, have a look at the tag "1920" in the master branch.

## Workflow
1. Edit settings at the top of download_images.py (defines season, ...)
2. Run download_images.py
3. Use notebook make-thumbnail-gif.ipynb to edit images and generate .gif (settings should suite most seasons now, change some line for Fonts depending on OS)

## Data
check the subfolders within each season in 'data'