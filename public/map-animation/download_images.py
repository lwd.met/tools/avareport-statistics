# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 11:42:06 2021

First script to execute.
Create folders.
Download the images from the albina homepage.


@author: Alexander Kehl
"""

import urllib.request
import os
import pandas as pd
from urllib.error import HTTPError
import time
import pdb

# Season in the format 2019/2020 = 1920.
season = str(2022)

# Start/End of season
start = '2021-12-01'
end   = '2022-05-01'

#%%
def try_create_dir(mydir):
    """
    Try to create a directory.

    Parameters
    ----------
    mydir : str
        The directory you want to create.

    Returns
    -------
    None.

    """

    try:
        os.mkdir(mydir)
    except FileExistsError:
        pass
        # If you prefer to have a warning uncomment the lines below.
        # warnings.warn(
        #     f'Cannot create a file when that file already exists: {mydir}',
        #     UserWarning)
    return(None)
#%%

main_url = 'https://lawinen.report/albina_files/'

# Date range in the format of the folders on the albina server
dates = [i for i in pd.date_range(start=start, end=end).strftime('%Y-%m-%d')]

#%% Create directories

# Create and define data folder structure and directories
try_create_dir('data')
dir_season = os.path.join('data', season)
try_create_dir(dir_season)

dir_jpg = os.path.join(dir_season, '0_JPG_IMAGE')
dir_png = os.path.join(dir_season, '1_PNG_IMAGE')
dir_pro = os.path.join(dir_season, '2_PROCESSED_IMAGE')
dir_mov = os.path.join(dir_season, '3_MOVIES')

for folder in [dir_jpg, dir_png, dir_pro, dir_mov]:
    try_create_dir(folder)

dir_thumb = os.path.join(dir_jpg, 'thumbnails')
dir_map = os.path.join(dir_jpg, 'maps')
dir_2thumb = os.path.join(dir_jpg, 'double_thumbnails')
dir_2map = os.path.join(dir_jpg, 'double_map')

dir_thumb_png = os.path.join(dir_png, 'thumbnails')
dir_thumb_pro = os.path.join(dir_pro, 'thumbnails')
dir_map_png = os.path.join(dir_png, 'maps')
dir_map_pro = os.path.join(dir_pro, 'maps')

for folder in [dir_thumb, dir_2thumb, dir_thumb_png, dir_thumb_pro,
               dir_map, dir_2map, dir_map_png, dir_map_pro]:
    try_create_dir(folder)

#%% Download images

if __name__ == '__main__':

    # Track the time of the download
    time1 = time.time()

    for date in dates:
        fname_thumb_am = 'am_albina_thumbnail.jpg'
        fname_thumb_fd = 'fd_albina_thumbnail.jpg'
        fname_thumb_pm = 'pm_albina_thumbnail.jpg'

        fname_map_am = 'am_albina_map.jpg'
        fname_map_fd = 'fd_albina_map.jpg'
        fname_map_pm = 'pm_albina_map.jpg'

        # OPTIMIZE: avoid long try statement (style & computation time),
        # rather look at:
        # https://stackoverflow.com/questions/11023530/python-to-list-http-files-and-directories
        try:
            # Thumbnails
            # AM/PM
            urllib.request.urlretrieve(
                ''.join((main_url, date, '/', fname_thumb_am)),
                os.path.join(dir_thumb, '_'.join((date, fname_thumb_am))))
            urllib.request.urlretrieve(
                ''.join((main_url, date, '/', fname_thumb_pm)),
                os.path.join(dir_thumb, '_'.join((date, fname_thumb_pm))))
            # FD into double_thumbnails
            urllib.request.urlretrieve(
                ''.join((main_url, date, '/', fname_thumb_fd)),
                os.path.join(
                    dir_2thumb, '_'.join((date, fname_thumb_fd))))
            # Maps
            # AM/PM
            urllib.request.urlretrieve(
                ''.join((main_url, date, '/', fname_map_am)),
                os.path.join(dir_map, '_'.join((date, fname_map_am))))
            urllib.request.urlretrieve(
                ''.join((main_url, date, '/', fname_map_pm)),
                os.path.join(dir_map, '_'.join((date, fname_map_pm))))
            # FD into double_map
            urllib.request.urlretrieve(
                ''.join((main_url, date, '/', fname_map_fd)),
                os.path.join(
                    dir_2map, '_'.join((date, fname_map_fd))))
        except HTTPError:
            # Thumbnails
            urllib.request.urlretrieve(
                ''.join((main_url, date, '/', fname_thumb_fd)),
                os.path.join(dir_thumb, '_'.join((date, fname_thumb_fd))))
            # Map
            urllib.request.urlretrieve(
                ''.join((main_url, date, '/', fname_map_fd)),
                os.path.join(dir_map, '_'.join((date, fname_map_fd))))

    print(f'{time.time() - time1}')