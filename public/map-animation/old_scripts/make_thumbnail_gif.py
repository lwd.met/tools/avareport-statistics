# -*- coding: utf-8 -*-
"""
Script that edits all images (adds timestamp) and then
makes a video from all images.

OPTIMIZE: this script is mostly copy-paste from make_map_gif.py ->
"""
import os
import pdb
import io
import imageio
# Documentation: https://pillow.readthedocs.io/en/stable/
from PIL import Image,ImageDraw,ImageFont
import numpy as np
import pandas as pd

from download_images import dates as my_dates # # Avoid conflict with dates module from pandas/numpy here
from download_images import dir_thumb, dir_thumb_png, dir_thumb_pro, dir_mov

# - Save .jpeg as .png - #
for file_name in os.listdir(dir_thumb):
    if not file_name.endswith('.png'):
        im = Image.open(os.path.join(dir_thumb, file_name))
        im.save(os.path.join(dir_thumb_png, file_name[:-4]) + '.png')

# - List of all png files - #
print(dir_thumb_png)
png_files = [k for k in os.listdir(dir_thumb_png)]
print(png_files)

# - Text above retangles - #
rects_text = pd.date_range(
    my_dates[0], my_dates[-1], freq='30D').strftime('%b-%d')
# Setting line length to the number of days is easier than other approaches
line_len = len(my_dates)
# Rectangles on the line
rectangle_pos = np.array(
    [i for i, j in enumerate(
        pd.date_range(my_dates[0], my_dates[-1],
                      freq='D').strftime('%b-%d'))
        if j in rects_text])

# Initialize some variables for the images
date_previous = my_dates[0]         # Start date
# counter, which determines the position of timeline pointer (=black circle)
i = 0
# Set parameters for some plot objects
x_start = 60                        # Start of line
x_end_line = x_start + line_len     # End of line
rec_width = 8                       # Width of rectangle
y_text=5                            # Start of text from upper image border

# load a font
# font = ImageFont.truetype("arial.ttf", 24)
# font_2 = ImageFont.truetype("arial.ttf", 12)

#%% Adapt each .png file
for file_name in png_files:

    png = Image.open(os.path.join(dir_thumb_png, file_name), mode= 'r')
    roiimg = png.crop()

    date = file_name[0 : 10]
    # Increase counter for a new day, not for AM/PM differences.
    # If you want your pointer to move also for AM/PM, you could do
    # something like:
        # if date == date_previous or (
        # i is "a multiple of 0.5, e.g. 1.5, 2.5,..."):
        #     i += 0.5
        # else:
        #     i += 1
    if date > date_previous:
        i += 1 # Increase by one day
        date_previous = date

    imgByteArr = io.BytesIO()
    roiimg.save(imgByteArr, format='PNG')
    imgByteArr = imgByteArr.getvalue()


    # create a PIL image object
    image = Image.open(io.BytesIO(imgByteArr))
    draw = ImageDraw.ImageDraw(image)


    # draw title
    draw.text((image.width - 140,305),
              date,
              fill=(26, 71, 255))
    #           font=font)

    draw.line([(x_start,40),(x_end_line,40)],
               fill = (26, 71, 255),
               width = 3)

    draw.text((10, y_text),'Month:\nDay:',
      fill=(26, 71, 255))
    #   font=font_2)


    for name, rec_pos in zip(rects_text, x_start + rectangle_pos):

        name = name.replace('-', '\n')

        draw.rectangle(
            [(rec_pos - rec_width/2, 36), (rec_pos + rec_width/2, 44)],
            fill='white', outline= (26, 71, 255))

        draw.text((rec_pos-10,y_text),name,
          fill=(26, 71, 255))
    #       font=font_2)

    # draw marker for timeline
    draw.ellipse((x_start - rec_width/2 + i, 36,
                  x_start + rec_width/2 + i, 44),
                  fill = 'black', outline='black')

    # write to a png file
    filename = os.path.join(dir_thumb_pro, file_name)
    image.save(filename, "PNG")

# - MAKE GIF - #
images = []
for file_name in os.listdir(dir_thumb_pro):
    if file_name.endswith('.png'):
        file_path = os.path.join(dir_thumb_pro, file_name)
        images.append(imageio.imread(file_path))
# OPTIMIZE season 1920 was slower/better than season 2021, adapt fps?!
imageio.mimsave(os.path.join(dir_mov,'movie_thumbnails.gif'), images,fps=10)

# - OPTIONAL: Delete images due to large size - #
# #providing the path of the folder
# folder_path = (thumbnails_dir)
# #using listdir() method to list the files of the folder
# test = os.listdir(folder_path)
# #taking a loop to remove all the images
# #using ".png" extension to remove only png images
# for images in test:
#     if images.endswith(".jpg"):
#         os.remove(os.path.join(folder_path, images))

