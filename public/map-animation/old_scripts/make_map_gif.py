# -*- coding: utf-8 -*-
"""
Created on Thu May 28 10:00:50 2020

Make a video from all single images.

@author: Admin/Alexander Kehl
"""

# OPTIMIZE: this script is mostly copy-paste from make_thumbnail_gif.py ->
# make one instead of two scripts


import os
import imageio
from PIL import Image,ImageDraw,ImageFont
import io
import numpy as np
import shutil
import glob
import fnmatch
import pandas as pd
import pdb

# Local imports
# Avoid conflict with dates module from pandas/numpy here
from download_images import dates as my_dates
from download_images import dir_map, dir_map_png, dir_map_pro, dir_mov

#%%

for file_name in os.listdir(dir_map):

    # save .jpg as .png
    if file_name.endswith('.jpg'):
        im = Image.open(os.path.join(dir_map,file_name))
        file = os.path.splitext(os.path.basename(file_name))[0]
        im.save(os.path.join(dir_map_png,file) + '.png')
#%%

# List of all png files
png_files = [k for k in os.listdir(dir_map_png)]

# Some stuff to put into images
# Text above retangles
rects_text = pd.date_range(
    my_dates[0], my_dates[-1], freq='30D').strftime('%b-%d')
# Setting line length to the number of days is easier than other approaches
line_len = len(my_dates)
# Rectangles on the line
rectangle_pos = np.array(
    [i for i, j in enumerate(
        pd.date_range(my_dates[0], my_dates[-1],
                      freq='D').strftime('%b-%d'))
        if j in rects_text])

# Initialize some variables for the images
date_previous = my_dates[0]         # Start date
i = 0                               # Date iterator
# Set parameters for some plot objects
x_start = 200                       # Start of line
x_end_line = x_start + 4*line_len   # End of line
rec_width = 36                      # Width of rectangle
y_text=5                            # Start of text from upper image border
# load a font
font = ImageFont.truetype("arial.ttf", 56)
font_2 = ImageFont.truetype("arial.ttf", 50)

#%%
for file_name in png_files:

    png = Image.open(os.path.join(dir_map_png, file_name), mode= 'r')
    roiimg = png.crop()

    date = file_name[0 : 10]
    # Increase counter for a new day, not for AM/PM differences.
    # If you want your pointer to move also for AM/PM, you could do
    # something like:
        # if date == date_previous or (
        # i is "a multiple of 0.5, e.g. 1.5, 2.5,..."):
        #     i += 0.5
        # else:
        #     i += 1
    if date > date_previous:
        i += 1 # Increase by one day
        date_previous = date

    imgByteArr = io.BytesIO()
    roiimg.save(imgByteArr, format='PNG')
    imgByteArr = imgByteArr.getvalue()


    # create a PIL image object
    image = Image.open(io.BytesIO(imgByteArr))
    draw = ImageDraw.ImageDraw(image)

    # draw title
    draw.text((400*4,15*4),
              date,
              fill=(26, 71, 255),
              font=font)

    draw.line([(x_start,4*40),(x_end_line,4*40)],
               fill = (26, 71, 255),
               width = 6)


    draw.text((x_start - 4*50,4*y_text),'Month:\nDay:',
      fill=(26, 71, 255),
      font=font_2)


    for name, rec_pos in zip(rects_text,x_start + 4*rectangle_pos):

        name = name.replace('-', '\n')

        draw.rectangle(
            [(rec_pos-rec_width/2, 4*36),
             (rec_pos+rec_width/2, 4*36 + rec_width)],
            fill='white', outline= (26, 71, 255))

        draw.text(((rec_pos - 4*10), y_text*4),name,
          fill=(26, 71, 255),
          font=font_2)

    # draw marker for timeline
    draw.ellipse((x_start-rec_width/2 + 4*i,4*36,
                  x_start-rec_width/2 + 4*(8+i),4*44),
                 fill='black', outline='black')


    # write to a png file
    filename = os.path.join(dir_map_pro, file_name)
    image.save(filename, 'PNG')

#%%
# make gif block
images = []
for file_name in os.listdir(dir_map_pro):
    if file_name.endswith('.png'):
        file_path = os.path.join(dir_map_pro, file_name)
        images.append(imageio.imread(file_path))
imageio.mimsave(os.path.join(dir_mov,'map_10s_24.gif'), images,fps=24)

# # Slow motion
# png_dir = 'img_maps/'
# images = []
# for file_name in os.listdir(png_dir):
#     if file_name.endswith('.png'):
#         file_path = os.path.join(png_dir, file_name)
#         # Append image twice to slow thing down
#         images.append(imageio.imread(file_path))
#         images.append(imageio.imread(file_path))
# imageio.mimsave('movies/map_20s_24.gif', images,fps=24)

######
# If you want to delete same images, adapt/uncomment the code below and run it,
# because images might require more than 1GB storage


# #providing the path of the folder
# folder_path = (dir_map)
# #using listdir() method to list the files of the folder
# image_files = os.listdir(folder_path)
# #taking a loop to remove all the images
# #using ".png" extension to remove only png images
# for images in image_files:
#     if images.endswith(".jpg"):
#         os.remove(os.path.join(folder_path, images))