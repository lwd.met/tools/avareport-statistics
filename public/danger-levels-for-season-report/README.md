# dangerlevelrating.py

Creates the table of the maximum danger ratings and all avalanche problems for each day and micro region.

Example usage:
```
python3 dangerlevelrating.py bulletins output.csv 2023-11-01 2024-05-31
```

Description of arguments:
```
bulletins - foldername, into which all the bulletins of the seasons are saved
output.csv - the output of the script in csv format (separators: ;)
2023-11-01 - date for start of the season or first report (YYYY-mm-dd)
2024-05-31 - date for end of the season or last report (YYYY-mm-dd)
```