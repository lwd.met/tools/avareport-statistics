import json
import os
import requests
import sys
from datetime import datetime, timedelta

#python3 dangerlevelrating.py bulletins output.csv 2023-11-01 2024-05-31
def main():
    bulletins_folder = sys.argv[1]
    output_file = sys.argv[2]
    start_date = datetime.strptime(sys.argv[3], "%Y-%m-%d")
    end_date = datetime.strptime(sys.argv[4], "%Y-%m-%d")

    if os.path.exists(bulletins_folder):
        print(f"Error: '{bulletins_folder}' already exists")
        return
    os.mkdir(bulletins_folder)
    
    if os.path.exists(output_file):
        print(f"Error: '{output_file}' already exists")
        return

    print("Downloading bulletins")
    download_bulletins(bulletins_folder, start_date, end_date)

    print("Extracting data")
    ratings = extract_ratings(bulletins_folder, "AT-07-")
    
    print("Creating csv")
    csv_text = convert_csv(ratings)

    f = open(output_file, "w")
    f.write(csv_text)
    f.close()

def download_bulletins(folder, start_date, end_date):
    date = start_date
    while date <= end_date:
        date_string = date.strftime("%Y-%m-%d")
        date += timedelta(1)

        baseurl = f"https://static.avalanche.report/bulletins/{date_string}/"
        content = requests.get(f"{baseurl}{date_string}_de_CAAMLv6.json").content
        if content == b"":
            continue

        file = open(os.path.join(folder, f"{date_string}.json"), "wb")
        file.write(content)
        file.close()

def extract_ratings(bulletins_folder, region_prefix):
    ratings = []

    for file in os.listdir(bulletins_folder):
        f = open(os.path.join(bulletins_folder, file), "r")
        data = json.loads(f.read())
        f.close()
        
        ratings_translation = {
            "no_snow": 0,
            "low": 1,
            "moderate": 2,
            "considerable": 3,
            "high": 4,
            "very high": 5
        }

        for i in data["bulletins"]:
            for j in i["regions"]:
                if j["regionID"].startswith(region_prefix):
                    max_rating = max([ratings_translation[r["mainValue"]] for r in i["dangerRatings"]])

                    start_valid = datetime.strptime(i["validTime"]["startTime"], "%Y-%m-%dT%H:%M:%S%z")
                    end_valid = datetime.strptime(i["validTime"]["endTime"], "%Y-%m-%dT%H:%M:%S%z")
                    
                    ratings.append({
                        "date": (start_valid + (end_valid - start_valid) / 2).strftime("%Y-%m-%d"),
                        "max_rating": max_rating,
                        "region_code": j["regionID"],
                        "region_name": j["name"],
                        "problems": list(dict.fromkeys([p["problemType"] for p in i["avalancheProblems"]]))
                    })

    return ratings

def convert_csv(ratings):
    max_problems = 0
    for i in ratings:
        max_problems = max(max_problems, len(i["problems"]))

    csv_text = '"date";"max_rating";"region_code";"region_name"'
    for i in range(max_problems):
        csv_text += f';"problem{i + 1}"'

    for i in ratings:
        problems = ""
        for j in i["problems"]:
            problems += f';"{j}"'
        problems += ";" * (max_problems - len(i["problems"]))

        csv_text += f'\n"{i["date"]}";"{i["max_rating"]}";"{i["region_code"]}";"{i["region_name"]}"{problems}'

    return csv_text.replace("\xA0", " ")

if __name__ == "__main__":
    main()